\global \c@tocdepth 1\relax 
\beamer@sectionintoc {1}{Main}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Horizontal pleiotropy and co-regulation}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Capturing local co-regulation}{3}{0}{1}
\beamer@subsectionintoc {1}{3}{$P_n^{(k)}$ distribution across eQTLs}{4}{0}{1}
\beamer@subsectionintoc {1}{4}{$P_n^{(all)}$ correlates with functional annotation}{5}{0}{1}
\beamer@subsectionintoc {1}{5}{Co-regulation correlates with GWAS effect size?}{6}{0}{1}
\beamer@subsectionintoc {1}{6}{$\mathaccentV {tilde}07E\beta ^2$ positively correlates with co-regulation}{7}{0}{1}
\beamer@subsectionintoc {1}{7}{Regions with higher level of co-regulation explain more heritability}{8}{0}{1}
\beamer@subsectionintoc {1}{8}{Co-regulation correlates with trait horizontal pleiotropy?}{9}{0}{1}
\beamer@subsectionintoc {1}{9}{Co-regulation correlates with horizontal pleiotropy in complex traits}{10}{0}{1}
\beamer@subsectionintoc {1}{10}{Summary and Caveats}{11}{0}{1}
\beamer@subsectionintoc {1}{11}{Acknowledgements}{12}{0}{1}
