\documentclass{article}

\usepackage{fancyhdr}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{qtree}
\usepackage{pgf}
\usepackage{tikz, calc}
\usetikzlibrary{arrows,automata}
\usetikzlibrary{shapes.multipart}
\usepackage[latin1]{inputenc}
\usepackage{verbatim}
\usepackage{fullpage}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{courier}
\usepackage{makecell}
\usepackage{indentfirst}
\usepackage[headsep=.5cm,headheight=0cm]{geometry}
\usepackage{mathtools}
\usepackage{tcolorbox}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\diag}{\text{diag}}
\begin{document}

\title{Lab meeting discussion on three paper}
\author{Yanyu Liang}
\maketitle

For the reference, the three papers are: \cite{barfield2018transcriptome}, \cite{mancuso2017probabilistic}, and \cite{park2017bayesian}.

The main goal of these papers is trying to integrate eQTL result with GWAS result to identify gene whose expression affects GWAS pheontype.

\section{Multi-gene model instead on single-gene model \cite{mancuso2017probabilistic}}

I want to start with \cite{mancuso2017probabilistic} because it seems to be the most straightforward one to me. As we all know, TWAS/PrediXcan test the association between the predicted gene expression and GWAS outcome one gene at a time. One issue about this approach is that many genes may be in high LD with each other, which makes their predicted expression levels are highly correlated. In such case, even if there is only one causal gene (in the sense of expression affects phenotype), many genes in high LD also show significant signal. In other word, if we treat all genes as independent, the summary statistic is inflated for sure.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\textwidth]{twas}
    \caption{Figure 1 of \cite{mancuso2017probabilistic}}
    \label{fig:twas}
\end{figure}

\subsection{Model}

One way to tackle this problem is to use multi-gene model instead (which is similar to the idea of fine-mapping in GWAS). The idea of this paper is to derive the likelihood of $\vec{z}_{twas}$ which depends on true effect of each gene $\vec{\alpha}$. To make it specific, the model they use is

\begin{align*}
    G &= XW + E \\
    \vec{y} &= G\vec{\alpha} + \vec{\epsilon} \quad, \vec{\epsilon} \sim N(0, \sigma_e^2 I)
\end{align*}
, where $X \in \mathbb{R}^{n \times p}, W \in \mathbb{R}^{p \times m}, G \in \mathbb{R}^{n \times m}$ so that $m$ is the number of gene in the candidate region. 

Under this model, 

\begin{align*}
    z_{twas, i} &:= \hat{b}_i / se(\hat{b}_i) \\
    \hat{b}_i &:= \hat{G}_i' \vec{y} / \hat{G}_i'\hat{G}_i \\
    \hat{G} &:= X \hat{W} \\
    \E(\vec{z}_{twas}) &= \frac{\sqrt{n}}{\sigma_e} S^{-1} \hat{W}' \Sigma_{snp} \hat{W} S^{-1} \vec{\alpha} \\
    &:= \Sigma_{pe} \vec{\lambda} \\
    \Var(\vec{z}_{twas}) &= S^{-1} \hat{W}' \Sigma_{snp} \hat{W} S^{-1} \\
    &:= \Sigma_{pe}
\end{align*}
, where $S := \diag(\|\hat{G}_i\|)$, $\Sigma_{snp}$ is LD matrix.

After deriving the likelihood of the observed summary statistic (\textit{i.e.} TWAS z-scores), the paper took Bayesian inference idea where a 'sparse' prior is assigned to $\alpha_i$ ($\lambda_i$). 

\begin{align*}
    \lambda_i | c_i, \sigma_c^2 &\sim \begin{cases}
    		N(0, n \sigma_c^2) & , c_i = 1 \\
    		N(0, \delta) & , c_i = 0
    		\end{cases} \\
    	c_i &\sim Bernoulli(p)
\end{align*}
, where $c_i$ indicates whether gene $i$ is causal (by expression) or not. By integrating out $\vec\lambda$, $\vec{z}_{twas}|\Sigma_{pe}, \vec{c}, \sigma_c^2 \sim N(0, \Sigma_{pe} + \Sigma_{pe} D_{\vec{c}} \Sigma_{pe})$.

\subsection{Inference}

In practice, $n\sigma_c^2$ is fixed to be 13 and $p = 1 / m$, \textit{i.e.} on average one gene per candidate region. $\Sigma_{snp}$ is available from reference panel. And $\hat{W}$ is obtained from eQTL study. 

The quantity of interest is $\Pr(c_i = 1 | \vec{z}_{twas}, \Sigma_{pe}, \sigma_c^2):= PIP_i$. The computation of PIP is simply

\begin{align*}
	\Pr(c_i = 1 | \vec{z}_{twas}, \Sigma_{pe}, \sigma_c^2) &= \sum_{\vec{c}': c'_i = 1} \Pr(\vec{c}' | \vec{z}_{twas}, \Sigma_{pe}, \sigma_c^2) \\
	\Pr(\vec{c} | \vec{z}_{twas}, \Sigma_{pe}, \sigma_c^2) &= \frac{\Pr(\vec{z}_{twas} | \Sigma_{pe}, \sigma_c^2, \vec{c}) \Pr(\vec{c})}{\sum_{\vec{c}'} \Pr(\vec{z}_{twas} | \Sigma_{pe}, \sigma_c^2, \vec{c}') \Pr(\vec{c}')}
\end{align*}

\subsection{Discussion}

The model does not consider pleiotropic effect (unmediated effect).

\section{Modeling pleiotropic effect \cite{park2017bayesian} (CaMMEL)}

In the previous paper \cite{mancuso2017probabilistic}, unmediated effect is not considered, which means that for a variant, its GWAS effect is only explained by changing the transcription level of a particular gene (or a set of genes). But a variant may have some effect which is not mediated by the particular gene expression or even make difference through other regulatory pathways (\textit{e.g.} genomic state, post-transcriptional regulation, post-translational regulation). \cite{park2017bayesian} tackles this problem by incorporating unmediated effect into the model.

\subsection{Model}

\begin{align*}
	\vec{y} &\sim N(G\vec\theta, \sigma^2I) \\
	\vec\theta &= \sum_{k = 1}^K \beta_k \vec\alpha_k + \vec{\gamma}
\end{align*}
, $G$ is genotype matrix, and $\vec\theta$ is the true effect size of a set of SNP on GWAS phenotype. $\vec\alpha_k$ is the effect of the set of SNP on the expressino of gene $k$, and $\beta_k$ is the effect of the expression of gene $k$ on GWAS phenotype. $\vec{\gamma}$ is the unmediated effect of the set of SNP on GWAS phenotype (the effect which does not go through the expression of the set of genes).

The Bayesian idea is to assign a normal prior on $\vec\gamma$ and a sparse prior on $\vec\beta$ so that gene level PIP can be obtained to indicate whether a gene is causal.

To make use of summary statistic data, the paper proposed the following likelihood.

\begin{align*}
	\hat\theta &\sim N(SRS^{-1} \sum	_{k = 1}^K \hat\alpha_k \beta_k + \vec\gamma, SRS)
\end{align*}
, where $S := \diag(\E(\hat\theta_j^2))$ and $R$ is LD matrix.

This likelihood comes from the RSS model which relates GWAS summary statistic with true effect under a multi-SNP linear model ($\hat\theta \sim N(SRS^{-1}\vec\theta, SRS)$). 

\begin{tcolorbox}
\subsubsection*{Side note}
\begin{align*}
	\E(\hat\theta) &= SRS^{-1} \theta = SRS^{-1} (\sum_{k = 1}^K \beta_k \vec\alpha_k + \vec{\gamma}) \\
	&= SRS^{-1} \sum_{k = 1}^K \beta_k S_\alpha R^{-1} S_\alpha^{-1} \E(\hat\alpha) + SRS^{-1} \vec{\gamma} \\
	&\approx \sum_{k = 1}^K \hat\alpha_k \beta_k + SRS^{-1} \vec\gamma
\end{align*}
The last line is valid if $S \approx c S_{\alpha}$ and $\E(\hat{\alpha}) \approx \hat\alpha$. 
\end{tcolorbox}

The main idea here is to make the connection between single-SNP model and multi-SNP model in both GWAS effect $\vec{\theta}$ and eQTL effect $\vec\alpha_k$. 

\subsection{Inference}

The inference problem is to obtain posterior distribution of $\vec{\beta}$ and $\vec\gamma$. Spike-and-slab prior is used for $\beta_k$ and normal prior is used for $\vec\gamma$. The inference is done by black box variational inference. The FDR is  of $\beta$ is computed by bootstrap. 

\subsection{Discussion}

Under simulation using ROSMAP genotype, CaMMEL is compared with TWAS and MR-Egger (see \cref{fig:twas}). 

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.95\textwidth]{cammel}
    \caption{Figure S1 of \cite{park2017bayesian}}
    \label{fig:twas}
\end{figure}

The paper discussed a little bit of adjusting reverse causality. To account for it, it is better to regress out potential confounding from gene expression. But it is unclear for me on how it is done. The only thing they mentioned in detail is to adjust for unobserved genetic effect (outside cis-eQTL window) on expression by half-sibling regression, which, I guess, has nothing to do with reverse causality adjustment. 

\section{Fixing LD issue in MR-Egger \cite{barfield2018transcriptome}}

Mendelian randomization (MR) tests the mediated effect under the situation where the expression of a gene mediates the effect of a set of SNP on a phenotype without considering pleiotropic effect.

The idea of MR-Egger \cite{bowden2015mendelian} is to relax the assumption on pleiotropic effect. But it works best when SNPs are independent. \cite{barfield2018transcriptome} proposed LD-aware MR-Egger to account for this short-come.

\subsection{Model}

\begin{align*}
	g(\E(\vec{Y}|\vec{M}, G) &= \gamma_0 + \vec{M} \gamma + G \vec\theta \\
	\vec{M} &= \beta_0 + G \vec{\beta}_E + \vec\epsilon_M \quad, \vec\epsilon_M \sim N(0, \sigma^2I) 
\end{align*}
, where $\vec{M} \in \mathbb{R}^{n \times 1}, \vec{Y} \in \mathbb{R}^{n \times 1}, G \in \mathbb{R}^{n \times p}$ 

The marginal effect of genotype on phenotype (GWAS) is

\begin{align*}
	g(\E(\vec{Y} | G)) &= \gamma_0 + G \vec{\beta})_G
\end{align*}

Under some conditions on $g$ or $\vec{Y}$, $\vec\beta_G \approx \vec\beta_E \gamma + \vec\theta$.

With normalized genotype, $\hat\beta_G = \Sigma^{-1} \hat\beta_G^*, \hat\beta_E = \Sigma^{-1} \hat\beta_E^*$. Then, 

\begin{align*}
	\hat\beta_G &\approx \vec\theta + \vec\beta_E \gamma + \vec\epsilon_G \quad, \vec\epsilon_G \sim N(0, \Sigma_G) \\
	\Sigma_G &= \Sigma^{-1} (\Sigma \cdot \vec\sigma_G^* \vec\sigma_G^{*'}) \Sigma^{-1}
\end{align*}

\subsection{Hypothesis test}

Different from the approaches taken by the above two papers, this paper constructed hypothesis test for testing whether $\gamma$ is zero. 

\subsubsection{TWAS}

\begin{align*}
	Z_{twas} &= \frac{\hat\beta_E^{*'} \Sigma^{-1} Z_G^*}{\sqrt{\hat\beta_E^* \Sigma^{-1} \hat\beta_E^*}}
\end{align*} 

\subsubsection{MR estimate}

\begin{align*}
	\hat\gamma_{MR} &= \frac{\hat\beta_E^{*'} V^{-1} \hat\beta_G^*}{\sqrt{\hat\beta_E^* V^{-1} \hat\beta_E^*}}
\end{align*}

If $\Sigma = I$ and $\vec\theta = 0$, $\hat\gamma_{MR}$ is unbiased (just the weighted linear regression estimate without intercept). 

\subsubsection{MR-Egger estimate}

Suppose $\vec\theta = \alpha I$ and $\Sigma = I$, the estimate of $\gamma$ is $\hat\gamma_{MR-Egger}$ being the weighted linear regression estimate with intercept.

\subsubsection{LDA MR estimate}

Instead of assuming $\Sigma = I$ and performing weighted linear regression, $\hat\gamma_{LDMR}$ is the estimate of linear regression with error covariance $\Sigma_G$. It is very similar to TWAS.

\subsubsection{LDA MR-Egger estimate}

Similarly, the assumption on $\Sigma = I$ in MR-Egger is dropped here.

\subsection{Type I error and power}

The paper considered three scenarios about pleiotropic effect: 

\begin{enumerate}
	\item No direct effect
	\item Variable direct effect ($\theta_i$ is highly variable)
	\item Directional pleiotropy ($\theta_i$'s are close to equal)
\end{enumerate}

Also the simulation was done under low and strong LD.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.95\textwidth]{t1e}
    \caption{Figure 1 of \cite{barfield2018transcriptome}}
    \label{fig:twas}
\end{figure}

If InSIDE condition is violated (pleiotropic effect correlated with eQTL association), type I error is not well controlled.

In terms of power, there is no big difference between variants of MR and variants of MR-Egger (just slightly weaker).

\subsection{Discussion}

To what extend directional pleiotropy is hold? It sounds like a strong condition though. It seems to me that \cite{park2017bayesian} is built on top of this framework with a more careful handling on $\vec\theta$. 

If I remember it correctly, TWAS assessed significance via permutation instead of asymptotic distribution to account of undesired correlation between GWAS z-score. My feeling is that such correlation also shows up in this paper. 

\bibliography{mybib}{}
\bibliographystyle{plain}
\end{document}
