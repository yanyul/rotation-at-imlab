\global \c@tocdepth 1\relax 
\beamer@sectionintoc {1}{Main}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Co-regulation and tissue-shared eQTL}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Capturing local co-regulation}{3}{0}{1}
\beamer@subsectionintoc {1}{3}{Capturing tissue-specificity}{4}{0}{1}
\beamer@subsectionintoc {1}{4}{FLASH factors captures tissue-sharing pattern}{5}{0}{1}
\beamer@subsectionintoc {1}{5}{Co-regulation is common along the genome}{6}{0}{1}
\beamer@subsectionintoc {1}{6}{Co-regulating variant tends to target multiple genes in all tissues}{7}{0}{1}
\beamer@subsectionintoc {1}{7}{GWAS catalog variants tend to have lower co-regulaton level}{8}{0}{1}
\beamer@subsectionintoc {1}{8}{DAPG variants with higher $P_n$ have stronger GWAS signal}{9}{0}{1}
\beamer@subsectionintoc {1}{9}{GWAS catalog variants tend to have more tissue-shared eQTL}{10}{0}{1}
\beamer@subsectionintoc {1}{10}{Tissue-shared DAPG eQTLs have stronger GWAS signal}{11}{0}{1}
\beamer@subsectionintoc {1}{11}{Plan}{12}{0}{1}
