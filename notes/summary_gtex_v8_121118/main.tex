\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{courier}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\diag}{\text{diag}}

\newcommand{\beginsupplement}{%
        \setcounter{table}{0}
        \renewcommand{\thetable}{S\arabic{table}}%
        \setcounter{figure}{0}
        \renewcommand{\thefigure}{S\arabic{figure}}%
     }

\begin{document}

\title{Summary on Co-regulation Analysis and eQTL Tissue-Specificity for GTEx V8}
\author{Yanyu Liang}
\maketitle

\tableofcontents

\section{Introduction}

If a variant is causal in more than one trait, it is called pleiotropy. It has been found that pleiotropy is common in complex trait and, in particular, horizontal pleiotropy, a variant affecting two traits via independent pathways rather than mediation, is widespread \cite{jordan2018landscape}. Here, we are interested in the pleiotropy in gene expression which, as molecular traits, has very different genetic architecture comparing to complex phenotypes. We captured the local pleiotropy for gene expression traits by making use of GTEx V8 \textit{cis}-eQTL calls followed by multi-tissue analysis to count the number of genes a variant is regulating for in the \textit{cis}-regulatory window (1Mb). The local pleiotropy of gene expression is approximately local horizontal pleiotropy, referred as co-regulation, since the mediated regulation is likely due to genes outside \textit{cis}-window. Regarding the significance of co-regulation to complex trait, gene expression is thought to be mediator of complex trait so co-regulation is the indicator (on average) of either the number of mediations or the chance to regulate a causal gene. In the following text, we characterized the co-regulation pattern and examined the relationship between co-regulation level and complex trait effect size/horizontal pleiotropy.

The eQTL analysis is performed tissue by tissue and many variant-gene pairs have been found to be eQTL in some tissues but not in other tissues. On the other hand, some tissues are likely to share the same eQTL. These evidences indicate that there are tissue-sharing patterns underlying cross-tissue eQTL data. So, we are interested in exploring such tissue-sharing pattern of eQTL in GTEx V8. Tissue-specificity pattern of gene expression regulation has been implicated in complex trait genetics. For instance, genes that are regulated in a small set of tissues have been found to be enriched for disease association \cite{gtex2017genetic} whereas tissue-shared eQTLs contribute more to complex trait association \cite{gamazon2018using}. Indeed, tissue-specificity is an indicator of either the number of tissues that eQTL can affect in or the chance to act in the causal tissue. Here, we characterized the eQTL tissue-sharing pattern by matrix factorization analysis and examined the relationship between eQTL tissue-specificity and complex trait effect size/horizontal pleiotropy.

\section{Results}

\subsection{Overview of co-regulation} \label{sec:coreg}

To explore the co-regulation pattern, we focused on a set of independent variants which were detected as DAPG eQTLs (with posterior inclusion probability $>$ 0.05, \Cref{sec:dapg_variant}) in at least one of the 49 tissues. For each DAPG variant, we captured the level of co-regulation (\textit{i.e.} the level of horizontal pleiotropy for local gene expression traits) by counting the number of genes that are regulated by the variants in \textit{cis-}window (referred as $P_{n}$ below). To account for the fact that tissue with larger sample size has higher power to detect eQTL, we used the local false sign rate (LFSR) output by \texttt{mashr} \cite{Urbut2018}, which obtains posterior effect size of variant-gene pair by pooling information across all tissues, to call significance (LFSR $< 0.05$). After pruning with $r^2 < 0.1$ and post-filtering eQTLs with non-significant LFSR in all tissues, we obtained 33k independent DAPG variants out of 357k variants from DAPG call across all tissues.

\subsubsection{Co-regulation is commonly found}

Among all these DAPG variants that are regulating at least one gene according to \texttt{mashr} LSFR ($\sim$ 25k aggregated across all tissues), the median percentage of these DAPG variants to be significantly associated with the expression of multiple genes in single tissue was $\sim$ 57\% and the percentage rose to $\sim$ 66\% if aggregating $P_n$ across all tissues (\Cref{fig:coreg_overview_percentage}). Around $\sim$ 4\% DAPG variants had more than 10 target genes aggregated across tissues (\Cref{fig:coreg_overview_pn}) and the co-regulation were detected genome-wide (\Cref{fig:coreg_overview_chr}). These evidences indicated that the existence of co-regulation is universal across the genome.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig1_Pn_of_eQTL/fig1_3_DAPGvariant-active-pruned.png}
    \caption{}
    \label{fig:coreg_overview_percentage}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig1_Pn_of_eQTL/fig1_1_DAPGvariant-active-pruned.png}
    \caption{}
    \label{fig:coreg_overview_pn}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig1_Pn_of_eQTL/fig1_4_DAPGvariant-active-pruned.png}
    \caption{}
    \label{fig:coreg_overview_chr}
  \end{subfigure}
  \caption{Overall distribution of $P_n$ in DAP-g variants}
  \label{fig:coreg_overview_1}
\end{figure}

\subsubsection{Co-regulating variant tends to target multiple genes in all tissues}

Among the DAPG variants that targeted multiple genes in at least one of the 49 tissues ($\sim$ 22k in total), about 18\% of of them targeted multiple genes across all 49 tissues (\Cref{fig:coreg_cross_tissue_hist}). To explore the co-regulation pattern across tissues, we defined binarized $P_n$ by assigning zero to $P_n \le 1$ and one to $P_n > 1$ to indicate whether the variant was targeting multiple genes in a given tissue. The binarized $P_n$ vector across 49 tissues was assigned to tissue-sharing pattern (induced by \texttt{flashr} analysis on strong eQTLs) or one-tissue-specific category (\Cref{sec:ts_mf}). The all-tissue category (\texttt{flashr} Factor1) was assigned most and there were about 68\% of DAPG variants failed to assigned to any \texttt{flashr} induced pattern or single-tissue pattern (\Cref{fig:coreg_cross_tissue_cate}). Besides Factor1, others factors from \texttt{flashr} analysis was (multi)-tissue-specific (\Cref{fig:supp_flash}), which explained the pattern of $\sim$ 1.9k variants. But these factors along with single-tissue category explained only 9\% of co-regulating DAPG variants. The leftover unexplained patterns fell in-between tissue-specific and all-tissue categories which were not captured by \texttt{flashr} analysis. Presumably, it was due to the fact that these unexplained patterns were not as generic as the one detected by \texttt{flashr}.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig3_Pn_across_tissue/fig3_2_DAPGvariant-active-pruned.png}
    \caption{}
    \label{fig:coreg_cross_tissue_hist}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig3_Pn_across_tissue/fig3_1_DAPGvariant-active-pruned.png}
    \caption{}
    \label{fig:coreg_cross_tissue_cate}
  \end{subfigure}
\end{figure}

\subsubsection{Co-regulating variant is enriched in promoter}

To survey the pattern of co-regulating variant in regulatory elements, we created the genomic functional annotation by collapsing experimentally determined annotation (by high-throughput sequencing) across the Ensembl regulatory build \cite{zerbino2015ensembl} of a set of cell types matched with GTEx V8 tissues (FIXME: Not exactly sure). The fraction of variants that fell into the functional annotation were compared among DAPG variant targeting single gene, DAPG variant targeting multiple genes, and two baselines, a randomly drawn set of GTEx v8 variants and a set of variants that were called as eQTL by DAPG (PIP $> 0.05$) with non-significant LFSR (this set of variants controlled for the bias introduced by selecting variants by DAPG) (\Cref{fig:coreg_func}). Active variants were enriched in enhancer and promoter flanking regions and, furthermore, co-regulating variants were more likely to locate within promoter but less likely to be in open chromatin region. Moreover, co-regulating variants located closer to the transcription start site of the regulated genes (median distance $\sim$ 16kb as comparing to $\sim$ 61kb for variants targeting single gene), which were consistent with the enrichment of co-regulating variant in promoter region.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}[b]{0.6\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig5_Pn_vs_functional_annotation/fig5_aggregated_DAPGvariant-active.png}
    \caption{}
    \label{fig:coreg_func}
  \end{subfigure}
\end{figure}

\subsection{Co-regulation and complex trait}

Here, we compared $P_n$ in DAPG variants with GWAS variants that were covered by GTEx v8. Also, we examined how co-regulation level is correlated with GWAS signal (in terms of effect size and significance) among all active (eQTL by MASHR) independent (pruned with $r^2 < 0.1$) DAPG variants in 114 harmonized GWAS data sets.

\subsubsection{GWAS catalog variants tend to have higher co-regulation level} \label{sec:coreg_gwas_catalog}

Among $\sim$ 55k GWAS catalog variants, $\sim$ 44k of them were GTEx variants. For these GTEx v8 covered GWAS variants, $\sim$ 63\% of them were detected as marginally significant eQTL (FDR $<$ 0.05) in at least one tissue and $\sim$ 73\% were active based on \texttt{mashr} LFSR $<$ 0.05 thanks to the increased power of calling eQTL by pooling information across tissues.

To compare the co-regulation level at GWAS loci to genome-wide background, we compared the distribution of $P_n$ for a list of independent GWAS variants (pruning with $r^2 < 0.1$) against the list of independent DAPG variants. We binned their $P_n$ into four categories: i) $P_n = 1$, ii) $P_n \in [1, 5]$, iii) $P_n \in [6, 10]$, iv) $P_n > 10$ and compared the histogram of $P_n$ categories between GWAS variants and DAPG variants (\Cref{fig:coreg_gwas_dist}). We obtained similar result as \cite{gamazon2018using} Figure 2a on DAPG variants (median 2 $\pm$ 3.60) with the same median but slightly higher variance on the number of target genes for active GWAS variants (median 2 $\pm$ 3.95).
This result indicated that the distribution of $P_n$ for GWAS catalog variants shift towards higher value comparing to DAPG variants and active GWAS variants were more likely to target multiple genes (one-side Fisher exact test, 1.09 ($4.2 \times 10^{-5}$))

\begin{figure}[ht!]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig1_Pn_of_eQTL/fig1_1_compare_to_gwas_DAPGvariant-active-GWAS-prune.png}
    \caption{}
    \label{fig:coreg_gwas_dist}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig7_Pn_regression/fig7_both2_DAPGvariant-prune-new.png}
    \caption{}
    \label{fig:coreg_gwas_regress}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig4_Pn_vs_trait_pleiotropy/fig4_2_DAPGvariant.png}
    \caption{}
    \label{fig:coreg_trait_pleio}
  \end{subfigure}
\end{figure}

\subsubsection{Co-regulating variants contribute more to GWAS signals}

Since GWAS variants were enriched for variants with high co-regulation level comparing to a co-regulation baseline (DAPG variants), we explored the relationship between co-regulation level and GWAS signal for 114 harmonized complex traits by fitting the following linear models using all independent active DAPG variants with variants inside MHC region being removed.
\begin{align}
  |\beta_{gwas}| &\sim P_n + LDSC + 1 \label{eq:pn_beta} \\
  z_{gwas}^2 &\sim P_n + LDSC + 1 \label{eq:pn_z}
\end{align}
$LDSC$ was introduced as covariate to adjust for the fact that LD is a confounder of both GWAS signal and $P_n$. The distributions of the estimated slopes for \Cref{eq:ts_z} among all traits were shifted towards right (\Cref{fig:coreg_gwas_regress}) whereas the slopes for \Cref{eq:ts_beta} were centered around zero (if include MHC region, some immune related phenotypes also showed significantly positive slopes). It indicated that co-regulation level was positively correlated with GWAS association for many complex traits, which was consistent with the enrichment result in \Cref{sec:coreg_gwas_catalog}. But we did not observe a clear trend of increased effect size across all traits, but for a set of body size traits (height, BMI, etc), hypertension, and blood cell count traits, the slopes were significantly positive (p-value $< 2.5 \times 10^{-7}$), which indicated that in these traits, variants targeting more genes tended to have larger effect size.

\subsubsection{Co-regulation level is positively correlated with trait horizontal pleiotropy}

Co-regulation as defined here is a special case of trait pleiotropy. It has been shown that the level of trait pleiotropy is significantly higher in loci that are targeting many genes than in others \cite{jordan2018landscape}. Here, we compared the trait pleiotropy score (for a set of 367 significantly heritable traits from UK Biobank, provided by Ron Do's lab) to the co-regulation $P_n$ on a set of DAPG variants (not necessarily independent) that were included in trait pleiotropy dataset. The level of co-regulation was positively correlated with trait pleiotropy (\Cref{fig:coreg_trait_pleio}) which is consistent with the fold enrichment result reported in \cite{jordan2018landscape}.

\subsection{Overview of eQTL tissue-specificity}

For an eQTL (a variant-gene pair where the variant significantly regulates the gene in at least one tissue), the effect size across all the 49 tissues are available thanks to the inclusion of a broad range of tissues in GTEx. We are interested in how the eQTL effect varies from tissue to tissue. Particularly, if a variant regulates the gene in the similar manner (similar effect size/direction) in (almost) all tissues, the corresponding eQTL is referred as "tissue-shared" eQTL; and likewise, if an eQTL only acts (and gets detected) in one tissue or a subset of tissues, it is referred as "tissue-specific" eQTL. Here, we explored the tissue-specificity pattern of a set of independent active DAPG variants (the same as \Cref{sec:coreg}) and examined the relationship between eQTL tissue-specificity and complex trait.

\subsubsection{The pattern of eQTL tissue-specificity by matrix factorization} \label{sec:ts_mf}

For a variant-gene pair $k$, the eQTL effect size in tissue $j$ is $\beta_{kj}$ and $\vec{\beta}_k = [\beta_{k1}, \cdots, \beta_{k,49}]^{'}$, as the eQTL vector across 49 tissues. The tissue-specificity pattern underlying eQTL vectors can be explained/defined as, for example, the variant-gene pair is likely to have similar effect size in some tissues (\textit{i.e.} the effect size in these tissues are not independent). A concrete example is that brain tissues share many eQTL which are not active outside brain. To extract such pattern, we applied \texttt{flashr}, a matrix factorization method based on Empirical Bayes \cite{wang2018empirical} to a random subset of strong eQTLs (16k) detected in single tissue analysis (\Cref{sec:flash}). We obtained 31 FLASH factors, each of which represented a strong tissue-sharing pattern underlying selected eQTLs (\Cref{fig:supp_flash}). An eQTL was assigned to one of the tissue-sharing pattern among all FLASH factors along with 49 single-tissue factors that best captured the tissue-sharing pattern of the eQTL (\Cref{sec:projection}).

\subsubsection{Half of the eQTLs are assigned to tissue-shared factor}

We extracted all the variant-gene pair with LFSR $< 0.05$ in at least one tissue for the set of active independent DAPG variants (110k eQTLs in total from 33k DAPG variants) and assigned them to the closest tissue-sharing pattern learnt from matrix factorization (\Cref{sec:projection}). About 51\% of the eQTLs were assigned as "Shared factor", and 25\% were assigned as "Brain factors" along with 0.5\% were assigned as "Blood factor" (\Cref{fig:ts_hist}, \Cref{fig:ts_hist2}). It indicated that half of the observed eQTLs were shared across tissues, and moreover, for an eQTL detected in a tissue, it had more than 50\% probability to be detected in other tissues.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig8_eqtl_tissue_specificity/fig8_1_dapg_eqtlDAPGvariant-active.png}
    \caption{}
    \label{fig:ts_hist}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig8_eqtl_tissue_specificity/fig8_2_dapg_eqtlDAPGvariant-active.png}
    \caption{}
    \label{fig:ts_hist2}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/misc/dapg_tss_fig1.png}
    \caption{}
    \label{fig:ts_tss}
  \end{subfigure}
\end{figure}

\subsubsection{Tissue-specific eQTL is closer to TSS}

We examined the distance between variant and TSS of the regulated gene for each eQTL. As the distance increased, the fraction of tissue-shared eQTL increased dramatically (\Cref{fig:ts_tss}). It indicated that distal regulation is more likely to be tissue-shared and tissue-specific regulation is enriched in region nearby TSS (up/downstream gave similar trend \Cref{fig:supp_ts_tss}).

\subsection{eQTL tissue-specificity and complex trait}

To explore relationship of eQTL tissue specificity and complex trait, we compared the tissue-sharing pattern distribution of GWAS variants against the DAPG variants. And examined how GWAS signal is correlated with eQTL tissue specificity status.

\subsubsection{GWAS catalog variants are enriched for tissue-shared eQTLs}

All of the eQTLs with LFSR $<$ 0.05 were extracted for a set of independent GWAS catalog variants covered by GTEx v8 and were assigned to a tissue-sharing pattern described in \Cref{sec:ts_mf}. 52\% of GWAS eQTLs were tissue-shared which was modestly but significantly higher than DAPG eQTLs (Fisher exact test, 1.04 (6.9 $\times 10^{-4}$); \Cref{fig:ts_gwas}). Consistently, GWAS variants were more likely to have tissue-shared eQTLs as compared to DAPG variants (Fisher exact test, 1.15 ($1.7 \times 10^{-10}$); \Cref{fig:ts_gwas_byvar}) and depleted for variants with tissue-specific eQTLs (Fisher exact test, 0.81 ($< 2.2 \times 10^{-16}$); \Cref{fig:supp_ts_gwas_fnot1}). This modest enrichment was likely to be driven by the fact that eQTLs at GWAS catalog loci were, on average, further away from target gene \cite{gamazon2018using}.

\begin{figure}[ht!]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig8_eqtl_tissue_specificity/fig8_1_compare_to_gwas_DAPGvariant-active-GWAS-prune.png}
    \caption{}
    \label{fig:ts_gwas}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig8_eqtl_tissue_specificity/fig8_2_compare_to_gwas_DAPGvariant-active-GWAS-prune.png}
    \caption{}
    \label{fig:ts_gwas_byvar}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig6_cross_tissue_eqtl_regression/fig6_both2_DAPGvariant-prune-new.png}
    \caption{}
    \label{fig:ts_regression}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{../../analysis/fig10_cross_tissue_vs_trait_pleiotropy/fig10_1_DAPGvariant.png}
    \caption{}
    \label{fig:ts_trait_pn}
  \end{subfigure}
\end{figure}

\subsubsection{Variants with more tissue-shared eQTLs tend to have larger GWAS effect size}

To examin how tissue-sharing pattern of eQTL is correlated with GWAS signal, we run the following two linear models.
\begin{align}
  |\beta_{gwas}| &\sim f_1 + LDSC + 1 \label{eq:ts_beta} \\
  z_{gwas}^2 &\sim f_1 + LDSC + 1 \label{eq:ts_z}
\end{align}
, where $f_1$ is an indicator variable of whether the variant had tissue-shared eQTLs. We calculated the slope of $f_1$ using the set of independent active DAPG variants with variants in MHC region being removed for 114 harmonized GWAS data sets. 97 out of 114 traits had significantly positive slopes for \Cref{eq:ts_beta} but, in contrast, only 35 out of 114 traits had significantly positive slopes for \Cref{eq:ts_z} with a set of blood cell count traits and height falling into this case (\Cref{fig:ts_regression}. It suggested that the variants with tissue-shared eQTLs were enriched for larger phenotypic effect size but the trend of enriched complex trait association signal was much weaker.

\subsubsection{Variants with tissue-shared eQTLs is more likely to have higher level of trait pleiotropy}

We compared the distribution of trait pleiotropy $P_n$ (for a set of 367 significantly heritable traits from UK Biobank) between DAPG variants with and without tissue-shared eQTLs (\Cref{fig:ts_trait_pn}). Variants with at least one tissue-shared eQTL had significantly higher trait pleiotropy $P_n$ than those without tissue-shared eQTL (two-sample Kolmogorov-Smirnov test, p-value $< 2.2 \times 10^{-16}$).

\section{Discussion}

Co-regulation is widespread along the genome with 66\% of regulatory variants targeting more than one gene. A large proportion of regulatory variants are co-regulating in almost all tissues but the tissue-sharing pattern of co-regulation is not well-aligned with eQTL tissue-specificity (68\% of co-regulation pattern cannot be explained by tissue-sharing pattern obtained from eQTL tissue-specificity analysis). Comparing to variants targeting single gene, co-regulating variants are enriched in promoter and depleted in open chromatin region and locate, on average, closer to TSS. Overall, co-regulating variants contribute more to complex trait association but the positive correlation between co-regulation and effect size was only observed in some well-powered body size traits and blood cell count traits. Consistent with \cite{jordan2018landscape}, we observed positive correlation between co-regulation level and trait horizontal pleiotropy. It suggested that the widespread co-regulation was likely to be a molecular mechanism underlying the widespread complex trait horizontal pleiotropy.

About half of eQTLs were assigned as tissue-shared in our analysis. It indicated that a large proportion of eQTL were shared by tissues, meaning that the detection of eQTL in one tissue is likely transferable to another tissue. Comparing to tissue-shared eQTL, the regulatory distance of tissue-specific eQTL was smaller but we also observed a depletion of tissue-specific eQTL in promoter region (data not shown) so the signal was not driven by promoter (local enhancer?). Though GWAS catalog loci had similar but slightly higher percentage of tissue-shared eQTLs comparing to DAPG loci, GWAS variants were enriched for the ones with tissue-shared variants. Also, variants with tissue-shared eQTLs tended to have significantly higher complex trait effect size in most of the tested traits (97 out of 114) even though the trend for squared z-score was less clear. These evidences suggested that tissue-shared eQTL is important in complex trait genetics with increased contribution to GWAS association signals. Furthermore, variants with tissue-shared eQTL had higher level of trait horizontal pleiotropy on average. This result indicated that the same gene acting in different tissues might potentially lead to different complex phenotypes.

\section{Methods}

\subsection{FLASH} \label{sec:flash}

FLASH was run on a random subset of strong eQTLs (16k in total; one eQTL per gene?) with marginal effect size across 49 tissues, which resulted in a $16000 \times 49$ data matrix $X$ as input. The mixture component of the prior for factor and loading were set to be uniform and normal to ensure sparse non-negative entries for factors and sparse loadings. FLASH was trained by first greedily adding new factors until meeting stop criteria and then backfit was performed to fine-tune parameters. The matrix factorization output by FLASH was $\sum_{k = 1}^{31} = d_k \vec{f}_k \vec{l}_k' = \tilde{X}$ with normalized $\vec{f}_k, \vec{l}_k$.

\subsection{Multi-tissue eQTL analysis by MASH}

MASH model was trained on a random subset of significant eQTLs (40k) with marginal effect size and standard error as input. The mixture component of the prior, \textit{i.e.} correlation matrix $U_k$, included data-driven correlation matrix generated by PCA (by \texttt{cov\_pca}) and FLASH (\Cref{sec:flash}) following by de-noise step using extreme deconvolution (by \texttt{cov\_ed}). Also, a set of canonical correlation structures were included (by \texttt{cov\_canonical}), such as single-tissue (singleton), perfect correlation (all tissues have equal effect size), etc. As an empirical Bayes framework, during the training phase, the mixture proportion of the prior was learned from the data. At inference phase, for any $49 \times 1$ eQTL vector $\vec\beta$, the trained MASH model was applied to obtain the posterior mean, posterior standard deviation, and local false sign rate.

\subsection{DAPG variants} \label{sec:dapg_variant}

DAPG was run in gene-by-gene and tissue-by-tissue manner on the \textit{cis-}window (1Mb) of the gene and variants with SNP level posterior inclusion probability (PIP) greater than 0.05 from each variant cluster were retained as eQTL of the corresponding gene in a given tissue. The list of independent active DAPG variants were created by the following steps.
\begin{enumerate}
  \item Collect all the variants that were involved in the DAPG fine-mapping eQTLs across all tissues as DAPG variants
  \item Obtain the marginal effect size estimates across 49 tissues (\textit{i.e.} obtain effect size vector) of all variant-gene pairs for these DAPG variants
  \item Feed these effect size vector into MASH model to compute posterior estimates
  \item Extract active DAPG variants by removing the variant with no any eQTL having LFSR $< 0.05$
  \item Prune the active DAPG variants using \texttt{plink --indep-pairwise} with window size 100 variants, step size 10 variants, and $r^2$ threshold 0.1.
\end{enumerate}

\subsection{Projection} \label{sec:projection}

For any eQTL vector $\vec{\beta}$, we obtained the projection-based tissue-sharing pattern among tissue-sharing factors (patterns), $\vec{f}_1, \cdots, \vec{f}_{31}$, learned from FLASH (\Cref{sec:flash}) by the following steps.
\begin{enumerate}
  \item Compute $b_k = \vec{\beta}' \vec{f}_k$ for $k = 1, \cdots, 31$
  \item Compute $PVE_k = \frac{b_k^2}{\|\vec{\beta}\|_2^2}$
  \item Assign to a pattern
  \begin{align*}
    membership &= \begin{cases}
      \arg\max_k PVE_k & \text{, if $\max(PVE) > 0.2$} \\
      \text{N.A.} & \text{, otherwise}
    \end{cases}
  \end{align*}
\end{enumerate}
\section{Supplementary Materials}
\beginsupplement


\begin{figure}[ht!]
  \centering
  \includegraphics[width=\linewidth]{../../docs/flash_fp_on_Pn_files/figure-html/unnamed-chunk-26-1.png}
  \caption{Factors from \texttt{flashr} analysis on strong eQTLs}
  \label{fig:supp_flash}
\end{figure}

\begin{figure}[t!]
  \centering
  \includegraphics[width=\linewidth]{../../analysis/fig8_eqtl_tissue_specificity/fig8_3_compare_to_gwas_DAPGvariant-active-GWAS-prune.png}
  \caption{Fraction of "tissue-shared" eQTL stratified by location relative to TSS}
  \label{fig:supp_ts_gwas_fnot1}
\end{figure}

\begin{figure}[t!]
  \centering
  \includegraphics[width=\linewidth]{../../analysis/misc/dapg_tss_fig2.png}
  \caption{Fraction of "tissue-shared" eQTL stratified by location relative to TSS}
  \label{fig:supp_ts_tss}
\end{figure}





\bibliography{mybib}
\bibliographystyle{apalike}

\end{document}
