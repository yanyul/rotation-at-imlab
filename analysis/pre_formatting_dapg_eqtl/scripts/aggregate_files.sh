out=$1
array=( $@ )
len=${#array[@]}
inputs=${array[@]:1:$len}
echo 'gene_id,variance_id,tss_distance,ma_samples,ma_count,maf,pval_nominal,Bhat,Shat' | tr ',' '\t' > $out.temp
cat $inputs >> $out.temp
gzip < $out.temp > $out
rm $out.temp
