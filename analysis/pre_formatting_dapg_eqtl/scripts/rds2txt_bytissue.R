library(optparse)

option_list = list(
  make_option(c("-i", "--input"), type="character", default=NULL,
              help="mashr output in RDS format", metavar="character"),
  make_option(c("-t", "--tissue"), type="character", default=NULL,
              help="name of output tissue", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="name of output text file", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

library(stringr)

mashr_out <- readRDS(opt$input)
row_name <- rownames(mashr_out$Bhat)
gene <- str_match(row_name, '([A-Z0-9]+)_[A-Z0-9_]+')[, 2]
chr <- paste0('chr', str_match(row_name, '[A-Z0-9]+_([A-Za-z0-9]+)_[A-Z0-9_]+')[, 2])
pos_alleles <- str_match(row_name, '[A-Z0-9]+_[A-Za-z0-9_]+_([0-9]+_[A-Z]+_[A-Z]+$)')[,2]
var_name <- paste0(chr, '_', pos_alleles, '_b38')
df <- data.frame(gene_id = gene, variant_id = var_name, tss_distance = -1, ma_samples = -1, ma_count = -1, maf = -1, pval_nominal = -1, Bhat = mashr_out$Bhat[, opt$tissue], Shat = mashr_out$Shat[, opt$tissue])
write.table(df, opt$output, row.names = F, quote = F, sep = '\t', col.names = F)
