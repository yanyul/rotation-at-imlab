tissues = read.table('../../../data/tissues.txt')


library(dplyr)
dapgs = data.frame()
for(t in tissues$V1) {
  message(t)
  sub = read.table(paste0('~/labshare/GTExV8/mediation_analysis_new_setting/splicing/extract_qtl_from_dapg/extracted-from-DAPG.cs_gt_0.25-x-pip_gt_0.01.', t, '.txt.gz'), header = T, stringsAsFactors = F) %>% mutate(tissue = t)
  dapgs = rbind(dapgs, sub)
}
dapg_eqtl = dapgs[ !duplicated(paste0(dapgs$gene, '--', dapgs$variant_id)), ]
write.table(dapg_eqtl, 'dapg_sqtl_eur_across_all_tissue.txt', col = F, row = F, quo = F, sep = '\t')
