gtex=$1
var=$2
out=$3
varcol=$4  # in list
genecol=$5  # in list
# var--gene

awk -v varcol=$varcol -v genecol=$genecol 'FNR==NR{a[$varcol"--"$genecol];next}($2"--"$1 in a)' $var <(zcat $gtex) | gzip > $out
