This module run stratified LDSC regression using 1000G reference panel

* Reference panel 1000G EUR [link](https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_plinkfiles.tgz)
* SNP list: HapMap3 [link](https://data.broadinstitute.org/alkesgroup/LDSCORE/w_hm3.snplist.bz2)
* Functional annotations: baselineLD_v1.1 (with MAF, LD, GERP annotations) [link](https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_baselineLD_v1.1_ldscores.tgz)
* Frequency files [link](https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_frq.tgz)
* QTL annotations: GTEx v8 (liftover back to hg19)


liftover chain file [link](http://hgdownload.cse.ucsc.edu/goldenpath/hg38/liftOver/hg38ToHg19.over.chain.gz)
