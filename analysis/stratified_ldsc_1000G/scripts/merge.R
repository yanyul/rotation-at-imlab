library(optparse)

option_list = list(
  make_option(c("-p", "--prefix"), type="character", default=NULL,
              help="prefix of results files", metavar="character"),
	make_option(c("-l", "--ldscname"), type="character", default=NULL,
              help="name of ldsc used to generate results files", metavar="character"),
  make_option(c("-t", "--traits"), type="character", default=NULL,
              help="list of traits separated by comma", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="name of output file", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

traits = strsplit(opt$traits, ',')[[1]]
df = data.frame()
for(t in traits) {
  filename = paste0(opt$prefix, t, '__', opt$ldscname, '.results')
  df_sub = read.table(filename, header = T, sep = '\t')
  df_sub$trait = t
  df = rbind(df, df_sub)
}
saveRDS(df, opt$output)
