# conda activate snmk
cd /mnt/disks/yanyu-ldsc/rotation-at-imlab/analysis/stratified_ldsc_1000G/data/qtl_list

# Rscript get_dapg_maxpip_cs.R --threshold 0.01 --output_prefix DAPG-maxpipCS-gt-0.01-0.95 --cs_threshold 0.95 --json ~/gtex-awg-im-6077f6f6b70b.json > ../../qsub/get_data/dapg_maxpipCS_gt_0.01_0.95_gcp.log 2>&1 
Rscript get_dapg_maxpip_cs.R --threshold 0.01 --output_prefix DAPG-maxpipCS-gt-0.01-0.95 --cs_threshold 0.95 --json ~/gtex-awg-im-6077f6f6b70b.json --sqtl TRUE > ../../qsub/get_data/dapg_maxpipCS_gt_0.01_0.95_gcp_another.log 2>&1
