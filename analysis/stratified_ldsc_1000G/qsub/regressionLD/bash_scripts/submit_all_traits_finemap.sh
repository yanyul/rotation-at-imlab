mkdir -p configs/
mkdir -p logs/

trait=($(ls /gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_* | sed 's#/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_##g'|sed 's#.txt.gz##g'))
for t in "${trait[@]}"
  do
    # echo $t
    qsub -v TRAIT=$t -o logs/$t\_finemap.out -N $t -e logs/$t\_finemap.err qtl_generic_finemap.qsub 
  done
