cd /mnt/disks/yanyu-ldsc/rotation-at-imlab/analysis/stratified_ldsc_1000G
TRAIT=$1

echo "other_inputs:
  name: '1000G-HapMap3-BaselineLDv1.1'
  w_ld: '/mnt/disks/yanyu-ldsc/data/1000G_Phase3_weights_hm3_no_MHC/weights.hm3_noMHC.'
  frq: '/mnt/disks/yanyu-ldsc/data/1000G_Phase3_frq/1000G.EUR.QC.'
  baseline: '/mnt/disks/yanyu-ldsc/data/baselineLD_v1.1/baselineLD.'
  snplist: '/mnt/disks/yanyu-ldsc/data/w_hm3.snplist'
QTL:
  name: 'GTExV8QTLandDAPG-maxpipCS-gt-0.05-0.95'
  prefix: 'output/QTL/MERGED.QTLandDAPG-maxpipCS-gt-0.05-0.95__1000G_EUR__HapMap3.'
  annot_prefix: 'output/QTL/MERGED.QTLandDAPG-maxpipCS-gt-0.05-0.95__1000G_EUR.'
GWAS:
  name: '$TRAIT'
  path: '/mnt/disks/yanyu-ldsc/processed_summary_imputation/imputed_$TRAIT.txt.gz'
  SNPID_name: 'variant_id'
  clean: False
ldsc_cmd: '/mnt/disks/yanyu-ldsc/miniconda3/envs/ldsc/bin/python2.7 /mnt/disks/yanyu-ldsc/ldsc/ldsc.py'
munge_sumstats_cmd: '/mnt/disks/yanyu-ldsc/miniconda3/envs/ldsc/bin/python2.7 /mnt/disks/yanyu-ldsc/ldsc/munge_sumstats.py'" > qsub/regressionLD/configs/config.$TRAIT\_finemap_maxpipCS_0.05_0.95.yaml

snakemake -s est_heritability.snmk --configfile qsub/regressionLD/configs/config.$TRAIT\_finemap_maxpipCS_0.05_0.95.yaml --unlock
snakemake -s est_heritability.snmk --configfile qsub/regressionLD/configs/config.$TRAIT\_finemap_maxpipCS_0.05_0.95.yaml -p --rerun-incomplete > qsub/regressionLD/logs/$TRAIT\_finemap_maxpipCS_0.05_0.95.log 2>&1
