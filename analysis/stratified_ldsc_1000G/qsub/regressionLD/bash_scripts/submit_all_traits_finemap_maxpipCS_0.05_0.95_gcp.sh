mkdir -p configs/
mkdir -p logs/

trait=($(ls /mnt/disks/yanyu-ldsc/processed_summary_imputation/imputed_* | sed 's#/mnt/disks/yanyu-ldsc/processed_summary_imputation/imputed_##g'|sed 's#.txt.gz##g'))
for t in "${trait[@]}"
  do
    # echo $t
    bash qtl_generic_finemap_maxpipCS_0.05_0.95_gcp.sh $t
  done
