cd /mnt/disks/yanyu-ldsc/rotation-at-imlab/analysis/stratified_ldsc_1000G

snakemake -s est_heritability.snmk --configfile qsub/regressionLD/config.prep_annot_all_traits_finemap_maxpipCS_0.05_0.95_gcp.yaml all_annot -p > qsub/regressionLD/prep_annot_all_traits_finemap_maxpipCS_0.05_0.95.log 2>&1
