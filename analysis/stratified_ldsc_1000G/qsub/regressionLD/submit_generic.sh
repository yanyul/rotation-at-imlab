ID=$1
# mkdir -p configs/
# mkdir -p logs/
echo $ID
trait=($(ls /gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_* | sed 's#/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_##g'|sed 's#.txt.gz##g'))
for t in "${trait[@]}"
  do
    # echo $t
    qsub -v TRAIT=$t,INDEX=$ID -o logs/$t\_$ID.out -N $t_$ID -e logs/$t\_$ID.err qtl_generic_baselineLD.qsub
  done
