cd /mnt/disks/yanyu-ldsc/rotation-at-imlab/analysis/stratified_ldsc_1000G

# snakemake --configfile qsub/qtl_ldsc/config.config.qtl_finemap_maxpipCS_0.05_0.95_gcp.yaml -np > qsub/qtl_ldsc/qtl_finemap_maxpip_0.05.log 2>&1
snakemake --configfile qsub/qtl_ldsc/config.qtl_finemap_maxpipCS_0.05_0.95_gcp.yaml --cores 4 -p > qsub/qtl_ldsc/qtl_finemap_maxpip_0.05.log 2>&1
