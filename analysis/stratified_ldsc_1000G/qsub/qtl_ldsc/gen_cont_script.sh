ID=$1
QTLLISTS=$2
echo $ID $QTLLISTS
echo "QTL:
  name: '$ID'
  bed:
    path: 'data/qtl_list/{qtl}.txt'
    list: 'cis-eQTL,cis-sQTL'
  cont_anno:
    path: 'data/qtl_list/{cont_anno}.txt'  
    list: '$QTLLISTS'
liftover: '/gpfs/data/im-lab/nas40t2/yanyul/data/hg38ToHg19.over.chain'
genotype:
  path: '/gpfs/data/im-lab/nas40t2/yanyul/data/1000G_EUR_Phase3_plink/1000G.EUR.QC.{chr}'
  name: '1000G_EUR'
snplist:
  path: '/gpfs/data/im-lab/nas40t2/yanyul/data/w_hm3.snplist'
  name: 'HapMap3'
ldsc_cmd: '/home/t.cri.yliang/miniconda2/envs/ldsc/bin/python2.7 /home/t.cri.yliang/scratch/repo/ldsc/ldsc.py'
make_annot_cmd: '/home/t.cri.yliang/miniconda2/envs/ldsc/bin/python2.7 /home/t.cri.yliang/scratch/repo/ldsc/make_annot.py'
" > configs/config.qtl_$ID.yaml

echo "#PBS -S /bin/bash
#PBS -l walltime=24:00:00
#PBS -l nodes=1:ppn=16
#PBS -l mem=32gb
#PBS -e qsubs/qtl_$ID.err
#PBS -o qsubs/qtl_$ID.out
#PBS -N qtl_$ID

source ~/.bash_profile
source ~/.bashrc
cd /home/t.cri.yliang/scratch/repo_new/rotation-at-imlab/analysis/stratified_ldsc_1000G/
conda activate pleio

snakemake --configfile qsub/qtl_ldsc/configs/config.qtl_$ID.yaml --unlock
snakemake --configfile qsub/qtl_ldsc/configs/config.qtl_$ID.yaml --cores 8 -p > qsub/qtl_ldsc/logs/qtl_$ID.log 2>&1" > qsubs/qtl_$ID.qsub

qsub qsubs/qtl_$ID.qsub
