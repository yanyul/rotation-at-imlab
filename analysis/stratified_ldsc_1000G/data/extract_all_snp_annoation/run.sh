infolder=/gpfs/data/im-lab/nas40t2/yanyul/data/baselineLD_v1.1/
outfolder=/gpfs/data/im-lab/nas40t2/yanyul/data/allSNP_from_baselineLD_v1.1/

mkdir -p $outfolder

chr=`seq 1 22`

# do
# 1. extract first column for: baselineLD.*.l2.M and baselineLD.*.l2.M_5_50
# 2. extract first 4 columns for: baselineLD.*.l2.ldscore.gz
# 3. extract first 5 columns for: baselineLD.*.annot.gz


for i in $chr
do
  # step 1
  cat $infolder/baselineLD.$i.l2.M | cut -f 1 > $outfolder/allSNP.$i.l2.M
  cat $infolder/baselineLD.$i.l2.M_5_50 | cut -f 1 > $outfolder/allSNP.$i.l2.M_5_50
  # step 2
  zcat $infolder/baselineLD.$i.l2.ldscore.gz | cut -f 1-4 | gzip > $outfolder/allSNP.$i.l2.ldscore.gz
  # step 3
  zcat $infolder/baselineLD.$i.annot.gz | cut -f 1-5 | gzip > $outfolder/allSNP.$i.annot.gz
done
