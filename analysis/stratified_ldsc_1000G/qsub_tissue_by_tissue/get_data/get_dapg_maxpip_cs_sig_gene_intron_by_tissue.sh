tissue=$1
pipthres=$2
csthre=$3
out=$4


if [ $tissue = "Skin_Sun_Exposed_Lower_leg" ]; then
  tt="Skin_Sun_Exposed"
elif [ $tissue = "Skin_Not_Sun_Exposed_Suprapubic" ]; then
  tt="Skin_Not_Sun_Exposed"
else
  tt=$tissue
fi
csfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/eqtl/parsed_dapg/$tt.clusters.txt.gz
pipfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/eqtl/parsed_dapg/$tt.variants_pip.txt.gz
siggenefile=/gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL/$tissue.v8.signif_variant_gene_pairs.txt.gz

awk -F"\t" 'FNR==NR{a[$1]=1; next}{if($1 in a) print $0}' <(zcat $siggenefile | tail -n +2 | cut -f2 | sort | uniq ) <(zcat $csfile | tail -n +2) > $out-siggene-csfile1
awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=1 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $3"\t"$4}}' $out-siggene-csfile1 <(zcat $pipfile | tail -n +2) > $out-cis-eQTL.temp

Rscript qsub_tissue_by_tissue/get_data/take_max.R -i $out-cis-eQTL.temp -o $out-cis-eQTL.txt
rm $out-cis-eQTL.temp
rm $out-siggene-csfile1 

csfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/sqtl/parsed_dapg_maf0.01_w1000000/$tissue.clusters.txt.gz
pipfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/sqtl/parsed_dapg_maf0.01_w1000000/$tissue.variants_pip.txt.gz
siggenefile=/gpfs/data/gtex-group/v8/63881/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/sqtl/GTEx_Analysis_v8_sQTL/$tissue.v8.sqtl_signifpairs.txt.gz

awk -F"\t" 'FNR==NR{split($1,c,":");gsub("chr", "", c[1]); a["intron_"c[1]"_"c[2]"_"c[3]]=1; next}{if($1 in a) print $0}' <(zcat $siggenefile | tail -n +2 | cut -f2 | sort | uniq) <(zcat $csfile | tail -n +2) > $out-siggene-csfile2
awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=1 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $3"\t"$4}}' $out-siggene-csfile2 <(zcat $pipfile | tail -n +2) > $out-cis-sQTL.temp

Rscript qsub_tissue_by_tissue/get_data/take_max.R -i $out-cis-sQTL.temp -o $out-cis-sQTL.txt
rm $out-cis-sQTL.temp
rm $out-siggene-csfile2
