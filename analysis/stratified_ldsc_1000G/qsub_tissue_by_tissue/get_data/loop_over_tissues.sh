tissues=`ls /gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL|grep -v egenes|sed 's/.v8.signif_variant_gene_pairs.txt.gz//g'`

mkdir -p configs/
mkdir -p logs/

cd ../../

csthres=0.95
pipthres=0.05
smallerpipthres=0.01
zeropipthres=0
pvalthres=1e-5
for tissue in $tissues
  do

    echo "output: 'data/qtl_list/by_tissue/DAPG-maxpipCS-gt-0.05-0.95-$tissue'
script: 'qsub_tissue_by_tissue/get_data/get_dapg_maxpip_cs_by_tissue.sh'
cmd: '$tissue $pipthres $csthres'
run: 'bash'" > qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0.05-0.95-"$tissue".yaml

    echo "output: 'data/qtl_list/by_tissue/QTL-$tissue'
script: 'qsub_tissue_by_tissue/get_data/get_qtl_by_tissue.sh'
cmd: '$tissue'
run: 'bash'" > qsub_tissue_by_tissue/get_data/configs/config.QTL-"$tissue".yaml

    echo "output: 'data/qtl_list/by_tissue/DAPG-maxpipCS-gt-0.05-0.95-eGene-sIntron-$tissue'
script: 'qsub_tissue_by_tissue/get_data/get_dapg_maxpip_cs_sig_gene_intron_by_tissue.sh'
cmd: '$tissue $pipthres $csthres'
run: 'bash'" > qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0.05-0.95-eGene-sIntron-"$tissue".yaml

    echo "output: 'data/qtl_list/by_tissue/DAPG-maxpipCS-gt-0.01-0.95-eGene-sIntron-$tissue'
script: 'qsub_tissue_by_tissue/get_data/get_dapg_maxpip_cs_sig_gene_intron_by_tissue.sh'
cmd: '$tissue $smallerpipthres $csthres'
run: 'bash'" > qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0.01-0.95-eGene-sIntron-"$tissue".yaml

    echo "output: 'data/qtl_list/by_tissue/DAPG-maxpipCS-gt-0-0.95-eGene-sIntron-$tissue'
script: 'qsub_tissue_by_tissue/get_data/get_dapg_maxpip_cs_sig_gene_intron_by_tissue.sh'
cmd: '$tissue $zeropipthres $csthres'
run: 'bash'" > qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0-0.95-eGene-sIntron-"$tissue".yaml

    echo "output: 'data/qtl_list/by_tissue/TransEQTL-$tissue.txt'
script: 'qsub_tissue_by_tissue/get_data/get_transeqtl_pval.R'
cmd: '--tissue $tissue --pval_thres $pvalthres --output'
onefile: True
run: 'Rscript'" > qsub_tissue_by_tissue/get_data/configs/config.TransEQTL-"$tissue".yaml

    if [ ! -f data/qtl_list/by_tissue/DAPG-maxpipCS-gt-0.05-0.95-"$tissue"-cis-eQTL.txt ]; then
      echo snakemake -s get_data.snmk --configfile qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-$pipthres-$csthres-$tissue.yaml # 2> qsub_tissue_by_tissue/get_data/logs/DAPG-maxpipCS-gt-"$pipthres"-"$csthres"-"$tissue".log
    fi
    echo snakemake -s get_data.snmk --configfile qsub_tissue_by_tissue/get_data/configs/config.QTL-$tissue.yaml # > qsub_tissue_by_tissue/get_data/logs/QTL-"$tissue".log

    echo snakemake -s get_data.snmk --configfile qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0.05-0.95-eGene-sIntron-$tissue.yaml # 2> qsub_tissue_by_tissue/get_data/logs/DAPG-maxpipCS-gt-0.05-0.95-eGene-sIntron-"$tissue".log

    echo snakemake -s get_data.snmk --configfile qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0.01-0.95-eGene-sIntron-$tissue.yaml # 2> qsub_tissue_by_tissue/get_data/logs/DAPG-maxpipCS-gt-0.01-0.95-eGene-sIntron-"$tissue".log

    echo snakemake -s get_data.snmk --configfile qsub_tissue_by_tissue/get_data/configs/config.DAPG-maxpipCS-gt-0-0.95-eGene-sIntron-$tissue.yaml # 2> qsub_tissue_by_tissue/get_data/logs/DAPG-maxpipCS-gt-0-0.95-eGene-sIntron-"$tissue".log

    snakemake -s get_data.snmk --configfile qsub_tissue_by_tissue/get_data/configs/config.TransEQTL-$tissue.yaml # 2> qsub_tissue_by_tissue/get_data/logs/TransEQTL-"$tissue".log

  done
