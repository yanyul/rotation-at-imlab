library(optparse)

option_list = list(
  make_option(c("-t", "--tissue"), type="character", default=NULL,
              help="input", metavar="character"),
  make_option(c("-p", "--pval_thres"), type="numeric", default=1e-5,
              help="output", metavar="character"),
  make_option(c("-o", "--output"), type="character", default=NULL,
              help="output", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

library(data.table)
options(datatable.fread.datatable = F)
library(dplyr)
df1 = fread(paste0('/gpfs/data/gtex-group/v8/trans-eQTL/ftp.cs.princeton.edu/pub/people/bj5/gtex_v8/consortium/trans_associations_1e-5/', opt$tissue, '_trans_associations_protein_coding_1e-5.tsv'), header = T)
df2 = fread(paste0('/gpfs/data/gtex-group/v8/trans-eQTL/ftp.cs.princeton.edu/pub/people/bj5/gtex_v8/consortium/trans_associations_1e-5/', opt$tissue, '_trans_associations_lincRNA_1e-5.tsv'), header = T)
df1 = df1 %>% filter(linreg_struct.pval <= opt$pval_thres)
df2 = df2 %>% filter(linreg_struct.pval <= opt$pval_thres)
varlist = unique(c(df1$rsid, df2$rsid))
varlist = varlist[order(varlist)]
write.table(varlist, opt$output, col = F, row = F, quo = F)
