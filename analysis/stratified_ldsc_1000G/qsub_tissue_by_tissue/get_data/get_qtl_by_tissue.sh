tissue=$1
out=$2

signiffile=/gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL/$tissue.v8.signif_variant_gene_pairs.txt.gz

zcat $signiffile | tail -n +2 | cut -f 1 | sort | uniq > $out-cis-eQTL.txt

signiffile=/gpfs/data/gtex-group/v8/63881/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/sqtl/GTEx_Analysis_v8_sQTL/$tissue.v8.sqtl_signifpairs.txt.gz

zcat $signiffile | tail -n +2 | cut -f 1 | sort | uniq > $out-cis-sQTL.txt
