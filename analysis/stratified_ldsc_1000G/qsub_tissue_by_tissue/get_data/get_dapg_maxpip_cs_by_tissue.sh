tissue=$1
pipthres=$2
csthre=$3
out=$4


if [ $tissue = "Skin_Sun_Exposed_Lower_leg" ]; then
  tt="Skin_Sun_Exposed"
elif [ $tissue = "Skin_Not_Sun_Exposed_Suprapubic" ]; then
  tt="Skin_Not_Sun_Exposed"
else
  tt=$tissue
fi
csfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/eqtl/parsed_dapg/$tt.clusters.txt.gz
pipfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/eqtl/parsed_dapg/$tt.variants_pip.txt.gz

awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=1 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $3"\t"$4}}' <(zcat $csfile | tail -n +2) <(zcat $pipfile | tail -n +2) > $out-cis-eQTL.temp

Rscript qsub_tissue_by_tissue/get_data/take_max.R -i $out-cis-eQTL.temp -o $out-cis-eQTL.txt
rm $out-cis-eQTL.temp

csfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/sqtl/parsed_dapg_maf0.01_w1000000/$tissue.clusters.txt.gz
pipfile=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/dapg/sqtl/parsed_dapg_maf0.01_w1000000/$tissue.variants_pip.txt.gz

awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=1 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $3"\t"$4}}' <(zcat $csfile | tail -n +2) <(zcat $pipfile | tail -n +2) > $out-cis-sQTL.temp

Rscript qsub_tissue_by_tissue/get_data/take_max.R -i $out-cis-sQTL.temp -o $out-cis-sQTL.txt
rm $out-cis-sQTL.temp
