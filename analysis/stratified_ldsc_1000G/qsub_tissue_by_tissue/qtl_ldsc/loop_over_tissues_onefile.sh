tissues=`ls /gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL|grep -v egene|sed 's/.v8.signif_variant_gene_pairs.txt.gz//g'`

mkdir -p configs/
mkdir -p qsubs/
mkdir -p logs/

MYTASK=$1

for tissue in $tissues
  do
    echo ---------------- "$tissue" ---------------------
    echo "QTL:
  name: '$MYTASK-$tissue'
  bed:
    path: 'data/qtl_list/by_tissue/{qtl}.txt'
    list: '$MYTASK-$tissue'
liftover: '/gpfs/data/im-lab/nas40t2/yanyul/data/hg38ToHg19.over.chain'
genotype:
  path: '/gpfs/data/im-lab/nas40t2/yanyul/data/1000G_EUR_Phase3_plink/1000G.EUR.QC.{chr}'
  name: '1000G_EUR'
snplist:
  path: '/gpfs/data/im-lab/nas40t2/yanyul/data/w_hm3.snplist'
  name: 'HapMap3'
ldsc_cmd: '/home/t.cri.yliang/miniconda2/envs/ldsc/bin/python2.7 /home/t.cri.yliang/scratch/repo/ldsc/ldsc.py'
make_annot_cmd: '/home/t.cri.yliang/miniconda2/envs/ldsc/bin/python2.7 /home/t.cri.yliang/scratch/repo/ldsc/make_annot.py'
" > configs/config."$MYTASK"-"$tissue".yaml

    echo "#PBS -S /bin/bash
#PBS -l walltime=24:00:00
#PBS -l nodes=1:ppn=8
#PBS -l mem=16gb
#PBS -e qsubs/$MYTASK-$tissue.err
#PBS -o qsubs/$MYTASK-$tissue.out
#PBS -N $MYTASK-$tissue

source ~/.bash_profile
source ~/.bashrc
cd /home/t.cri.yliang/labshare/mv_from_scratch/repo_new/rotation-at-imlab/analysis/stratified_ldsc_1000G/
conda activate pleio

snakemake --configfile qsub_tissue_by_tissue/qtl_ldsc/configs/config.$MYTASK-$tissue.yaml --unlock
snakemake --configfile qsub_tissue_by_tissue/qtl_ldsc/configs/config.$MYTASK-$tissue.yaml --cores 8 -p --nolock > qsub_tissue_by_tissue/qtl_ldsc/logs/$MYTASK-$tissue.log 2>&1" > qsubs/"$MYTASK"-"$tissue".qsub
    if test -f logs/$MYTASK-$tissue.log; then
      e=`cat logs/$MYTASK-$tissue.log|tail -n 2|head -n 1|grep done`
      if [ -z "$e" ];then
        qsub qsubs/$MYTASK-$tissue.qsub
      fi
    else
      qsub qsubs/$MYTASK-$tissue.qsub
    fi
  done
