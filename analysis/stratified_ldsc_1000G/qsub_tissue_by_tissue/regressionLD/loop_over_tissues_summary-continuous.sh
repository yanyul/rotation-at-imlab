tissues=`ls /gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL|grep -v egene|sed 's/.v8.signif_variant_gene_pairs.txt.gz//g'`

MYTASK=$1

for tissue in $tissues
  do
    THISTASK=$MYTASK-$tissue-continuous
    bash summary_generic.sh $THISTASK
  done
