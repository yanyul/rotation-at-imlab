MYTASK=$1
bash loop_over_tissues_summary-continuous.sh $MYTASK 2> unfinished-$MYTASK-continuous.log
traits=`cat unfinished-$MYTASK-continuous.log|grep output | sed 's#output\/regression\/##g'|awk -F"__" '{print $1}' | sort | uniq`
for trait in $traits
  do
    qsub -v MYTASK=$MYTASK,TRAIT=$trait -e logs/$MYTASK-$trait.err -o logs/$MYTASK-$trait.out -N $MYTASK-$trait run_all_tissues_generic-continuous.qsub 
  done
