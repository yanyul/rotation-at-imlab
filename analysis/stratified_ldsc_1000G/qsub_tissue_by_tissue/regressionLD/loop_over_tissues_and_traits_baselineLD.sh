tissues=`ls /gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL|grep -v egene|sed 's/.v8.signif_variant_gene_pairs.txt.gz//g'`

mkdir -p configs/
mkdir -p configs_manual/
mkdir -p qsubs/
mkdir -p logs/
mkdir -p logs_manual/

MYTASK=$1

## first: loop over tissues to prep annot and generate summary configfile
for tissue in $tissues
  do
    THISTASK=$MYTASK-$tissue
    echo ---------------- "$tissue" ---------------------
    echo "other_inputs:
  name: '1000G-HapMap3-BaselineLDv1.1'
  w_ld: '/gpfs/data/im-lab/nas40t2/yanyul/data/1000G_Phase3_weights_hm3_no_MHC/weights.hm3_noMHC.'
  frq: '/gpfs/data/im-lab/nas40t2/yanyul/data/1000G_Phase3_frq/1000G.EUR.QC.'
  baseline: '/gpfs/data/im-lab/nas40t2/yanyul/data/baselineLD_v1.1/baselineLD.'
  snplist: '/gpfs/data/im-lab/nas40t2/yanyul/data/w_hm3.snplist'
QTL:
  name: 'GTExV8$THISTASK'
  prefix: 'output/QTL/MERGED.$THISTASK""__1000G_EUR__HapMap3.'
  annot_prefix: 'output/QTL/MERGED.$THISTASK""__1000G_EUR.'
GWAS:
  name: 'GIANT_HEIGHT'
  path: '/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_GIANT_HEIGHT.txt.gz'
  SNPID_name: 'variant_id'
  clean: False
ldsc_cmd: '/home/t.cri.yliang/miniconda2/envs/ldsc/bin/python2.7 /home/t.cri.yliang/scratch/repo/ldsc/ldsc.py'
munge_sumstats_cmd: '/home/t.cri.yliang/miniconda2/envs/ldsc/bin/python2.7 /home/t.cri.yliang/scratch/repo/ldsc/munge_sumstats.py'
" > configs_manual/config.prep_annot_all_traits_$THISTASK.yaml

    cd ../../

    echo snakemake -s est_heritability.snmk --configfile qsub_tissue_by_tissue/regressionLD/configs_manual/config.prep_annot_all_traits_$THISTASK.yaml all_annot -p # > qsub_tissue_by_tissue/regressionLD/logs_manual/prep_annot_all_traits_$THISTASK.log 2>&1

    cd qsub_tissue_by_tissue/regressionLD/
    echo 'prep_annot DONE'

    echo "path:
  prefix: 'output/regression/'
  name: 'GTExV8$THISTASK""__1000G-HapMap3-BaselineLDv1.1'  # it will be used as the part of suffix to prevent overwritting
traits:
  list: 'ADIPOGen_Adiponectin,Astle_et_al_2016_Eosinophil_counts,Astle_et_al_2016_Granulocyte_count,Astle_et_al_2016_High_light_scatter_reticulocyte_count,Astle_et_al_2016_Lymphocyte_counts,Astle_et_al_2016_Monocyte_count,Astle_et_al_2016_Myeloid_white_cell_count,Astle_et_al_2016_Neutrophil_count,Astle_et_al_2016_Platelet_count,Astle_et_al_2016_Red_blood_cell_count,Astle_et_al_2016_Reticulocyte_count,Astle_et_al_2016_Sum_basophil_neutrophil_counts,Astle_et_al_2016_Sum_eosinophil_basophil_counts,Astle_et_al_2016_Sum_neutrophil_eosinophil_counts,Astle_et_al_2016_White_blood_cell_count,BCAC_ER_negative_BreastCancer_EUR,BCAC_ER_positive_BreastCancer_EUR,BCAC_Overall_BreastCancer_EUR,CARDIoGRAM_C4D_CAD_ADDITIVE,CKDGen_Chronic_Kidney_Disease,CNCR_Insomnia_all,DIAGRAM_T2D_TRANS_ETHNIC,EAGLE_Eczema,EGG_BL,EGG_BMI_HapMapImputed,EGG_BW3_EUR,EGG_Pubertal_growth_10F,EGG_Pubertal_growth_12M,ENIGMA_Intracraneal_Volume,GABRIEL_Asthma,GEFOS_Forearm,GIANT_2015_BMI_EUR,GIANT_2017_BMI_Active_EUR,GIANT_HEIGHT,GIANT_HIP_Combined_EUR,GIANT_WC_Combined_EUR,GIANT_WHR_Combined_EUR,GLGC_Mc_HDL,GLGC_Mc_LDL,GLGC_Mc_TG,GPC-NEO-NEUROTICISM,HRGene_HeartRate,IBD.EUR.Crohns_Disease,IBD.EUR.Inflammatory_Bowel_Disease,IBD.EUR.Ulcerative_Colitis,ICBP_DiastolicPressure,ICBP_SystolicPressure,IGAP_Alzheimer,ILAE_Genetic_generalised_epilepsy,IMMUNOBASE_Celiac_disease_hg19,IMMUNOBASE_Multiple_sclerosis_hg19,IMMUNOBASE_Systemic_lupus_erythematosus_hg19,ISGC_Malik_2016_METASTROKE_all_strokes,Jones_et_al_2016_Chronotype,Jones_et_al_2016_SleepDuration,MAGIC_FastingGlucose,MAGIC_ln_FastingInsulin,MAGNETIC_CH2.DB.ratio,MAGNETIC_HDL.C,MAGNETIC_IDL.TG,MAGNETIC_LDL.C,PGC_ADHD_EUR_2017,PGC_ASD_2017_CEU,RA_OKADA_TRANS_ETHNIC,SSGAC_Depressive_Symptoms,SSGAC_Education_Years_Pooled,TAGC_Asthma_EUR,UKB_1160_Sleep_duration,UKB_1180_Morning_or_evening_person_chronotype,UKB_1200_Sleeplessness_or_insomnia,UKB_1807_Fathers_age_at_death,UKB_20002_1065_self_reported_hypertension,UKB_20002_1094_self_reported_deep_venous_thrombosis_dvt,UKB_20002_1111_self_reported_asthma,UKB_20002_1154_self_reported_irritable_bowel_syndrome,UKB_20002_1222_self_reported_type_1_diabetes,UKB_20002_1223_self_reported_type_2_diabetes,UKB_20002_1225_self_reported_hyperthyroidism_or_thyrotoxicosis,UKB_20002_1226_self_reported_hypothyroidism_or_myxoedema,UKB_20002_1243_self_reported_psychological_or_psychiatric_problem,UKB_20002_1261_self_reported_multiple_sclerosis,UKB_20002_1262_self_reported_parkinsons_disease,UKB_20002_1265_self_reported_migraine,UKB_20002_1289_self_reported_schizophrenia,UKB_20002_1309_self_reported_osteoporosis,UKB_20002_1313_self_reported_ankylosing_spondylitis,UKB_20002_1452_self_reported_eczema_or_dermatitis,UKB_20002_1453_self_reported_psoriasis,UKB_20002_1461_self_reported_inflammatory_bowel_disease,UKB_20002_1462_self_reported_crohns_disease,UKB_20002_1463_self_reported_ulcerative_colitis,UKB_20002_1464_self_reported_rheumatoid_arthritis,UKB_20002_1466_self_reported_gout,UKB_20002_1473_self_reported_high_cholesterol,UKB_20002_1616_self_reported_insomnia,UKB_20016_Fluid_intelligence_score,UKB_20022_Birth_weight,UKB_20127_Neuroticism_score,UKB_21001_Body_mass_index_BMI,UKB_23099_Body_fat_percentage,UKB_2395_2_Hair_or_balding_pattern_Pattern_2,UKB_2395_3_Hair_or_balding_pattern_Pattern_3,UKB_2395_4_Hair_or_balding_pattern_Pattern_4,UKB_3526_Mothers_age_at_death,UKB_50_Standing_height,UKB_6150_1_Vascular_or_heart_problems_diagnosed_by_doctor_Heart_attack,UKB_6152_5_diagnosed_by_doctor_Blood_clot_in_the_leg_DVT,UKB_6152_7_diagnosed_by_doctor_Blood_clot_in_the_lung,UKB_6152_8_diagnosed_by_doctor_Asthma,UKB_6152_9_diagnosed_by_doctor_Hayfever_allergic_rhinitis_or_eczema,UKB_G40_Diagnoses_main_ICD10_G40_Epilepsy,UKB_G43_Diagnoses_main_ICD10_G43_Migraine,pgc.scz2,tag.evrsmk.tbl'
  name: 'all_traits'
# get_M_5_50: '/gpfs/data/im-lab/nas40t2/yanyul/data/baselineLD_v1.1/baselineLD.'  # prefix !
" > configs_manual/config.summary_all_traits_$THISTASK.yaml
    echo run: bash summary_generic.sh "$THISTASK"
    echo "prepare summary config DONE"
  done
## first step DONE

## second: loop over traits to better parallelize jobs
traits=($(ls /gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_* | sed 's#/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_##g'|sed 's#.txt.gz##g'))
for trait in "${traits[@]}"
  do
    echo ---------------- "$trait" ---------------------
    qsub -v MYTASK=$MYTASK,TRAIT=$trait -e logs/$MYTASK-$trait.err -o logs/$MYTASK-$trait.out -N $MYTASK-$trait run_all_tissues_generic.qsub
  done
## second step DONE
