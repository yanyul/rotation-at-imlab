MYTASK=$1
regulation=$2
bash loop_over_tissues_summary_allSNP.sh $MYTASK $regulation 2> unfinished-allSNP-$MYTASK$regulation.log
traits=`cat unfinished-allSNP-$MYTASK$regulation.log|grep output | sed 's#output\/regression\/##g'|awk -F"__" '{print $1}' | sort | uniq`
for trait in $traits
  do
    qsub -v MYTASK=$MYTASK,TRAIT=$trait,SUFFIX=$regulation -e logs/allSNP-$MYTASK-$trait.err -o logs/allSNP-$MYTASK-$trait.out -N $MYTASK-$trait run_all_tissues_generic_allSNP.qsub 
  done
