Here is some set up scripts needed for running GCP

## Dependent files

* liftover chain
* 1000G genotypes
* `w_hm3.snplist`
* `ldsc` repo
* `w_ld` files: `1000G_Phase3_weights_hm3_no_MHC/weights.hm3_noMHC.*`
* `frq` files: `1000G_Phase3_frq/1000G.EUR.QC.*`
* baselineLD LDSC: `baselineLD_v1.1/baselineLD.*`
* GWAS summary statistics: `/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_*.txt.gz`

## Environment set up

* `python3` overall for `snakemake`
* `python2` for `ldsc`
* `liftover`
* `bedtools`
* `R 3.3.2`
  * `data.table`
  * `optparse`
  * `stringr`
  * `bigrquery`
* `gcloud`

Build two conda environments, so that install conda first.

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

### Create conda environment `snmk`

This will be the main environment for the pipeline

```
conda create -n snmk python=3
conda activate snmk
conda install -y -c bioconda snakemake
conda install -y -c bioconda ucsc-liftover
conda install -y -c bioconda bedtools
conda install -y -c r r=3.3.2
conda install -y -c conda-forge r-bigrquery
conda install -y -c r r-data.table
conda install -y -c conda-forge r-optparse
conda install -y -c r r-stringr
conda install -y r-dplyr
conda env export | grep -v "^prefix: " > env_snmk.yml
```

Once generate enviroment YAML, next time we only need to do

```
conda env create --file env_snmk.yml
```

### Create conda environment `ldsc`

This will be used only for `ldsc/` tools

```
# ldsc repo
git clone https://github.com/bulik/ldsc.git
conda env create --file ldsc/environment.yml
```

**Remember** to export environment YAML

## SSH key

1. Remember to generate ssh key for the local machine and put public key onto GCP machine named as `~/.ssh/cri-ssh-key`
2. Remember to generate ssh key for GCP machine and put it onto bitbucket so that the source code of the pipeline can be cloned `rotation-at-imlab` (Anyway, without doing it, I won't see this note ...)
