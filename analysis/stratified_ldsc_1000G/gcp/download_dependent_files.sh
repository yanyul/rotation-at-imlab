TARGETDIR=$1
mkdir -p $TARGETDIR/
cd $TARGETDIR/

# 1000G genotypes
wget https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_plinkfiles.tgz
tar -xvzf 1000G_Phase3_plinkfiles.tgz
rm 1000G_Phase3_plinkfiles.tgz

# w_hm3
wget https://data.broadinstitute.org/alkesgroup/LDSCORE/w_hm3.snplist.bz2
bzip2 -d w_hm3.snplist.bz2

# w_ld
wget https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_weights_hm3_no_MHC.tgz
tar -xvzf 1000G_Phase3_weights_hm3_no_MHC.tgz
rm 1000G_Phase3_weights_hm3_no_MHC.tgz

# frq
wget https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_frq.tgz
tar -xvzf 1000G_Phase3_frq.tgz
rm 1000G_Phase3_frq.tgz

# baselineLDv1.1
wget https://data.broadinstitute.org/alkesgroup/LDSCORE/1000G_Phase3_baselineLD_v1.1_ldscores.tgz
tar -xvzf 1000G_Phase3_baselineLD_v1.1_ldscores.tgz
rm 1000G_Phase3_frq.tgz

# liftover chain
wget http://hgdownload.cse.ucsc.edu/goldenpath/hg38/liftOver/hg38ToHg19.over.chain.gz
gunzip hg38ToHg19.over.chain.gz
