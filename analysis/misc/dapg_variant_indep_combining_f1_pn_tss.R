source('../../scripts/mylib.R')
library(data.table)
library(dplyr)
library(stringr)

dapg_var_list <- read.table('../ld_pruning/output/DAPGvariant-active_in_GTEx_v8_PCA_EUR.prune_varlist.txt')
tss <- fread(cmd = 'zcat ../prepare_eqtl_matrix/output/tss_DAPGeQTL.GTExV8__DAPG_variant.gz')
projection <- fread(cmd = 'zcat ../cross_tissue_pattern_of_eqtl/output/gtex-v8-dapg-variant.formatted_variance_explained_DAPGeQTL-active.txt.gz')
pn <- fread('zcat ../pleiotropy_effect_size/GTEx_EQTL/Aggregated-MASHR-DAPGvariant__DAPG_variant.join.gz')
gene_map <- read.table('../../shared_data/gene_annotation/genarch_GTEx_gencode_v18.tsv', header = T)
gene_map$gene <- str_replace(gene_map$ensembl_id, '.[0-9]+$', '')


tss_indep <- tss[ tss$V1 %in% dapg_var_list$V1, ]
eqtl_indep <- paste(tss_indep$V1, tss_indep$V2)

projection$var <- paste0('chr', projection$variantID, '_b38')
projection_indep <- projection[ projection$var %in% dapg_var_list$V1, ]
mem <- get_membership_from_pve_mat(projection_indep[ , 3:33])
mem_cat <- merge_cat(mem)

pn_indep <- pn[ pn$V4 %in% dapg_var_list$V1, ]

df <- data.frame(var = tss_indep$V1, gene = tss_indep$V2, tss = tss_indep$V3)
df$mem <- mem_cat[ match(eqtl_indep, paste(projection_indep$var, projection_indep$geneID)) ]
df$Pn <- pn_indep$V5[ match(df$var, pn_indep$V4) ]

df <- df %>% mutate(f1 = !is.na(mem) & mem == 'Shared factor')

# only looking at protein coding gene ?
df <- df %>% mutate(is_coding_gene = df$gene %in% gene_map$gene)
df$strand <- NA
df$strand[df$is_coding_gene] <- as.character(gene_map$strand[ match(df$gene[df$is_coding_gene], gene_map$gene) ])
df <- df %>% mutate(dist_relative_to_tss = ((strand == '+') * 2 - 1) * tss)

dapg_eqtl <- fread('../../shared_data/DAPG_pip_gt_0.01-AllTissues.txt', header = T)
dapg_eqtl <- unique(paste(dapg_eqtl$variant_id, str_replace(dapg_eqtl$gene_id, '.[0-9]+$', '')))
df <- df %>% mutate(is_dapg_eqtl = paste(var, gene) %in% dapg_eqtl)
saveRDS(df, 'dapg_variant_indep_combining_f1_pn_tss.rds')
