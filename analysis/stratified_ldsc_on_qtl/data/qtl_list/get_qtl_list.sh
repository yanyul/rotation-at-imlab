cp ../../../annotate_gwas_catalog/output/ALL.signif_eqtl.GTExFDRlt0.05.txt cis-eQTL.txt
cp ../../../annotate_gwas_catalog/output/ALL.signif_eqtl.GTExSQTLFDRlt0.05.txt cis-sQTL.txt 
cat /gpfs/data/im-lab/nas40t2/rbonazzola/GTEx/v8/one_snp_models/output/filtered_eqtl/DAPG_pip_gt_0.01-AllTissues.txt | cut -f3 | tail -n +2 | sort | uniq > DAPG-eQTL.txt
