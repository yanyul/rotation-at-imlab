files=`ls ../../../../data/baseline_v1.1/*`
for i in $files
do
  filename=$(basename -- "$i")
  filename="${filename%.*}"
  liftOver $i ../../../../data/hg19ToHg38.over.chain $filename.hg38.bed $filename.hg38.unmap
  rm $filename.hg38.unmap
done
