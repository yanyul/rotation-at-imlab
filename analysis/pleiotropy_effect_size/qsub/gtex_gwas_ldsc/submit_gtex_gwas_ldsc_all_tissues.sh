e=$( ls /group/im-lab/nas40t2/rbonazzola/GTEx/v8/GTEx_Analysis_v8_eQTL_all_associations | sed 's/.allpairs.txt.gz//')

for i in $e
  do
    qsub -v TISSUE=$i -o $i.out -N gtex_gwas_$i -e $i.err $1
  done
