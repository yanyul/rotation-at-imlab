name=( $( cat ../../../../data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 | sed 's/imputed_//' | sed 's/.txt.gz//' ) )
file=( $( cat ../../../../data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 ) )
for (( i=0; i<${#name[@]}; i++)) 
  do
    # echo $i
    qsub -v NAME=${name[$i]},FILE=/group/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38/${file[$i]} -o ${name[$i]}.out -N gwas_${name[$i]} -e ${name[$i]}.err gwas.qsub
  done
