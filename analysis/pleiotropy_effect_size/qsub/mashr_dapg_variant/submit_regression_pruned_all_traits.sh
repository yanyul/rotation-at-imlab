name=( $( cat ../../../../shared_data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 | sed 's/imputed_//' | sed 's/.txt.gz//' ) )

for (( j=0; j<${#name[@]}; j++))
  do
    qsub -v GWAS=${name[$j]} -o logs/regression_${name[$j]}.out -N ${name[$j]} -e logs/regression_${name[$j]}.err regression_pruned.qsub
  done
