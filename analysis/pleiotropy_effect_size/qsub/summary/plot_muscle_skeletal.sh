name=( $( cat ../../data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 | sed 's/imputed_//' | sed 's/.txt.gz//' | grep 'GIANT_HEIGHT\|UKB_50_Standing_height\|EGG_Pubertal_growth_10F\|EGG_Pubertal_growth_12M\|UKB_20002_1289_self_reported_schizophrenia\|pgc.scz2\|UKB_20016_Fluid_intelligence_score\|ISGC_Malik_2016_METASTROKE_all_strokes') )
tissue=Muscle_Skeletal
declare -a eqtl=('ConditionalAnalysis' 'DAPG_pip_gt_0.01')
for t in "${eqtl[@]}"
  do
  for (( j=0; j<${#name[@]}; j++))
    do
      namei=${name[$j]}
      snakemake --configfile qsub/ls2/config.$tissue\__$namei\__$t.yaml all_ls2_plot
    done
  done
