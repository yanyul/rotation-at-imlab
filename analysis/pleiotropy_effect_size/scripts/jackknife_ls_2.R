library(optparse)

option_list = list(
  make_option(c("-i", "--input"), type="character", default=NULL,
              help="name of input file (joined eqtl and gwas)", metavar="character"),
  make_option(c("-a", "--af"), type="numeric", default=NA,
              help="column number of allele frequency", metavar="character"),
  make_option(c("-z", "--zscore"), type="numeric", default=NA,
              help="column number of z-score", metavar="numeric"),
  make_option(c("-n", "--num_egene"), type="numeric", default=NA,
              help="column number of the number of significant genes", metavar="character"),
  make_option(c("-s", "--sample_size"), type="numeric", default=NA,
              help="column number of sample size", metavar="character"),
  make_option(c("-b", "--beta"), type="numeric", default=NA,
              help="column number of effect size", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="name of output file", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);
source('../../scripts/mylib.R')
library(data.table)

options(datatable.fread.datatable = FALSE)
set.seed(2018)

message('--------------------------')
message(opt$input)
message('--------------------------')

# read data
message('Fread')
df <- fread(paste0('gunzip -c ', opt$input), showProgress = F, sep = '\t')


# obtain MAF
message('MAF')
maf.pmin <- do.call(pmin, data.frame(df[, opt$af], 1 - df[, opt$af]))
df <- df[!is.na(maf.pmin), ]
df$maf <- maf.pmin[!is.na(maf.pmin)]
df <- df[df$maf != 0 & df$maf * df[, opt$sample_size] >= 50, ]  # remove rare variants (low power variants)


# Remove NA zscore
df <- df[!is.na(df[, opt$zscore]), ]


# impute beta_tilde
message('Beta')
df$beta_tilde <- df[, opt$zscore] / sqrt(2 * df[, opt$sample_size] * df$maf * (1 - df$maf))
df$num_sig_gene <- df[, opt$num_egene]


# obtain estimate
theta <- solveLS(df$num_sig_gene, df$beta_tilde^2, intercept = T)
theta <- c(theta$beta.hat, theta$intercept)

# obtain deleted values
message('Deleted values')
deleted.values <- data.frame(t(vapply(1 : nrow(df), function(x) {
  df.sub <- df[-x, ]
  out <- solveLS(df.sub$num_sig_gene, df.sub$beta_tilde^2, intercept = T)
  return(c(out$beta.hat, out$intercept))
}, c(-Inf, -Inf))))
colnames(deleted.values) <- c('beta', 'intercept')
# head(deleted.values)
# dim(deleted.values) 


# obtain pseudovalues
message('Pseudo values')
n <- nrow(df)
# message(dim(deleted.values))
# message(length(theta))
pseudo.values <- data.frame(t(apply(deleted.values, 1, function(x) { if(length(x) != 2) {print(x)}; n * theta - (n - 1) * x })))
colnames(pseudo.values) <- c('beta', 'intercept')
# dim(pseudo.values)
# print(theta)

# obtain jackknife estimate
message('Jackknife estimates')
jknife_cov <- cov(pseudo.values) / n
jknife_var <- diag(jknife_cov)
jknife_se <- sqrt(jknife_var)
jknife_est <- colMeans(pseudo.values)
# dim(jknife_cov)
# length(jknife_est)

# generate output
message('Output')
stat <- data.frame(slope.jk = jknife_est[1], intercept.jk = jknife_est[2], slope.jk.se = jknife_se[1], intercept.jk.se = jknife_se[2])
write.table(stat, opt$output, quote = F, row.names = F, col.names = T, sep = '\t')
message('----------END----------')
