zcat < $1 | tail -n +2 | sort --parallel=$3 -k3,3 -k4,4n |awk -F"\t" -v OFS="\t" '{print $3,$4-1,$4,$2,".",".",$1,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15}'> $2.bed
bgzip -f $2.bed
