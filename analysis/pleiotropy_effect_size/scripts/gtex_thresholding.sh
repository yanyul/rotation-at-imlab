# zcat < $1 | head -n 1 > $2
zcat < $1 | tail -n +2 | grep -v nan | grep -v NA | awk -v x=$2 -F"\t" 'function abs(v) {return v < 0 ? -v : v};{if($9!=0){if(abs($8/$9) > x) print $0}}' 
