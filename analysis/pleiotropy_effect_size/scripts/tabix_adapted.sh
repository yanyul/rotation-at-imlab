zcat < $1.txt.gz | head -n 1 > $1.sorted.gz
zcat < $1.txt.gz | tail -n +2 | sort -k3,3 -k4,4n >> $1.sorted.gz
bgzip -f $1.sorted.gz
tabix -f -S 1 -s 3 -b 4 -e 4 $1.sorted.gz
