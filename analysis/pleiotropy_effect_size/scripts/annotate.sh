i=1
bedtools intersect -a $1 -b <(zcat $2$i.l2.ldscore.gz | tail -n +2 | awk -F"\t" -v OFS="\t" '{print "chr"$1,$3-1,$3,$2,$4}') -wa -wb -sorted | awk -F"\t" -v OFS="\t" '{if($4==$(NF-1)) {a=$1; for(i = 2; i <= NF-5; i++) {a=a"\t"$i}; print a,$(NF)}}' > $3.txt
for i in `seq 2 22`;
    do
        bedtools intersect -a $1 -b <(zcat $2$i.l2.ldscore.gz | tail -n +2 | awk -F"\t" -v OFS="\t" '{print "chr"$1,$3-1,$3,$2,$4}') -wa -wb -sorted | awk -F"\t" -v OFS="\t" '{if($4==$(NF-1)) {a=$1; for(i = 2; i <= NF-5; i++) {a=a"\t"$i}; print a,$(NF)}}' >> $3.txt
    done

cat $3.txt | gzip > $3
rm $3.txt
