files=("${@: 2}")  # obtain all inputs
output=$1

cp ${files[0]} $output.tmp.0

for (( j=0; j<${#files[@]}-1; j++ ))
  do
    next=$((j+1))
    filei=${files[$next]}
    awk -v OFS="\t" -F"\t" 'FNR==NR{a[$2"-"$1]=$1"\t"$2;print $1"\t"$2;next} {if($2"-"$1 in a == 0){print $1"\t"$2}}' $output.tmp.$j $filei > $output.tmp.$next
    rm $output.tmp.$j
  done

cat $output.tmp.$next | awk -F"\t" -v OFS="\t" '{split($2, a, "_"); print a[1],a[2]-1,a[2],$2,$1}' > $1.with_pos
cat $1.with_pos | sort -k1,1 -k2,2n -k3,3 | awk -F"\t" -v OFS="\t" '{print $1,$2,$3,$4,$5}'> $1.bed
bgzip -f $1.bed
rm $1.with_pos
rm $output.tmp.$next
