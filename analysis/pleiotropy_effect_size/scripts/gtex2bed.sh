cat $1.sig | awk -F"\t" -v OFS="\t" '{split($2, a, "_"); print a[1],a[2]-1,a[2],$2,$1}' > $1.with_pos
cat $1.with_pos | sort --parallel=$2 -k1,1 -k2,2n -k3,3 | awk -F"\t" -v OFS="\t" '{print $1,$2,$3,$4,$5}'> $1.bed
bgzip -f $1.bed
rm $1.with_pos
