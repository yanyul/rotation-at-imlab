cat $1.sig | tail -n +2 | awk -F"," -v OFS="\t" '{split($2, a, "_"); print a[1],a[2],$0}' > $1.with_pos
cat $1.with_pos | head -n 1 > $1.sorted
cat $1.with_pos | tail -n +2 | sort -k1,1 -k2,2n >> $1.sorted
bgzip -f $1.sorted
tabix -f -S 1 -s 1 -b 2 -e 2 $1.sorted.gz
rm $1.with_pos
