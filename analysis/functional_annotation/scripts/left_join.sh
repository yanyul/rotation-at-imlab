output=$1
a=$2
b=$3
aid=$4
bid=$5
bval=$6
bname=$7

header=$(zcat $a | head -n 1)
header="$header $7"
echo $header | tr ' ' '\t' > $output.tmp
awk -v bid=$bid -v aid=$aid -v bval=$bval 'NR==FNR{a[$(bid)]=$(bval);next}; {if($(aid) in a){print $0"\t"a[$(aid)]}else{print $0"\tNA"}}' <(zcat $b) <(zcat $a |tail -n +2) >> $output.tmp
cat $output.tmp | gzip > $output
rm $output.tmp
