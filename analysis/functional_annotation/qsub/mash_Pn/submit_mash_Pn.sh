e=$( ls /gpfs/data/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL_all_associations/ | sed 's/.allpairs.txt.gz//')
for i in $e
  do
    qsub -v TISSUE=$i -o logs/$i.out -N mash_Pn_$i -e logs/$i.err mash_Pn.qsub
  done
