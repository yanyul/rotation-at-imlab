gwas=$1
varlist=$2

zcat $1 | head -n 1 
awk -v colnum=$3 'FNR==NR{a[$1]=1;next}{if($(colnum) in a){print $0}}' $varlist <(zcat < $1)
