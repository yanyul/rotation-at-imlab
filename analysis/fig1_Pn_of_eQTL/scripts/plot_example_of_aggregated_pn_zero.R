library(stringr)
library(gridExtra)
library(reshape2)
library(ggplot2)

str <- '1_3078885_G_A'

eqtl <- data.frame()
for(i in 1 : 8) {
  tmp1 <- readRDS(paste0('/gpfs/data/gtex-group/v8/independent_eQTL/MASH/DAPG_pip_gt_0.01-AllTissues/DAPG_pip_gt_0.01-AllTissues.batch_', i, '.rds'))$Shat

  tmp2 <- readRDS(paste0('/gpfs/data/gtex-group/v8/independent_eQTL/MASH/DAPG_pip_gt_0.01-AllTissues/DAPG_pip_gt_0.01-AllTissues.batch_', i, '.rds'))$Bhat
  tmp4 <- readRDS(paste0('/gpfs/data/gtex-group/v8/independent_eQTL/MASH/DAPG_pip_gt_0.01-AllTissues/DAPG_pip_gt_0.01-AllTissues.batch_', i, '.posterior.rds'))$PosteriorMean
  tmp5 <- readRDS(paste0('/gpfs/data/gtex-group/v8/independent_eQTL/MASH/DAPG_pip_gt_0.01-AllTissues/DAPG_pip_gt_0.01-AllTissues.batch_', i, '.posterior.rds'))$PosteriorSD
  ind <- !is.na(str_match(row.names(tmp5), str))
  if(sum(ind) > 0){
    tmp3 <- rbind(tmp2[ind, ], tmp1[ind, ], tmp4[ind, ], tmp5[ind, ])
    colnames(tmp3) <- colnames(tmp5)
    row.names(tmp3) <- c('bhat', 'shat', 'p.mean', 'p.sd')
    eqtl <- rbind(eqtl, tmp3)

  }
}

eqtl.b <- melt(eqtl[1, ])
eqtl.s <- melt(eqtl[2, ])
eqtl.b$s <- eqtl.s[, 2]
p1 <- ggplot(eqtl.b) + geom_bar(aes(x = variable, y = value), stat = 'identity') + geom_errorbar(aes(x = variable, ymin = value - 2 * s, ymax = value + 2 * s)) + ggtitle('effect size') + theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))

eqtl.b <- melt(eqtl[3, ])
eqtl.s <- melt(eqtl[4, ])
eqtl.b$s <- eqtl.s[, 2]
p2 <- ggplot(eqtl.b) + geom_bar(aes(x = variable, y = value), stat = 'identity') + geom_errorbar(aes(x = variable, ymin = value - 2 * s, ymax = value + 2 * s)) + ggtitle('posterior') + theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))

g <- arrangeGrob(p1, p2, ncol = 1, top=paste0('chr', str, '_b38'))
ggsave(file="Pn_zero_example.png", g) #saves g
