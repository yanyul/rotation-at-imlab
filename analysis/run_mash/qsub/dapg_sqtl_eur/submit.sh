END=20

mkdir -p logs

for i in $(seq 1 $END)
  do
    qsub -v IDX=$i -e logs/$i.err -o logs/$i.out -N mash_$i run.qsub
  done
