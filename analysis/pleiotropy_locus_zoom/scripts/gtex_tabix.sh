e1="chr,start,"
e2=`zcat $1 | head -n 1 | tr '\t' ','`
echo $e1$e2 | tr ',' '\t' > $2.with_pos
zcat $1 | tail -n +2 | awk -F"\t" -v OFS="\t" '{split($2, a, "_"); print a[1],a[2],$0}' >> $2.with_pos
cat $2.with_pos | head -n 1 > $2
cat $2.with_pos | tail -n +2 | sort -k1,1 -k2,2n >> $2
bgzip -f $2
tabix -f -S 1 -s 1 -b 2 -e 2 $2.gz
rm $2.with_pos
