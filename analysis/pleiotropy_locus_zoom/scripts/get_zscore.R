library(optparse)

option_list = list(
  make_option(c("-t", "--tss"), type="character", default=NULL,
              help="RDS containing one line data frame with TSS and meta data of the given gene", metavar="character"),
  make_option(c("-g", "--gene_set"), type="character", default=NULL,
              help="RDS containing multiple rows where each row corresponds to one gene (according to TSS) nearby TSS (may include itself)", metavar="character"),
  make_option(c("-z", "--zscore"), type="character", default=NULL,
              help="tabixed big file where GTEx sum_stat locates", metavar="character"),
  make_option(c("-o", "--output"), type="character", default=NULL,
              help="name of the output RDS (zscore)", metavar="character"),
  make_option(c("-b", "--output_beta"), type="character", default=NULL,
              help="name of the output RDS (beta)", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

source('../../scripts/mylib.R')
library(data.table)
options(datatable.fread.datatable = FALSE)


# load RDS files: focul gene and nearby genes
gene.set <- readRDS(opt$gene_set)
focus.gene <- readRDS(opt$tss)


# determine the region
lrange <- min(gene.set$tss)
rrange <- max(gene.set$tss)
bin.size <- 1e6
start <- lrange - bin.size - 1
end <- rrange + bin.size + 1
range <- paste0(focus.gene$chromosome, ':', start, '-', end)


# query by region and load as data.frame
cmd <- paste('tabix', opt$zscore, range, "| grep -v 'nan'")
message(paste('CMD =', cmd))
zscore.df <- fread(cmd, showProgress = F, sep = '\t')
header <- fread(paste('zcat', opt$zscore, '| head -n 2'), showProgress = F, sep = '\t')
header <- colnames(header)
colnames(zscore.df) <- header


# clean up df
zscore.df <- zscore.df[zscore.df$gene_id %in% gene.set$gene, ]


# obtain shared variants
shared.snp <- zscore.df %>% group_by(variant_id) %>%
  summarise(ngene = length(gene_id)) %>%
  filter(ngene == nrow(gene.set)) %>%
  select(variant_id)
zscore.df.shared <- zscore.df[zscore.df$variant_id %in% shared.snp$variant_id, ]
zscore.df.shared$zscore <- zscore.df.shared$slope / zscore.df.shared$slope_se
zscore_mat <- dcast(zscore.df.shared, variant_id ~ gene_id, value.var = 'zscore')
beta_mat <- dcast(zscore.df.shared, variant_id ~ gene_id, value.var = 'slope')
saveRDS(zscore_mat, opt$output)
saveRDS(beta_mat, opt$output_beta)
