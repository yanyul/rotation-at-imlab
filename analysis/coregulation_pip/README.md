here is a more stringent analysis on co-regulation
criteria for regulation: pip > 0.5

To obtain `cis-eqtl_dapg_pip_ge_0.5.gz` 

```
$ Rscript analysis_script.R -p 0.5 -o cis-eqtl_dapg_pip_ge_0.5.gz
```
