CONFIG=$1
TISSUE=$2

cd /home/t.cri.yliang/labshare/mv_from_scratch/repo_new/rotation-at-imlab/analysis/map_intron_to_gene_splicing/

for i in `cat qsub/get_gene_level_splicing/traits.txt`
do
  TRAIT=$i
  snakemake -s get_gene_level_score.snmk --configfile qsub/get_gene_level_splicing/config.$CONFIG.yaml -p --unlock --config trait=$TRAIT tissue=$TISSUE 
  snakemake -s get_gene_level_score.snmk --configfile qsub/get_gene_level_splicing/config.$CONFIG.yaml -p --config trait=$TRAIT tissue=$TISSUE > qsub/get_gene_level_splicing/logs/$CONFIG--$TISSUE--$TRAIT.log 2>&1
done
