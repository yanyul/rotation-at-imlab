cat ../../../data/annotations_gencode_v26.tsv | head -n 1 > annotations_gencode_v26.intron_gene_joint.tsv
python ../scripts/gene2joint.py --if_gz 0 --gene_input ../../../data/annotations_gencode_v26.tsv --gene_col 1 --map /gpfs/data/im-lab/nas40t2/yanyul/GTExV8/intron_to_gene/top-map-by-sample-size-gene2intron.signif-sgenes.txt.gz >> annotations_gencode_v26.intron_gene_joint.tsv
