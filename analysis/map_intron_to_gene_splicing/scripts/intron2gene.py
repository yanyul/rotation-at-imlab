import argparse
parser = argparse.ArgumentParser(prog='intron2gene.py', description='''
    map intron to intron+gene
    print output
''')
parser.add_argument('--score_if_gz', type=int, default=1, help='''
    if score file is GZ input (if not GZ use 0)
''')
parser.add_argument('--score_sep', type=str, default='\t', help='''
    separator/delimiter of score file
''')
parser.add_argument('--map_if_gz', type=int, default=1, help='''
    if map file is GZ input (if not GZ use 0)
''')
parser.add_argument('--score', help='''
    input file of original intron level score
''')
parser.add_argument('--score_score_col', type=int, help='''
    column number (1-based) of score column in score file
''')
parser.add_argument('--score_intron_col', type=int, help='''
    column number (1-based) of intron column in score file
''')
parser.add_argument('--map_gene_col', type=int, help='''
    column number (1-based) of gene column in map file
''')
parser.add_argument('--map_intron_col', type=int, help='''
    column number (1-based) of intron column in map file
''')
parser.add_argument('--map', help='''
    map file (intron to gene)
''')
parser.add_argument('--method', help='''
    set it to max or min
''')
args = parser.parse_args()


import gzip
import re
import sys

if args.method not in ('max', 'min'):
    print('Wrong method:', args.method)
    sys.exit()

if args.score_sep == 'TAB':
    args.score_sep = '\t'

def myopen(filename, if_gz):
    if if_gz != 0:
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'r')

def get_gene_leve_score_idx(scores, method):
    # return selected idx (0-based)
    selected_value = 0
    if method == 'max':
        selected_value = max(scores)
    elif method == 'min':
        selected_value = min(scores)
    return scores.index(selected_value)

score_dic = {}
header_line = None

with myopen(args.score, args.score_if_gz) as f:
    header_line = next(f)
    for i in f:
        i = i.strip()
        j = i.split(args.score_sep)
        intron = j[args.score_intron_col - 1]
        score_temp = j[args.score_score_col - 1]
        if score_temp == 'NA':
            if args.method == 'max':
                score_temp = 0
            elif args.method == 'min':
                score_temp = 1
        score = float(score_temp)
        if intron not in score_dic:
            score_dic[intron] = (score, i)

map_dic = {}

with myopen(args.map, args.map_if_gz) as f:
    next(f)
    for i in f:
        i = i.strip().split('\t')
        intron = i[args.map_intron_col - 1]
        gene = i[args.map_gene_col - 1]
        if gene not in map_dic:
            map_dic[gene] = []
        map_dic[gene].append(intron)

print(header_line.strip())
for gene in list(map_dic.keys()):
    introns = map_dic[gene]
    scores = []
    idxs = []
    counter = 0
    for intron in introns:
        if intron in score_dic:
            scores.append(score_dic[intron][0])
            idxs.append(counter)
        counter += 1
    if len(scores) == 0:
        continue
    idx = get_gene_leve_score_idx(scores, args.method)
    record = score_dic[introns[idxs[idx]]][1]
    record_split = record.split(args.score_sep)
    record_split[args.score_intron_col - 1] = gene
    record = args.score_sep.join(record_split)
    # record = score_dic[introns[idxs[idx]]][1]
    print(record)
