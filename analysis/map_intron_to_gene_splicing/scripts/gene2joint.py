import argparse
parser = argparse.ArgumentParser(prog='gene2joint.py', description='''
    map gene to intron+gene
    print output
''')
parser.add_argument('--if_gz', type=int, default=1, help='''
    if it is GZ input (if not GZ use 0)
''')
parser.add_argument('--gene_input', help='''
    input file of genes
''')
parser.add_argument('--gene_col', type=int, help='''
    column number (1-based) of gene column
''')
parser.add_argument('--map', help='''
    map (one gene per row)
''')
args = parser.parse_args()


import gzip
import re
import sys

def myopen(filename, if_gz):
    if if_gz != 0:
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'r')

map_dic = {}

with gzip.open(args.map, 'rt') as f:
    for i in f:
        i = i.strip().split('\t')
        gene = i[0]
        intron = i[1]
        gene = gene.split('.')[0]
        intron = re.sub('chr', 'intron_', intron)
        if gene not in map_dic:
            map_dic[gene] = intron
        else:
            print('Wrong map. Exit')
            sys.exit()

with myopen(args.gene_input, args.if_gz) as f:
    for i in f:
        i = i.strip().split('\t')
        gene = i[args.gene_col - 1]
        if gene in map_dic:
            intron = map_dic[gene]
            prev = i[ : (args.gene_col - 1)]
            aftr = i[args.gene_col : ]
            id = intron + ':' + gene
            all = prev + [ id ] + aftr
            print('\t'.join(all))
