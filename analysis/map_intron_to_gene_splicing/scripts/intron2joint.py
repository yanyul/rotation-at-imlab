import argparse
parser = argparse.ArgumentParser(prog='intron2joint.py', description='''
    map intron to intron+gene
    print output
''')
parser.add_argument('--if_gz', type=int, default=1, help='''
    if it is GZ input (if not GZ use 0)
''')
parser.add_argument('--intron_input', help='''
    input file of intron
''')
parser.add_argument('--intron_col', type=int, help='''
    column number (1-based) of intron column
''')
parser.add_argument('--map', help='''
    map
''')
args = parser.parse_args()


import gzip
import re

def myopen(filename, if_gz):
    if if_gz != 0:
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'r')

map_dic = {}

with gzip.open(args.map, 'rt') as f:
    for i in f:
        i = i.strip().split('\t')
        gene = i[0]
        intron = i[1]
        gene = gene.split('.')[0]
        intron = re.sub('chr', 'intron_', intron)
        if intron not in map_dic:
            map_dic[intron] = []
        map_dic[intron].append(gene)

with myopen(args.intron_input, args.if_gz) as f:
    for i in f:
        i = i.strip().split('\t')
        intron = i[args.intron_col - 1]
        if intron in map_dic:
            genes = map_dic[intron]
            for g in genes:
                prev = i[ : (args.intron_col - 1)]
                aftr = i[args.intron_col : ]
                id = intron + ':' + g
                all = prev + [ id ] + aftr
                print('\t'.join(all))
