import argparse
parser = argparse.ArgumentParser(prog='aggregate_intron_gene_map_across_tissues.py', description='''
    Aggregate intron to gene map across all tissues
''')
parser.add_argument('--if_gz', type=int, default=1, help='''
    if it is GZ input (if not GZ use 0)
''')
parser.add_argument('--map_prefix', help='''
    prefix of intron to gene map
''')
parser.add_argument('--map_suffix', help='''
    suffix of intron to gene map
''')
parser.add_argument('--gene_col', type=int, help='''
    column number (1-based) of gene column
''')
parser.add_argument('--intron_col', type=int, help='''
    column number (1-based) of intron column
''')
parser.add_argument('--tissue_str', type=str, help='''
    tissue list separated by `,`
''')
args = parser.parse_args()


import gzip
import re
import sys

def myopen(filename, if_gz):
    if if_gz != 0:
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'r')

intron_gene_map = {}  # key: gene; value: list of intron

tissues = args.tissue_str.strip().split(',')

for tissue in tissues:
    filename = args.map_prefix + tissue + args.map_suffix
    with myopen(filename, args.if_gz) as f:
        next(f)
        for i in f:
            i = i.strip()
            j = i.split('\t')
            gene = j[args.gene_col - 1]
            intron = j[args.intron_col - 1]
            if gene not in intron_gene_map:
                intron_gene_map[gene] = {}
            intron_gene_map[gene][intron] = i

for gene in list(intron_gene_map.keys()):
    this_gene_map = intron_gene_map[gene]
    for intron in list(this_gene_map.keys()):
        print(this_gene_map[intron])

