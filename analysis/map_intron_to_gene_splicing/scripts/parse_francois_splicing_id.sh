input=$1
col=$2
out=$3

zcat $input | tail -n +2 | cut -f $2 | awk -F":" -v OFS="\t" '{print $1"_"$2"_"$3,$5}' | gzip > $out 
