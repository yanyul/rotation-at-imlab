import argparse
parser = argparse.ArgumentParser(prog='add_gtex_variant_id_to_hapmap_snplist.py', description='''
    Using a lookup table, append gtex_variant_id to hapmap snplist
''')
parser.add_argument('--snplist', help='''
    name of snplist (TXT)
''')
parser.add_argument('--lookup_table', help='''
    name of lookup table (GZ)
''')
parser.add_argument('--gtex_col', type=int, help='''
    column number (1-based) of gtex_variant_id column in lookup table
''')
parser.add_argument('--snp_col', type=int, help='''
    column number (1-based) of snp_id column in lookup table
''')
parser.add_argument('--snp_col_snplist', type=int, help='''
    column number (1-based) of snp_id column in snplist
''')
parser.add_argument('--output', help='''
    output file name (TXT)
''')
args = parser.parse_args()

import gzip

var_dic = {}

with gzip.open(args.lookup_table, 'rt') as f:
    for i in f:
        i = i.strip().split('\t')
        var_dic[i[args.snp_col - 1]] = i[args.gtex_col - 1]

o = open(args.output, 'wt')
with open(args.snplist, 'r') as f:
    for i in f:
        i = i.strip().split('\t')
        snp = i[args.snp_col_snplist - 1]
        if snp in var_dic:
            i.append(var_dic[snp])
            o.write('\t'.join(i) + '\n')
