if [ "$3" = "False" ]; then
    zcat $1 | awk -F"\t" -v OFS="\t" '{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14}' | gzip > $2
else
    zcat $1 | awk -F"\t" -v OFS="\t" '{if($14!="imputed"){print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14}}' | gzip > $2
fi
