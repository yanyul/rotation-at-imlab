# See this link https://www.biostars.org/p/222697/
# for information on how to interpret genetic map file
# $1: input genetic map file
# $2: chromosome name
# $3: chain file
# $4: output file name
head -n 1 $1 > $4
liftOver -bedPlus=2 <(cat $1|tail -n +2 |awk -v OFS="\t" -v c=$2 '{print c,$1-1,$1,$2,$3}') $3 $1.liftover $1.liftover.fail

cat $1.liftover | awk -v OFMT=%.10f -v OFS="\t" -v a=1 -v aa=0 '{b=$3-a;c=$5-aa;if(b>0){print $3,$5,b,c;a=$3;aa=$5}}' > $1.liftover.delta

cat $1.liftover.delta | awk -F"\t" -v OFMT=%.15f -v OFS=" " '{d=$4/$3*1e6;print $1,d,$2}' >> $4

rm $1.liftover
rm $1.liftover.fail
rm $1.liftover.delta
