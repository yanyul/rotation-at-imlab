name=( $( cat ../../../../data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 | sed 's/imputed_//' | sed 's/.txt.gz//' | grep 'GIANT_HEIGHT\|UKB_50_Standing_height\|EGG_Pubertal_growth_10F\|EGG_Pubertal_growth_12M\|UKB_20002_1289_self_reported_schizophrenia\|pgc.scz2\|UKB_20016_Fluid_intelligence_score\|ISGC_Malik_2016_METASTROKE_all_strokes') )
file=( $( cat ../../../../data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 | grep 'GIANT_HEIGHT\|UKB_50_Standing_height\|EGG_Pubertal_growth_10F\|EGG_Pubertal_growth_12M\|UKB_20002_1289_self_reported_schizophrenia\|pgc.scz2\|UKB_20016_Fluid_intelligence_score\|ISGC_Malik_2016_METASTROKE_all_strokes') )
# name=${name[0]}
# file=${file[0]}


# e=$( ls /group/im-lab/nas40t2/rbonazzola/GTEx/v8/GTEx_Analysis_v8_eQTL_all_associations | sed 's/.allpairs.txt.gz//')
declare -a snp=('GTEx_v8_EUR' '1kg_EUR')
declare -a snpname=('gtex_variant_id' 'variant_id')
declare -a snppath=('/home/t.cri.yliang/scratch/repo/rotation-at-imlab/data/snplist/w_hm3_kept_gtexID.snplist' '/home/t.cri.yliang/scratch/repo/rotation-at-imlab/data/snplist/w_hm3_kept_rsID.snplist')
declare -a snpprefix=('/home/t.cri.yliang/scratch/repo/rotation-at-imlab/analysis/compute_ldsc/plink2ldsc/GTEx_v8__PCA_EUR/chr' '../../data/1kg_ldsc/1000G_EUR_Phase3_hg38_baseline_only/baseline_only.')
declare -a geno=('vcf2plink/GTEx_v8__PCA_EUR/chr' '../../data/1kg_ldsc/1000G_EUR_Phase3_hg38/plink_files/1000G.EUR.hg38.')
declare -a snplist=('True' 'False')
declare -a impute=('True' 'False')
for (( t=0; t<${#snp[@]}; t++))
  do
  for (( j=0; j<${#name[@]}; j++))
    do
    for(( k=0; k<${#snplist[@]}; k++ ))
      do
      for(( m=0; m<${#impute[@]}; m++ ))
        do
          namei=${name[$j]}
          filei=${file[$j]}
          snpi=${snp[$t]}
          snpnamei=${snpname[$t]}
          snppathi=${snppath[$t]}
          snpprefixi=${snpprefix[$t]}
          genoi=${geno[$t]}
          snplistk=${snplist[$k]}
          imputem=${impute[$m]}
          qsub -v GWAS=$namei,GWASPATH=/group/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38/$filei,SNP=$snpi,SNPLIST=$snppathi,SNPNAME=$snpnamei,SNPPATH=$snpprefixi,GENO=$genoi,USESNP=$snplistk,IMPUTE=$imputem -o $namei\_$snpi.out -N est_h2_selected_traits_$namei\_$snpi -e $namei\_$snpi.err selected_traits.qsub
        done
      done
    done
  done
