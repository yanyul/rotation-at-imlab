if [ -z "$5" ]
then
    awk -v key1="$3" -v key2="$4" -v OFS="\t" -F"\t" 'FNR==NR{a[$(key1)]=$0; next} {if($(key2) in a){print a[$(key2)],$0}}' $1 $2
else
    awk -v key1="$3" -v key2="$4" -v okey2="$5" -v OFS="\t" -F"\t" 'FNR==NR{a[$(key1)]=$0; next} {if($(key2) in a){print a[$(key2)],$(okey2)}}' $1 $2
fi
