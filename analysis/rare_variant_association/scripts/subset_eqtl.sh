eqtl=$1
rare=$2

awk 'FNR==NR{split($1,a,":"); pos[a[1]":"a[2]]=1;next}{split($2,b,"_"); if(b[1]":"b[2] in pos) print $0}' <(cat $rare | tail -n +2) <(zcat < $eqtl | tail -n +2) 
