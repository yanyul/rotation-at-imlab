This module explore the concordance of gene-level mediated effect called from PrediXcan and gene-level effect size called from rare variant association

The analysis plan is the following:

Given a tissue of interest, for each trait

1. Get PrediXcan significant genes (alpha = 0.05 under Bonferroni correction)
2. Post-filter by enloc RCP > 0.01
3. Collect the effect size (and other sum stats) from gene-based rare variant analysis

**Note that**: only keep protein coding genes and genes with gene name successfully mapped to ENSG ID.
