this module records the scripts used to compose the OMIM genes used as gold standard

Workflow:

1. Run `Rscript map_trait_to_hpo_and_mim.R` which will generate `trait-to-hpo-and-mim.txt`
2. Build download `mim2gene.txt` and `genemap2.txt` files from OMIM website
3. Run `Rscript mim-to-gene.R` which will generate `summary_ngene_by_trait.txt`

These output files will be required for the generation of OMIM gene/trait in `../fdr_power_specificity/data/gen_omim_gene_list.R` 
