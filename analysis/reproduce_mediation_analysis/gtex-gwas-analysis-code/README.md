This directory hosts the scripts written by Rodrigo along with the modified version of the code.

They are similar (with uncommited changes) to the ones hosted at [here](https://github.com/hakyimlab/gtex-gwas-analysis/tree/master/code)

These scripts are mainly for side-by-side comparison with the new implementation.

Naming convention:

* Original script: `[NAME].EXT`
* Modified script: `[NAME]_MODIFIED.EXT`

**CAUTION**: the dependent files of these scripts are hosted at `/gpfs/data/im-lab/nas40t2/rbonazzola/GTEx/v8/repos/gtex-gwas-analysis/data/`. Remember to link it as `../data/` relative to this directory

# Test1: concordance of beta_gene between primary and secondary QTL

Scripts:

* Original: `gwas_eqtl_correlation_with_rcp_threshold.R`
* Modified:`gwas_eqtl_correlation_with_rcp_threshold_MODIFIED.R`
* Re-implementation: `../scripts/compute_prim_vs_sec_cor_by_rcp_cutoff.R`

Test runs:

Whole_Blood x GIANT_HEIGHT

See `concordance/test_concordance.sh`

# Test2: cor(|gwas|, |qtl|)

The way to report number of QTL included NA's. In the new implementation, it is fixed. 
