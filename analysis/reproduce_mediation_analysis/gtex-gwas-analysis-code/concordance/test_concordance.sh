tissue=Whole_Blood
trait=GIANT_HEIGHT

cd ../

Rscript gwas_eqtl_correlation_with_rcp_threshold.R \
  --tissue $tissue \
  --phenotype $trait

Rscript gwas_eqtl_correlation_with_rcp_threshold_MODIFIED.R \
  --tissue $tissue \
  --phenotype $trait

cd ../

newpipdir=/gpfs/data/im-lab/nas40t2/yanyul/GTExV8/mediation_analysis_all_indiv

Rscript scripts/compute_prim_vs_sec_cor_by_rcp_cutoff.R \
  --qtl_list $newpipdir/extract_qtl_from_dapg/extracted-from-DAPG.cs_gt_0.25-x-pip_gt_0.01.$tissue.txt.gz \
  --enloc $newpipdir/annotations/enloc/$trait-x-$tissue.txt.gz \
  --qtl_slope $newpipdir/annotations/qtl_effect_size/$tissue.txt.gz \
  --gwas $newpipdir/annotations/gwas/AllTissues-x-$trait.txt.gz \
  --ld $newpipdir/annotations/ld/AllTissues--$tissue.ld_by_gene.gz \
  --file_for_rank $newpipdir/annotations/qtl_effect_size/$tissue.txt.gz \
  --method_for_rank rank_by_qtl_effect_size \
  --rcp_cutoff 0.01,0.05,0.1,0.2,0.5 \
  --output output/eqtl_gwas_correlation/concordance-yoson--$trait-x-$tissue.txt

# prepare Alvaro's enloc output on all individuals as a comparison
filename=/gpfs/data/im-lab/nas40t2/abarbeira/projects/gtex_v8/enloc/eqtl/results/$trait\__PM__$tissue.enloc.rst
echo gwas_locus,molecular_qtl_trait,locus_gwas_pip,locus_rcp,lead_coloc_SNP,lead_snp_scp | tr ',' '\t' > gtex-gwas-analysis-code/temp-$trait-x-$tissue.enloc.txt
cat $filename | awk -F" " -v OFS="\t" '{print $1,$2,$3,$4,$5,$6}'  >> gtex-gwas-analysis-code/temp-$trait-x-$tissue.enloc.txt
gzip gtex-gwas-analysis-code/temp-$trait-x-$tissue.enloc.txt

Rscript scripts/compute_prim_vs_sec_cor_by_rcp_cutoff.R \
  --qtl_list $newpipdir/extract_qtl_from_dapg/extracted-from-DAPG.cs_gt_0.25-x-pip_gt_0.01.$tissue.txt.gz \
  --enloc gtex-gwas-analysis-code/temp-$trait-x-$tissue.enloc.txt.gz \
  --qtl_slope $newpipdir/annotations/qtl_effect_size/$tissue.txt.gz \
  --gwas $newpipdir/annotations/gwas/AllTissues-x-$trait.txt.gz \
  --ld $newpipdir/annotations/ld/AllTissues--$tissue.ld_by_gene.gz \
  --file_for_rank $newpipdir/annotations/qtl_effect_size/$tissue.txt.gz \
  --method_for_rank rank_by_qtl_effect_size \
  --rcp_cutoff 0.01,0.05,0.1,0.2,0.5 \
  --output output/eqtl_gwas_correlation/concordance-alvaro--$trait-x-$tissue.txt

rm gtex-gwas-analysis-code/temp-$trait-x-$tissue.enloc.txt.gz
