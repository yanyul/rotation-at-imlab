inprefix=$1
out=$2

cat $inprefix.chr1.ld > $out.temp
for i in `seq 2 22`
do
  cat $inprefix.chr$i.ld | tail -n +2 >> $out.temp
done

cat $out.temp | gzip > $out
rm $out.temp
