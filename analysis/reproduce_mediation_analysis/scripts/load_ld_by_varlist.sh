ld=$1
varlist=$2

zcat < $ld | head -n 1

awk -F"\t" 'FNR==NR{a[$1]=1 ;next}{b=$3;c=$6; if(b in a && c in a) {print $0}}' <(cat $varlist) <(zcat < $ld | tail -n +2) 
