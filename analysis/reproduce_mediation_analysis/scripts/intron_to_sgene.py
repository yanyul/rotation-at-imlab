import argparse
parser = argparse.ArgumentParser(prog='intron_to_sgene.py', description='''
    Extract LD by gene and by tissue
''')
parser.add_argument('--dapg_intron', help='''
    DAPG output parsed by Alvaro where only intron presents
''')
parser.add_argument('--sqtl', help='''
    sQTL file prepared by Francois's where gene_id is in intron:gene format
''')
parser.add_argument('--output', help='''
    name of output file in TXT.GZ
''')
args = parser.parse_args()

import gzip, re

# procedures:
# read QTL file first and record all sGene's and corresponding introns
# go through DAPG output replace intron id with intron/gene id as used in QTL file

sgene_dic = {}
with gzip.open(args.sqtl, 'rt') as f:
    next(f)
    for i in f:
        i = i.strip().split('\t')
        gene_id = i[1]
        temp = gene_id.split(':')
        chr = re.sub('chr', '', temp[0])
        intron_id = 'intron_{i}_{start}_{end}'.format(i = chr, start = temp[1], end = temp[2])
        if intron_id not in sgene_dic:
            sgene_dic[intron_id] = {}
        if gene_id not in sgene_dic[intron_id]:
            sgene_dic[intron_id][gene_id] = 1

o = gzip.open(args.output, 'wt')

with gzip.open(args.dapg_intron, 'rt') as f:
    header = next(f)
    o.write(header)
    for j in f:
        i = j.strip().split('\t')
        if i[0] in sgene_dic:
            for gene_id in list(sgene_dic[i[0]].keys()):
                i[0] = gene_id
                o.write('\t'.join(i) + '\n')
o.close()
