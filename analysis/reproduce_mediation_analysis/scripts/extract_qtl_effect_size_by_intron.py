import argparse
parser = argparse.ArgumentParser(prog='extract_qtl_effect_size_by_intron.py', description='''
    Extract LD by gene and by tissue
''')
parser.add_argument('--dapg_intron', help='''
    DAPG output parsed by Alvaro where only intron presents
''')
parser.add_argument('--sqtl', help='''
    sQTL file prepared by Francois's where gene_id is in intron:gene format
''')
parser.add_argument('--output', help='''
    name of output file in TXT.GZ
''')
args = parser.parse_args()

import gzip, re

# procedures:
# read DAPG selected sQTLs and record the corresponding intron/variant pairs
# go through QTL sum stats if intron/variant pair matched, replace gene_id with intron ID and record cluster ID at the last column

dapg_selected_sqtl = {}

with gzip.open(args.dapg_intron, 'rt') as f:
    next(f) 
    for j in f:
        i = j.strip().split('\t')
        sdapg = '{intron}--{var}'.format(intron = i[0], var = i[1])
        dapg_selected_sqtl[sdapg] = 1

o = gzip.open(args.output, 'wt')
with gzip.open(args.sqtl, 'rt') as f:
    header = next(f)
    header = header.strip()
    header += '\t'.join(['', 'cluster_id', 'mapped_gene_id'])
    header += '\n'
    o.write(header)
    for i in f:
        i = i.strip().split('\t')
        gene_id = i[0]
        temp = gene_id.split(':')
        chr = re.sub('chr', '', temp[0])
        cluster_id = temp[1]
        intron_id = 'intron_{i}_{start}_{end}'.format(i = chr, start = temp[1], end = temp[2])
        mapped_gene_id = temp[2]
        var_id = i[1]
        sdapg = '{intron}--{var}'.format(intron = intron_id, var = var_id)
        if sdapg in dapg_selected_sqtl:
            i[0] = intron_id
            i.append(cluster_id)
            i.append(mapped_gene_id)
            o.write('\t'.join(i))
            dapg_selected_sqtl.pop(sdapg)
o.close()
