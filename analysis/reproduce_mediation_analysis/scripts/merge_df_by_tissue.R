library(optparse)
option_list = list(
  make_option(c("-p", "--prefix"), type="character", default=NULL,
              help="prefix of input files", metavar="character"),
  make_option(c("-s", "--suffix"), type="character", default=NULL,
              help="suffix of input files", metavar="character"),
  make_option(c("-t", "--trait_list"), type="character", default=NULL,
              help="list of traits to merge from", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="output", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

library(data.table)
options(datatable.fread.datatable = F)
library(dplyr)
# library(stringr)


prefix = opt$prefix
suffix = opt$suffix

traits = read.table(opt$trait_list, header = F, stringsAsFactors = F)$V1

outlist = list()
for(trait in traits) {
  sub = fread(paste0('zcat ', prefix, trait, suffix), header = T, stringsAsFactors = F, sep = '\t')
  outlist[[trait]] = sub
}
saveRDS(outlist, opt$output)
