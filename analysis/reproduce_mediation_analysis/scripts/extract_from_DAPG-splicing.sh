# Formatting DAPG output parsed by Alvaro
# Extra input needed for splicing
# Post-process to extract gene id (gene/intron pair) and limit to sGene only

csfile=$1
pipfile=$2
csthre=$3
pipthres=$4
out=$5
sqtl=$6

awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=$4 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $1"\t"$3"\t"$4"\t"$6"\t"a[b]}}' <(zcat $csfile | tail -n +2) <(zcat $pipfile | tail -n +2) | gzip > $out.temp

Rscript scripts/get_top_variant_within_cluster.R --input $out.temp --output $out.temp.gz

rm $out.temp

# post-processing starts here
python scripts/intron_to_sgene.py --dapg_intron $out.temp.gz --sqtl $sqtl --output $out
# END post-processing

rm $out.temp.gz
