inprefix=$1
out=$2

zcat $inprefix.chr1.txt.gz > $out.temp
for i in `seq 2 22`
do
  zcat $inprefix.chr$i.txt.gz | tail -n +2 >> $out.temp
done

cat $out.temp | gzip > $out
rm $out.temp
