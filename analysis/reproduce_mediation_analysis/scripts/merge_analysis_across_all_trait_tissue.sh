traitlist=$1
tissuelist=$2
prefix=$3
out=$4
suffixin=$5  # for merge1 and merge2 it should be empty. only used for merge3

# if [ -f "$out" ]; then
#     rm $out.temp
# fi

if [[ -z $suffixin ]]
then
  suffixin=txt
fi

tissue1=`cat $2 | head -n 1`
trait1=`cat $1 | head -n 1`

# get header with additional columns on tissue and trait
cat $prefix$trait1-x-$tissue1.$suffixin | head -n 1 | awk '{print $0"\t""tissue""\t""trait"}' > $out.temp

for tissue in `cat $2`
do
  for trait in `cat $1`
  do
    filename=$prefix$trait-x-$tissue.$suffixin
    cat $filename | tail -n +2 | awk -v TISSUE=$tissue -v TRAIT=$trait '{print $0"\t"TISSUE"\t"TRAIT}' >> $out.temp
  done
done
cat $out.temp | gzip > $out
rm $out.temp
