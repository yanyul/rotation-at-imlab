# Formatting DAPG output parsed by Alvaro

csfile=$1
pipfile=$2
csthre=$3
pipthres=$4
out=$5


awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=$4 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $1"\t"$3"\t"$4"\t"$6"\t"a[b]}}' <(zcat $csfile | tail -n +2) <(zcat $pipfile | tail -n +2) | gzip > $out.temp

Rscript scripts/get_top_variant_within_cluster.R --input $out.temp --output $out

rm $out.temp
