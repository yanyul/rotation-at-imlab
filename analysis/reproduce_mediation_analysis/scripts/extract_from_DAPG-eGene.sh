# Formatting DAPG output parsed by Alvaro
# Extra input needed for filtering out non-eGene
# Post-process/Filtering to limit to eGene only

csfile=$1
pipfile=$2
csthre=$3
pipthres=$4
out=$5
eqtl=$6

awk -F"\t" -v pipthres="$pipthres" -v csthre="$csthre" 'FNR==NR{if($4 >= csthre) a[$1"-"$2]=$4 ;next}{b=$1"-"$6; if(b in a && $4 > pipthres) {print $1"\t"$3"\t"$4"\t"$6"\t"a[b]}}' <(zcat $csfile | tail -n +2) <(zcat $pipfile | tail -n +2) | gzip > $out.temp

Rscript scripts/get_top_variant_within_cluster.R --input $out.temp --output $out.temp.gz

rm $out.temp

# post-processing starts here
zcat $out.temp.gz | head -n 1 > $out.temp2
awk -F"\t" 'FNR==NR{a[$2]=1; next}{if($1 in a) {print $0}}' <(zcat $eqtl | tail -n +2) <(zcat $out.temp.gz | tail -n +2) >> $out.temp2
cat $out.temp2 | gzip > $out
# END post-processing

rm $out.temp2
