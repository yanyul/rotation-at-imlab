qtllist=$1
qtlallpair=$2
out=$3

zcat $qtlallpair | head -n 1 | sed 's#phenotype#gene#g' | tr '\n' '\t' > $out.temp
echo "cluster,gene" | tr ',' '\t' >> $out.temp

awk -F"\t" -v OFS="\t" 'FNR==NR{a[$1"-"$2]=1 ;next}{split($1,temp,":");gsub("chr", "",temp[1]);intron="intron_"temp[1]"_"temp[2]"_"temp[3];cluster=temp[4];mgene=temp[5];b=intron"-"$2; if(b in a) {print intron,$2,$3,$4,$5,$6,$7,$8,$9,cluster,mgene}}' <(zcat $qtllist | tail -n +2) <(zcat $qtlallpair | tail -n +2) >> $out.temp


# cat $out.temp | gzip > $out
# rm $out.temp

# merge genes
# Rscript scripts/extract_qtl_effect_size_by_intron_post_merge_genes.R --input $out.temp --output $out
# rm $out.temp
