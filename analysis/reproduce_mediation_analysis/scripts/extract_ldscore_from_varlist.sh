varlist=$1
ldscore=$2
out=$3

zcat $ldscore | head -n 1 > $out.temp

awk 'FNR==NR{a[$1]=1;next}{if($2 in a) print $0}' <(zcat $varlist) <(zcat $ldscore | tail -n +2) >> $out.temp

cat $out.temp | gzip > $out
rm $out.temp
