qtllist=$1
qtlallpair=$2
out=$3

zcat $qtlallpair | head -n 1 | sed 's#phenotype#gene#g' > $out.temp

awk -F"\t" 'FNR==NR{a[$1"-"$2]=1 ;next}{b=$1"-"$2; if(b in a) {print $0}}' <(zcat $qtllist | tail -n +2) <(zcat $qtlallpair | tail -n +2) >> $out.temp

cat $out.temp | gzip > $out
rm $out.temp
