qtllist=$1
enloc=$2
out=$3
col=$4

zcat $enloc | head -n 1 > $out.temp

if [ -z "$col" ]
then
  awk -F"\t" 'FNR==NR{a[$1]=1 ;next}{b=$2; if(b in a) {print $0}}' <(zcat $qtllist | tail -n +2) <(zcat $enloc | tail -n +2) >> $out.temp
else
  awk -F"\t" -v ncol=$col 'FNR==NR{a[$1]=1 ;next}{b=$(ncol); if(b in a) {print $0}}' <(zcat $qtllist | tail -n +2) <(zcat $enloc | tail -n +2) >> $out.temp
fi

cat $out.temp | gzip > $out
rm $out.temp
