import argparse
parser = argparse.ArgumentParser(prog='extract_ld_by_gene.py', description='''
    Extract LD by gene and by tissue
''')
parser.add_argument('--lddb', help='''
    LD database to search from
''')
parser.add_argument('--qtl_pattern', help='''
    pattern of QTL files: path_to_dir/{tissue}.txt.gz
''')
parser.add_argument('--tissue_list', help='''
    list of tissues to work with
''')
parser.add_argument('--output_pattern', help='''
    output file name: path_to_dir/{tissue}.ld.gz (TXT.GZ)
''')
args = parser.parse_args()

import gzip


def get_var_pairs_from_qtl(qtlfile):
    gene_dic = {}
    with gzip.open(qtlfile, 'rt') as f:
        next(f)
        for i in f:
            i = i.strip().split('\t')
            gene = i[0]
            var = i[1]
            if gene not in gene_dic:
                gene_dic[gene] = []
            gene_dic[gene].append(var)
    varpairs = {}
    for i in list(gene_dic.keys()):
        vars = gene_dic[i]
        if len(vars) > 1:
            for j in vars:
                for k in vars:
                    varpairs[j + '---' + k] = 1
    return varpairs

tissues = []
with open(args.tissue_list, 'r') as f:
    for i in f:
        i = i.strip()
        tissues.append(i)

var_pairs_by_tissues = {}
for t in tissues:
    var_pairs_by_tissues[t] = get_var_pairs_from_qtl(args.qtl_pattern.format(tissue = t))

var_pairs_output = {}
for t in tissues:
    var_pairs_output[t] = []

with gzip.open(args.lddb, 'rt') as f:
    header = next(f)
    for j in f:
        i = j.strip().split('\t')
        varpair = i[2] + '---' + i[5]
        for t in tissues:
            if varpair in var_pairs_by_tissues[t]:
                var_pairs_output[t].append(j)

for t in tissues:
    filename = args.output_pattern.format(tissue = t)
    f = gzip.open(filename, 'wt')
    f.write(header)
    for line in var_pairs_output[t]:
        f.write(line)
    f.close()
