This directory reproduces mediation analysis performed by Rodrigo

# Overall workflow

## Extracting finemapped eQTLs

DAPG per cluster top (allow cluster cutoff and per variant PIP cutoff) at tissue/gene/variant level. **Optional**: some filtering on gene (only keep eGene/sIntron etc).

**CAUTION**: for splicing, we need to deal with the intron-gene mapping issue. For this analysis, probably it is more clear to fix the gene-to-intron map at the very beginning.

## Annotate eQTLs

Precisely, some annotations are at variant/trait: GWAS. Some are at tissue/gene/trait level: ENLOC. Some are at tissue/gene/variant level: eQTL effect size. LD information should also be prepared within the extracted DAPG variants.

_Ideally, step 2 can be independent to step 1 since it works from a list of eQTL_

## Reproduce current analysis

1. **Concordance of prim. and sec. beta gene**
2. **Cor(|GWAS|, |QTL|)**
3. **Sigma_gene** (need LD-score annotation)

They can be done by tissue or trait. Also, we can add a filtering step on genes to include eGene only.

# Modules

* Step1: `extract_qtl.snmk`
* Step2: `annotate_qtl.snmk`
* Step3: `reproduce_analysis.snmk`
