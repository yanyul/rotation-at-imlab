# About code

1. GWAS vs eQTL at different RCP threshold:

[https://github.com/hakyimlab/gtex-gwas-analysis/blob/master/code/gwas_eqtl_correlation_with_rcp_threshold.R](https://github.com/hakyimlab/gtex-gwas-analysis/blob/master/code/gwas_eqtl_correlation_with_rcp_threshold.R)

2. Cor(|GWAS|, |eQTL|)

[https://github.com/hakyimlab/gtex-gwas-analysis/blob/master/code/GWAS_eQTL_correlation.R](https://github.com/hakyimlab/gtex-gwas-analysis/blob/master/code/GWAS_eQTL_correlation.R)

3. sigma_genes

# Questions

1. How to reproduce these input files depended by scripts

```
dapg_file_pattern <- "../data/dapg_selected_variants/expression/gwas_and_eqtl/DAPG_with_mashr__{tissue}.rds"
enloc_file_pattern <- "../data/enloc/{tissue}_w_{short_phenotype}_enloc_output.txt.gz"
ld_file_pattern <- "../data/dapg_selected_variants/expression/LD/dapg_filterd_LD_{tissue}.txt"
output_file_pattern <- "../output/eqtl_gwas_correlation/{phenotype}__{tissue}__correlation.txt"
```

Composed one set of data (for one tissue/trait pair) at `/home/t.cri.yliang/labshare/GTExV8/data_for_reproduce_mediation_analysis/gtex-gwas-analysis-data.tar.gz` 

2. ?

```
/gpfs/data/im-lab/nas40t2/rbonazzola/GTEx/v8/DAPG
```

# CAUTION

DAPG files have different tissue names for skin ones
