CONFIG=$1
TISSUE=$2

source ~/.bash_profile
source ~/.bashrc
cd /home/t.cri.yliang/labshare/mv_from_scratch/repo_new/rotation-at-imlab/analysis/reproduce_mediation_analysis/
conda activate pleio
snakemake -s extract_qtl.snmk --configfile qsub/extract/config.$CONFIG.yaml -p --config tissue=$TISSUE > qsub/extract/logs/$CONFIG--$TISSUE.log 2>&1
