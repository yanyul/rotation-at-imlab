# ARGV1: name of config file in this directory in the form: config.NAME.yaml
# ARGV2: add something here if only want to run locally otherwise do not SPECIFY anything and it will submit jobs

configname=$1
local=$2

mkdir -p logs

tissues=`cat ../../../../data/tissues.txt`

for tissue in $tissues
do
  echo $tissue
  if [ ! -z "$local" ]
  then
    bash run.sh $configname $tissue
  else
    qsub -v CONFIG=$configname,TISSUE=$tissue -N $configname-$tissue run.qsub
  fi
done
