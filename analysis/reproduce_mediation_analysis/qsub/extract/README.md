# About archived configs

I archived the configfiles used for building the pipelines and replicating Rodrigo's runs (to use the same data even though they may not be the perfect/finalized setting we want).

# New settings

DAPG: EUR
QTL: EUR
ENLOC: EUR
LD: EUR
LDscore: EUR

See `config.expression.yaml`

# Run

* Expression

```
$ conda activate pleio
$ bash submit.sh expression local   # run locally. Remember to load conda envir first
```

* Splicing

```
$ bash submit.sh splicing local   # run locally. Remember to load conda envir first
```
