# About

Similar to `../extract/`, I archived config files used for testing.

# New settings

DAPG: EUR
QTL: EUR
ENLOC: EUR
LD: EUR
LDscore: EUR

# Run

* Expression

```
$ qsub -v CONFIG=expression run_gwas_and_ld.qsub  # annotate with GWAS, LD, LD-score
$ bash submit_by_tissue.sh expression all_qtl_effect_size  # submit jobs by tissue
$ bash submit_by_tissue.sh expression all_enloc local  # run by tissue locally
```

* Splicing

```
$ qsub -v CONFIG=splicing run_gwas_and_ld.qsub
$ bash submit_by_tissue.sh splicing all_qtl_effect_size
$ bash submit_by_tissue.sh splicing all_enloc local  
```
