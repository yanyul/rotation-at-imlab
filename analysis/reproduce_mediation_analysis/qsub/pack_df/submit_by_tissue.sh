## ARGV1: name of configfile: config.NAME.yaml
## ARGV2: target rule name in qsub script to use: run_by_tissue.qsub
## ARGV3: specify if want to run locally
## ARGV4: tissue list if not specified will use all tissues

myconfig=$1
myqsub=$2
mylocal=$3

if [[ ! -z $4 ]]
then
  tissuelist=$4
else
  tissuelist=../../../../data/tissues.txt
fi

for tissue in `cat $tissuelist`
do
  echo $tissue
  if [ "$mylocal" == "local" ]
  then
    bash run_one_tissue.sh $myconfig $myqsub $tissue
  else
    qsub -v TISSUE=$tissue,CONFIG=$myconfig,RULE=$myqsub -N $myqsub-$tissue run_one_tissue.qsub
  fi
done
