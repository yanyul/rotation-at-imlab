CONFIG=$1
RULE=$2
TISSUE=$3

source ~/.bash_profile
source ~/.bashrc
cd /home/t.cri.yliang/labshare/mv_from_scratch/repo_new/rotation-at-imlab/analysis/reproduce_mediation_analysis/
conda activate pleio
snakemake -s pack_rds.snmk --configfile qsub/pack_df/config.$CONFIG.yaml -p $RULE --nolock --config tissue=$TISSUE > qsub/pack_df/logs/$CONFIG-$TISSUE-$RULE.log 2>&1
