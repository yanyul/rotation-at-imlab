# Prepare DAPG eQTL and annotations

Definition: variant-level PIP > 0.01 and cluster-level PIP > 0.25, and select the top per cluster

## Extract DAPG eQTL

```
$ cd extract
$ bash submit.sh dapg local
```

## Annotate GWAS, LD, and LD-score

```
$ cd annotate
$ qsub -v CONFIG=gwas-and-ld run_gwas_and_ld.qsub
```

## Annotate ENLOC

```
$ cd annotate
$ bash submit_by_tissue.sh enloc-and-qtl_effect_size all_enloc
```

## Annotate QTL effect size

```
$ cd annotate
$ bash submit_by_tissue.sh enloc-and-qtl_effect_size all_qtl_effect_size
```

# Reproduce analysis

See `README.md` at `reproduce/`

# Pack data frames for Josh

```
$ cd pack_df
$ bash submit_by_tissue.sh [CONFIG-NAME] [RULE-NAME]  # rule: all: pack by tissue; all2: pack all in one
```
