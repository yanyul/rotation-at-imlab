myconfig=$1
myrule=$2
checkifdone=$3
debug=$4

for i in `cat ../../../../data/traits.txt`
do
  # check if job has been done
  if [[ ! -z $checkifdone ]]
  then
    mylog=logs/$myrule--$myconfig--$i.log
    # check if log has been generated
    if [[ -f "$mylog" ]]
    then
      e=`cat $mylog|tail -n 2|grep done`

      # check if there is done in log
      if [[ ! -z $e ]]
      then
        continue
      fi
    fi
  fi
  if [[ -z $debug ]]
  then
    qsub -v CONFIG=$myconfig,RULE=$myrule,TRAIT=$i -N $myrule--$i run_on_one_trait.qsub
  else
    echo qsub -v CONFIG=$myconfig,RULE=$myrule,TRAIT=$i -N $myrule--$i run_on_one_trait.qsub
  fi
done
