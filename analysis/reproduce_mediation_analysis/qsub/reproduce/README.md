Throughout the submission, `all traits` are listed in `../../../../data/traits.txt` and `all tissues` are listed in `../../../../data/tissues.txt`

`[CONFIG-NAME]` contains all parameters used for the analysis. It should correspond to a file called `config.[CONFIG-NAME].yaml` in this folder

See options for `[RULE]` in `reproduce_analysis.snmk`:

* all trait/tissue pairs: `all1`, `all2`, `all3`
* one trait all tissues: `all1_onetrait`, `all2_onetrait`, `all3_onetrait`

# prim_vs_sec_by_rcp

Per tissue/trait computation is fast and it will run all trait/tissue pairs in one job

```
$ bash submit.sh [CONFIG-NAME] all1_onetrait
```

After all runs finished, run

```
$ qsub -v CONFIG=[CONFIG-NAME],RULE=all1 run_on_all_pairs.qsub
```

or run `all2` locally.

# cor_gwas_qtl

The submission is by trait and run on all tissues in one job.

```
$ qsub -v CONFIG=[CONFIG-NAME],RULE=all2 run_on_all_pairs.qsub
```

or run by trait as `prim_vs_sec_rcp`

# sigma_gene

It will loop over a list of models set in CONFIGFILE (model_list)

```
$ bash submit.sh [CONFIG-NAME] all3_onetrait
```

# General usage of `submit.sh`

```
$ bash submit.sh [CONFIG-NAME] [RULE-ALL-ONETRAIT] [CHECK-IF-DONE] [NOT-SUBMIT-ONLY-PRINT]
```

# Archiving configs

I archived the scripts for testing/replicating Rodrigo's results. The new runs will focus on the desired settings:

DAPG: EUR
QTL: EUR
ENLOC: EUR
LD: EUR
LDscore: EUR

# Runs

* Expression

```
# submit by tissue
$ bash submit.sh expression all1_onetrait  # run prim_vs_sec_by_rcp
$ bash submit.sh expression all2_onetrait  # run cor_gwas_qtl
$ bash submit.sh expression all3_onetrait  # run sigma_gene
```

* Splicing

```
# submit by tissue
$ bash submit.sh splicing all2_onetrait  # run cor_gwas_qtl
$ bash submit.sh splicing all3_onetrait  # run sigma_gene
```
