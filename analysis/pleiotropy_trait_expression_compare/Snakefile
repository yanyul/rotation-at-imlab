# This module works with data sets shared by Ron Do's lab on horizontal pleiotropy score (Pn/Pm), https://www.biorxiv.org/content/early/2018/04/30/311332
# The working flow is
# 1. Annotate eQTL file with num_sig_gene
#   The idea is that eQTL variant is a subset of all GTEx variant and num_sig_gene only has variants with non-zero num_sig_gene. So, left joining eQTL with num_sig_gene data can solve the problem
# 2. Intersect Ron data and annotated eQTL
#   Here, a summary file should be generated to record how they overlap
# 3. Exploratory analysis
#   3.1 Scatter plot: tPn vs. ePn (summary.snakemake)
#   3.2 Cross-tissue comparison of ePn (summary.snakemake)
#   3.3 Show result of one tissue
# 4. Genome-wide Pn plot (by tissue/trait set)
#   4.1 By tissue
#   4.2 By trait set

if 'keyword' not in list(config['RON'].keys()):
    config['RON']['keyword'] = 'l0norm2$'

rule all_join_ron:
    input:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.intersect.gz'.format(tissue = config['tissue'], eqtl = config['EQTL']['name'], num_sig_gene = config['NUM_SIG_GENE']['name'], ron = config['RON']['name'])

rule all_tissue_plot:
    input:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.panels.png'.format(tissue = config['tissue'], eqtl = config['EQTL']['name'], num_sig_gene = config['NUM_SIG_GENE']['name'], ron = config['RON']['name'])

rule all_simple_plot:
    input:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.simple_Pm.png'.format(tissue = config['tissue'], eqtl = config['EQTL']['name'], num_sig_gene = config['NUM_SIG_GENE']['name'], ron = config['RON']['name']),
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.simple_Pn.png'.format(tissue = config['tissue'], eqtl = config['EQTL']['name'], num_sig_gene = config['NUM_SIG_GENE']['name'], ron = config['RON']['name'])

# 1
rule annotate_eqtl_prepare:
    input:
        config['EQTL']['path']
    output:
        temp('annotated_eqtl/{tissue}__{eqtl}.bed')
    shell:
        '''
        cat {input[0]} | tail -n +2 | awk -F"\\t" -v OFS="\\t" '{{split($2,a,"_");print a[1],a[2]-1,a[2],$2}}' > {output[0]}
        '''

rule annotate_eqtl_sort:
    input:
        'annotated_eqtl/{tissue}__{eqtl}.bed'
    output:
        'annotated_eqtl/{tissue}__{eqtl}.sorted_bed'
    shell:
        'cat {input[0]} | sort -k1,1 -k2,2n | uniq > {output[0]}'

rule annotate_eqtl_intersect:
    input:
        gtex = config['NUM_SIG_GENE']['path'],
        eqtl = 'annotated_eqtl/{tissue}__{eqtl}.sorted_bed'
    output:
        'annotated_eqtl/{tissue}__{eqtl}__{num_sig_gene}.gz'
    shell:
        '''
        bedtools intersect -a {input.eqtl} -b {input.gtex} -loj -sorted | awk -F"\t" -v OFS="\t" '{{a=$1; for(i = 2; i <= NF-5; i++) a=a"\t"$i; last=0; if($(NF-1)!=".") last=$(NF); a=a"\t"last; print a}}' | gzip > {output[0]}
        '''
# 1 END

# 2
rule join_ron_intersect:
    input:
        ron = config['RON']['path'],
        eqtl = 'annotated_eqtl/{tissue}__{eqtl}__{num_sig_gene}.gz'
    output:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.intersect.gz'
    shell:
        'bedtools intersect -a <(tail -n +2 {input.ron}) -b <(zcat {input.eqtl}) -loj | gzip > {output[0]}'
# 2 END

# 3.3
rule tissue_plot:
    input:
        data = 'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.intersect.gz',
        header = config['RON']['path']
    params:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}',
        config['RON']['keyword']
    output:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.count.txt',
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.panels.png'
    shell:
        'Rscript scripts/plot_one_tissue.R --input {input.data} --header {input.header} --output_prefix {params[0]} --keyword {params[1]}'
# 3.3 END

# simple plot
rule simple_plot:
    input:
        data = 'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.intersect.gz',
        header = config['RON']['path']
    params:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}',
        config['RON']['keyword']
    output:
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.simple_Pm.png',
        'eqtl_ron_joined/{tissue}__{eqtl}__{num_sig_gene}__{ron}.simple_Pn.png'
    shell:
        'Rscript scripts/plot_one_tissue_simple.R --input {input.data} --header {input.header} --output_prefix {params[0]}'
# END

# # 4
# rule genome_wide_by_tissue:
#     input:
#         config['NUM_SIG_GENE']['path']
#     output:
#         'genome_wide_plot/{tissue}__{num_sig_gene}.png'
# # 4 END
