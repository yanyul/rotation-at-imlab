TYPE=$1
TRAIT=$2

cd /home/t.cri.yliang/labshare/mv_from_scratch/repo_new/rotation-at-imlab/analysis/fdr_power_specificity/

snakemake --configfile qsub/config.$TYPE.yaml -p --config trait=$TRAIT > qsub/logs/$TYPE--$TRAIT.log 2>&1

