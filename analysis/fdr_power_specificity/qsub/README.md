To work with splicing data, first format PrediXcan, MultiXcan, ENLOC result to gene-level using `../../map_intron_to_gene_splicing/get_gene_level_score.snmk`

To submit jobs, specify everything in configfile and do something like

```
$ for i in `cat ../data/omim/trait-list-omim.txt`; do qsub -v TRAIT=$i,TYPE=ldblock-omim-gene-splicing -N omim-$i run.qsub ; done
```
