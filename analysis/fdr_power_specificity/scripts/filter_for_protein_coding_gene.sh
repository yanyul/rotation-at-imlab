eqtl=$1
out=$2

geneannot=/gpfs/data/im-lab/nas40t2/yanyul/mv_from_scratch/repo_new/rotation-at-imlab/data/annotations_gencode_v26.tsv

zcat $1 | head -n 1 > $out.temp

awk 'FNR==NR{a[$1]=1;next}{split($2,b,"."); if(b[1] in a) print $0}' <(cat $geneannot | grep 'protein_coding') <(zcat $1 | tail -n +2) >> $out.temp

cat $out.temp | gzip > $out
rm $out.temp
