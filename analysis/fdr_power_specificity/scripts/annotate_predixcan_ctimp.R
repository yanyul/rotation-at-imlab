library(optparse)
option_list = list(
  make_option(c("-p", "--prefix"), type="character", default=NULL,
              help="ENLOC file prefix (generated and formatted by Alvaro)", metavar="character"),
  make_option(c("-s", "--suffix"), type="character", default=NULL,
              help="suffix", metavar="character"),
  make_option(c("-g", "--gwas"), type="character", default=NULL,
              help="gwas leading variant and extracted gene list", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="output", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

library(data.table)
options(datatable.fread.datatable = F)
library(dplyr)
library(stringr)
source('../../scripts/mylib.R')
source('scripts/rlib.R')

tissues = read.csv('../../data/gtex_sample_counts_by_tissue.csv'); tissues = tissues %>% filter(v8_all > 35); tissues$tissue[tissues$tissue == 'Cells_Transformed_fibroblasts'] = 'Cells_Cultured_fibroblasts'


predixcan = read_gz_and_merge(opt$prefix, opt$suffix, tissues$tissue, cmd = 'cat ', sep = ',')
predixcan = predixcan %>% mutate(gene = trim_dot(gene)) %>% group_by(gene) %>% summarize(pvalue = min(pvalue, na.rm = T)) %>% ungroup()

extracted_genes = readRDS(opt$gwas)$extracted_genes
if(nrow(extracted_genes) == 0) {
  saveRDS(extracted_genes, opt$output)
  quit()
}
annotation = left_join(extracted_genes, predixcan, by = 'gene')
saveRDS(annotation, opt$output)
