library(optparse)
option_list = list(
  make_option(c("-c", "--clump"), type="character", default="test",
              help="clump file path", metavar="character"),
  make_option(c("-g", "--gene_list"), type="character", default="test",
              help="gene model", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="output", metavar="character"),
  make_option(c("-d", "--dist_to_gene_body"), type="numeric", default=2e6,
              help="cutoff on distance to gene body to extract a gene", metavar="character"),
  make_option(c("-l", "--gene_list_filter"), type="character", default=NULL,
              help="a list of gene", metavar="character"),
  make_option(c("-i", "--gene_list_cutoff"), type="numeric", default=1e6,
              help="distance cutoff for include a GWAS loci by closeness to gene list", metavar="character"),
  make_option(c("-t", "--gene_list_cutoff_type"), type="character", default=NULL,
              help="set to gene_body if want to use distance to gene body otherwise distance to tss will be used", metavar="character"),
  make_option(c("-w", "--gwas_pval_gt"), type="numeric", default=NULL,
              help="if only want to use large p-value (to mimic GWAS with low sample size)", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);


library(data.table)
options(datatable.fread.datatable = F)
library(dplyr)
source('../../scripts/mylib_doc.R')
source('../../scripts/mylib_generic.R')
source('scripts/rlib.R')

get_gwas_lead_from_plink_clump = function(filename, cutoff_p_gt) {
  df = read.table(filename, header = T, sep = '\t')
  df = df[, -ncol(df)]
  if(nrow(df) == 0) {
    return(data.frame())
  }
  o = data.frame(cs_idx = 1 : nrow(df), lead_var = df$SNP, chr = paste0('chr', df$CHR), pos = df$BP, stringsAsFactors = F)
  if(!is.null(cutoff_p_gt)) {
    o = o[df$P > cutoff_p_gt, ]
  }
  o
}


gene_map = read.table(opt$gene_list, header = T) %>% filter(gene_type == 'protein_coding')
gene_map$tss = get_tss(gene_map$start, gene_map$end, gene_map$strand)

gwas_lead = get_gwas_lead_from_plink_clump(opt$clump, opt$gwas_pval_gt)
if(nrow(gwas_lead) == 0) {
  saveRDS(list(gwas_leading_variant = gwas_lead, extracted_genes = data.frame()), opt$output)
  quit()
}


if(!is.null(opt$gene_list_filter)) {
  gene_list = read.table(opt$gene_list_filter, header = F, stringsAsFactors = F)$V1
  gene_map_this = gene_map %>% filter(gene_id %in% gene_list)
  if(is.null(opt$gene_list_cutoff_type)) {
    gwas_lead = post_filter_gwas_by_causal_gene_position(gwas_lead, gene_map_this$chromosome, gene_map_this$tss, dist_to_tss_cutoff = opt$gene_list_cutoff)
  } else {
    if(opt$gene_list_cutoff_type != 'gene_body') {
      gwas_lead = post_filter_gwas_by_causal_gene_position(gwas_lead, gene_map_this$chromosome, gene_map_this$tss, dist_to_tss_cutoff = opt$gene_list_cutoff)
    } else {
      gwas_lead = post_filter_gwas_by_causal_gene_position_by_gene_body(gwas_lead, gene_map_this$chromosome, gene_map_this$start, gene_map_this$end, dist_to_gene_body_cutoff = opt$gene_list_cutoff)
    }
  }

}

loci_pos = gwas_lead[, c('chr', 'pos')]
df_extracted_genes_tmp = data.frame()
if(nrow(gwas_lead) == 0) {
  saveRDS(list(gwas_leading_variant = gwas_lead, extracted_genes = df_extracted_genes_tmp), opt$output)
  quit()
}
for(i in 1 : nrow(gwas_lead)) {
  sub = gene_map %>% filter(chromosome == loci_pos$chr[i])
  sub = sub %>% mutate(dist_to_gene_body = get_distance(start, end, loci_pos$pos[i]), dist_to_tss = abs(tss - loci_pos$pos[i])) %>% filter(dist_to_gene_body <= opt$dist_to_gene_body)
  if(nrow(sub) == 0) {
    next
  }
  sub$variant = gwas_lead$lead_var[i]
  df_extracted_genes_tmp = rbind(df_extracted_genes_tmp, sub)
}
df_extracted_genes_tmp = df_extracted_genes_tmp %>% rename(gene = gene_id) %>% group_by(gene) %>% summarize(dist_to_tss = min(dist_to_tss), dist_to_gene_body = min(dist_to_gene_body)) %>% ungroup()

saveRDS(list(gwas_leading_variant = gwas_lead, extracted_genes = df_extracted_genes_tmp), opt$output)
