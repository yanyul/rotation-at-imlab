# steps:
#   1. GWAS loci -> GWAS leading variant -> extracted genes
#   2. Annotate extracted genes (by method: multixcan, enloc, coloc, nearest gene, EWAS) with GWAS leading variant

out = []
if 'scores' in config:
    for i in list(config['scores']):
        out.append('score_' + i)
out.append('nearest_gene')
out.append('random_precision')

region_based_cmd = ''
if 'GWAS-ldblock' in config:
    region_based_cmd = '--region_based 1'

if 'GWAS-ldblock' in config:
    config['distance_to_gene_body'] = 'region-based'

if 'outdir' not in config:
    config['outdir'] = 'output'

rule all:
    input:
        [ '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.{type}.rds'.format(outdir = config['outdir'], gene_body_distance = config['distance_to_gene_body'], trait = config['trait'], type = i) for i in out ]

if 'GWAS-susie' in config:
    rule gwas_loci_to_extracted_gene:
        input:
            cs = config['GWAS-susie']['cs'],
            pip = config['GWAS-susie']['pip'],
            gene_list = config['gene-list']
        output:
            '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.rds'
        shell:
            'Rscript scripts/susie2gene.R --cs {input.cs} --pip {input.pip} --gene_list {input.gene_list} --output {output[0]} --dist_to_gene_body {wildcards.gene_body_distance}'

elif 'GWAS-clump' in config:
    gene_list_cmd = ''
    if 'gene-list' in config['GWAS-clump']:
        if 'path' in config['GWAS-clump']['gene-list']:
            gene_list_cmd = '--gene_list_filter ' + config['GWAS-clump']['gene-list']['path']
        if 'cutoff' in config['GWAS-clump']['gene-list']:
            gene_list_cmd += ' --gene_list_cutoff ' + str(config['GWAS-clump']['gene-list']['cutoff'])
        if 'cutoff_type' in config['GWAS-clump']['gene-list']:
            gene_list_cmd += ' --gene_list_cutoff_type ' + str(config['GWAS-clump']['gene-list']['cutoff_type'])
    gwas_cutoff_cmd = ''
    if 'gwas_cutoff' in config['GWAS-clump']:
        gwas_cutoff_cmd = '--gwas_pval_gt ' + str(config['GWAS-clump']['gwas_cutoff'])
    rule gwas_loci_to_extracted_gene:
        input:
            clump = config['GWAS-clump']['clump'],
            gene_list = config['gene-list']
        params:
            gene_list_cmd = gene_list_cmd,
            gwas_cutoff_cmd = gwas_cutoff_cmd
        output:
            '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.rds'
        shell:
            'Rscript scripts/clump2gene.R --clump {input.clump} --gene_list {input.gene_list} --output {output[0]} --dist_to_gene_body {wildcards.gene_body_distance} {params.gene_list_cmd} {params.gwas_cutoff_cmd}'

elif 'GWAS-ldblock' in config:
    gwas_cutoff_cmd = ''
    if 'gwas_cutoff_gt' in config['GWAS-ldblock']:
        gwas_cutoff_cmd = '--gwas_pval_gt ' + str(config['GWAS-ldblock']['gwas_cutoff_gt'])
    if 'gwas_cutoff_lt' in config['GWAS-ldblock']:
        gwas_cutoff_cmd += ' --gwas_pval_lt ' + str(config['GWAS-ldblock']['gwas_cutoff_lt'])
    gene_list_cmd = ''
    if 'gene-list' in config['GWAS-ldblock']:
        if 'path' in config['GWAS-ldblock']['gene-list']:
            gene_list_cmd = '--gene_list_filter ' + config['GWAS-ldblock']['gene-list']['path']
    rule gwas_loci_to_extracted_gene:
        input:
            gwas = config['GWAS-ldblock']['sumstat'],
            ldblock = config['GWAS-ldblock']['ldblock'],
            gene_list = config['gene-list']
        params:
            gene_list_cmd = gene_list_cmd,
            gwas_cutoff_cmd = gwas_cutoff_cmd
        output:
            '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.rds'
        shell:
            'Rscript scripts/ldblock2gene.R --gwas {input.gwas} --gene_list {input.gene_list} --ldblock {input.ldblock} --output {output[0]} {params.gene_list_cmd} {params.gwas_cutoff_cmd}'

rule annotate_score:
    input:
        gwas = '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.rds'
    params:
        prefix = lambda wildcards: config['scores'][wildcards.score]['prefix'].format(trait = wildcards.trait),
        suffix = lambda wildcards: config['scores'][wildcards.score]['suffix'].format(trait = wildcards.trait)
    log:
        '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.score_{score}.log'
    output:
        '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.score_{score}.rds'
    shell:
        'Rscript scripts/annotate_{wildcards.score}.R --prefix {params.prefix} --suffix {params.suffix} --gwas {input.gwas} --output {output[0]} > {log} 2>&1'

rule nearest_gene:
    input:
        gwas = '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.rds',
        gene_list = config['gene-list']
    output:
        '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.nearest_gene.rds'
    shell:
        'Rscript scripts/annotate_nearest_gene.R --gwas {input.gwas} --output {output[0]} --gene_list {input.gene_list}'

rule random_precision:
    input:
        gwas = '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.rds',
        gene_list = config['gene-list']
    output:
        '{outdir}/gwas_in_{gene_body_distance}_bp-{trait}.random_precision.rds'
    shell:
        'Rscript scripts/annotate_random_precision.R --gwas {input.gwas} --output {output[0]} --gene_list {input.gene_list} {region_based_cmd}'
