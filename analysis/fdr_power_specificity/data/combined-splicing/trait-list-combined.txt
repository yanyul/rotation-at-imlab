Astle_et_al_2016_Eosinophil_counts
Astle_et_al_2016_Lymphocyte_counts
Astle_et_al_2016_Monocyte_count
Astle_et_al_2016_Neutrophil_count
Astle_et_al_2016_White_blood_cell_count
CARDIoGRAM_C4D_CAD_ADDITIVE
DIAGRAM_T2D_TRANS_ETHNIC
GIANT_WC_Combined_EUR
GLGC_Mc_HDL
GLGC_Mc_LDL
GLGC_Mc_TG
IBD.EUR.Inflammatory_Bowel_Disease
IBD.EUR.Ulcerative_Colitis
ICBP_DiastolicPressure
ICBP_SystolicPressure
IGAP_Alzheimer
IMMUNOBASE_Systemic_lupus_erythematosus_hg19
pgc.scz2
RA_OKADA_TRANS_ETHNIC
UKB_1200_Sleeplessness_or_insomnia
UKB_20002_1065_self_reported_hypertension
UKB_20002_1094_self_reported_deep_venous_thrombosis_dvt
UKB_20002_1111_self_reported_asthma
UKB_20002_1222_self_reported_type_1_diabetes
UKB_20002_1226_self_reported_hypothyroidism_or_myxoedema
UKB_20002_1452_self_reported_eczema_or_dermatitis
UKB_20002_1453_self_reported_psoriasis
UKB_20002_1466_self_reported_gout
UKB_20002_1473_self_reported_high_cholesterol
UKB_21001_Body_mass_index_BMI
UKB_50_Standing_height
