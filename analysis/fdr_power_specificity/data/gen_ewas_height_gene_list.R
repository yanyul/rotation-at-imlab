library(dplyr)

# Load gene annotation
# focus on protein coding genes
gene_map = read.table('../../../data/annotations_gencode_v26.tsv', header = T) %>% filter(gene_type == 'protein_coding')

# Set gene list collector
df = data.frame() # gene, trait pairs

# Height from Nature 2017 EWAS
# All significance in variant based test
nature_2017_ewas = read.csv('~/Desktop/rare_variant/height_rare_variant_nature_2017_vartest.csv')
nature_2017_ewas = nature_2017_ewas %>% filter(VEP.annotation %in% c('missense_variant', 'splice_acceptor_variant', 'splice_donor_variant', 'splice_region_variant', 'start_lost', 'stop_gained'))
nature_2017_ewas = left_join(nature_2017_ewas %>% rename(gene_name = Gene), gene_map %>% select(gene_name, gene_id, gene_type), by = 'gene_name') %>% rename(gene = gene_id)
nature_2017_ewas = nature_2017_ewas %>% filter(!is.na(gene))
nature_2017_ewas = nature_2017_ewas %>% group_by(gene) %>% summarize(nobs = n())
# END

df = rbind(df, data.frame(gene = unique(nature_2017_ewas$gene), trait = 'height')) %>% ungroup()



# Map trait to GWAS data set
gwass = c('UKB_50_Standing_height', 'GIANT_HEIGHT')


# Write gene list by GWAS data set
for(i in gwass) {
  sub = df %>% filter(trait == 'height')
  write.table(sub$gene, paste0('ewas-height/trait-to-gene-ewas-height', '.', i, '.txt'), row = F, col = F, quo = F)
  write.table(sub %>% select(gene) %>% mutate(trait = i), paste0('ewas-height/trait-to-gene-ewas-height', '.', i, '.full-list.txt'), row = F, col = T, quo = F)
}
write.table(gwass, paste0('ewas-height/trait-list-ewas-height', '.txt'), row = F, col = F, quo = F)
