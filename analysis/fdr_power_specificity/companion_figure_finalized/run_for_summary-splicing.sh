# About plot_with_iterative_method.R & test_with_logistic.R options
#
# -e: enloc cutoff when applying enloc jointly with other methods
# -y: ylim in plot (PR curves)
# -w: window size as shown in input file (the window size acting on distance to gene body used in extracting candidate genes)
# -s: tss_cutoff used in calculating average performance of random selection (with tss cutoff for defining candidate genes)
# -n: gene_body_cutoff used to link a locus to a gene for variant-based output. For region-based one, set it to Inf
# -c: cutoff_mask to set all genes with tss distance (minimum across all loci) larger than this cutoff to non-significant (always consider them as not significant in subsequent analysis). FIXME: we did not apply this at locus level so that it is a bit off for per-locus analysis. It is better to use Inf to avoid this imperfection
# -u: output directory
# -i: include a method from predixcan family or smr or color in logistic test

u=summary_on_092719_splicing

# region-based approach (by LD block)

# common variables
e=0.1
w='region-based'
s=1e6
n=Inf
c=Inf

# by approach
declare -a approachnames=("LD-block")
declare -a inputprefixs=("~/Desktop/rare_variant/new-pipeline-newrun-on-summary/output-ldblock")
# by gene set
declare -a genesets=("EWAS" "OMIM")
declare -a ylims=(0.6 0.4)
declare -a traitlists=("../data/ewas/trait-list-omim-maf_0.01.txt" "../data/omim/trait-list-omim.txt")
declare -a traitgenelists=("../data/ewas/trait-to-gene-ewas-maf_0.01.full-list.txt" "../data/omim/trait-to-gene-omim.full-list.txt")
declare -a inputsuffixs=("-ewas-gene-maf_0.01-splicing" "-omim-gene-splicing")

# loop over
for a in "${!approachnames[@]}"; do
  for g in "${!genesets[@]}"; do
    Rscript plot_with_iterative_method.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}" \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -e $e \
      -y "${ylims[$g]}" \
      -w $w \
      -s $s \
      -n $n \
      -c $c \
      -u $u \
      -x score_predixcan_mashr,score_smr,score_coloc_yoson,score_predixcan_en_dapgw_eur
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-PrediXcan-DAPGW \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_predixcan_en_dapgw
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-PrediXcan-MASH-EUR \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_predixcan_mashr_eur
  done
done
