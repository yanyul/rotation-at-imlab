# About plot_with_iterative_method.R & test_with_logistic.R options
#
# -e: enloc cutoff when applying enloc jointly with other methods
# -y: ylim in plot (PR curves)
# -w: window size as shown in input file (the window size acting on distance to gene body used in extracting candidate genes)
# -s: tss_cutoff used in calculating average performance of random selection (with tss cutoff for defining candidate genes)
# -n: gene_body_cutoff used to link a locus to a gene for variant-based output. For region-based one, set it to Inf
# -c: cutoff_mask to set all genes with tss distance (minimum across all loci) larger than this cutoff to non-significant (always consider them as not significant in subsequent analysis). FIXME: we did not apply this at locus level so that it is a bit off for per-locus analysis. It is better to use Inf to avoid this imperfection
# -u: output directory
# -i: include a method from predixcan family or smr or color in logistic test

u=summary_on_083119

# variant-based approach

# common variables
e=0.1
w='1e6'
s=1e6
n=1e6
c=Inf

# by approach
declare -a approachnames=("GWAS-sparse" "GWAS-dense")
declare -a inputprefixs=("~/Desktop/rare_variant/new-pipeline-newrun-on-summary/output-clump-r2_0-kb_500" "~/Desktop/rare_variant/new-pipeline-newrun-on-summary/output-clump-r2_0.2-kb_500")
# by gene set
declare -a genesets=("EWAS" "OMIM")
declare -a ylims=(0.6 0.4)
declare -a traitlists=("../data/ewas/trait-list-omim-maf_0.01.txt" "../data/omim/trait-list-omim.txt")
declare -a traitgenelists=("../data/ewas/trait-to-gene-ewas-maf_0.01.full-list.txt" "../data/omim/trait-to-gene-omim.full-list.txt")
declare -a inputsuffixs=("-ewas-gene-maf_0.01" "-omim-gene")

# loop over
for a in "${!approachnames[@]}"; do
  for g in "${!genesets[@]}"; do
    Rscript plot_with_iterative_method.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}" \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -e $e \
      -y "${ylims[$g]}" \
      -w $w \
      -s $s \
      -n $n \
      -c $c \
      -u $u
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-PrediXcan \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_predixcan_en
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-PrediXcan-MASH \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_predixcan_mashr
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-SMR \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_smr
  done
done


# region-based approach (by LD block)

# common variables
e=0.1
w='region-based'
s=1e6
n=Inf
c=Inf

# by approach
declare -a approachnames=("LD-block")
declare -a inputprefixs=("~/Desktop/rare_variant/new-pipeline-newrun-on-summary/output-ldblock")
# by gene set
declare -a genesets=("EWAS" "OMIM")
declare -a ylims=(0.6 0.4)
declare -a traitlists=("../data/ewas/trait-list-omim-maf_0.01.txt" "../data/omim/trait-list-omim.txt")
declare -a traitgenelists=("../data/ewas/trait-to-gene-ewas-maf_0.01.full-list.txt" "../data/omim/trait-to-gene-omim.full-list.txt")
declare -a inputsuffixs=("-ewas-gene-maf_0.01" "-omim-gene")

# loop over
for a in "${!approachnames[@]}"; do
  for g in "${!genesets[@]}"; do
    Rscript plot_with_iterative_method.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}" \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -e $e \
      -y "${ylims[$g]}" \
      -w $w \
      -s $s \
      -n $n \
      -c $c \
      -u $u
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-PrediXcan \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_predixcan_en
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-PrediXcan-MASH \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_predixcan_mashr
    Rscript test_with_logistic.R \
      -d "${inputprefixs[$a]}""${inputsuffixs[$g]}" \
      -o "${genesets[$g]}"-"${approachnames[$a]}"-SMR \
      -t "${traitgenelists[$g]}" \
      -r "${traitlists[$g]}" \
      -w $w \
      -n $n \
      -c $c \
      -u $u \
      -i score_smr
  done
done
