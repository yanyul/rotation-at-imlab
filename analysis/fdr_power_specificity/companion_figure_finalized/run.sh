# expression
## ewas
Rscript plot.R -d ~/Desktop/rare_variant/new-pipeline-rerun/output-clump-ewas-gene-maf_0.01/ -o ewas-gene-maf_0.01 -t ../data/ewas/trait-to-gene-ewas-maf_0.01.full-list.txt -r ../data/ewas/trait-list-omim-maf_0.01.txt -e 0.1
## omim
Rscript plot.R -d ~/Desktop/rare_variant/new-pipeline-rerun/output-clump-omim-gene/ -o omim -t ../data/omim/trait-to-gene-omim.full-list.txt -r ../data/omim/trait-list-omim.txt -e 0.1
## combined
Rscript plot.R -d ~/Desktop/rare_variant/new-pipeline-rerun/output-clump-combined-gene/ -o combined -t ../data/combined/trait-to-gene-combined.full-list.txt -r ../data/combined/trait-list-combined.txt -e 0.1

# splicing
## ewas
Rscript plot.R -d ~/Desktop/rare_variant/new-pipeline-rerun/output-clump-ewas-splicing-maf_0.01/ -o ewas-splicing-maf_0.01 -t ../data/ewas/trait-to-gene-ewas-maf_0.01.full-list.txt -r ../data/ewas/trait-list-omim-maf_0.01.txt -e 0.1 -x score_smr,score_coloc_yoson
## omim
Rscript plot.R -d ~/Desktop/rare_variant/new-pipeline-rerun/output-clump-omim-splicing/ -o omim-splicing -t ../data/omim/trait-to-gene-omim.full-list.txt -r ../data/omim/trait-list-omim.txt -e 0.1 -x score_smr,score_coloc_yoson
## combined
Rscript plot.R -d ~/Desktop/rare_variant/new-pipeline-rerun/output-clump-combined-splicing/ -o combined-splicing -t ../data/combined/trait-to-gene-combined.full-list.txt -r ../data/combined/trait-list-combined.txt -e 0.1 -x score_smr,score_coloc_yoson
