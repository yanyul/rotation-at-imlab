library(optparse)
option_list = list(
  make_option(c("-d", "--dir"), type="character", default=NULL,
              help="input directory", metavar="character"),
  make_option(c("-o", "--outname"), type="character", default=NULL,
              help="name in output", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

library(dplyr)
library(ggplot2)
theme_set(theme_bw(base_size=20))

outname = 'lipid'
source('../../../scripts/mylib_generic.R')
source('../../../scripts/mylib_doc.R')


## Color guide (subject to change)
cbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
## END



traits = c(
  'GLGC_Mc_HDL',
  'GLGC_Mc_LDL',
  'GLGC_Mc_TG',
  'UKB_20002_1473_self_reported_high_cholesterol'
)

enloc_joint_idx = 1

gwas_gene_trait_pairs = c()
for(trait in traits) {
  gwas = readRDS(paste0(opt$dir, '/gwas_in_2e6_bp-', trait, '.rds'))
  gwas_gene_trait_pairs = c(gwas_gene_trait_pairs, paste(gwas$extracted_genes$gene, '--', trait))
}

enloc_cutoffs = 0.05

score_info = list(name = c('score_enloc_alvaro', 'score_multixcan', 'nearest_gene', 'score_coloc_yoson', 'score_enloc_yoson'), tag = c('enloc', 'multixcan', 'nearest', 'coloc', 'enloc-all'), method = c('gt', 'lt', 'gt', 'gt', 'gt'), cutoff = c(0.5, NA, 0.5, 0.5, 0.5), fill.na = c(0, 1, 0, 0, 0), score_col = c('rcp', 'pvalue', 'nearest_gene', 'PP.H4.abf', 'rcp'))
result = list()

for(i in 1 : length(score_info$name)) {
  # if(i == 1) {
  #   result[[i]] = NA
  #   next
  # }
  temp_collect = data.frame()
  for(trait in traits) {
    temp = paste0(opt$dir, '/gwas_in_2e6_bp-', trait, '.', score_info$name[i], '.rds')
    temp = readRDS(temp)
    temp = temp[, c('gene', score_info$score_col[i])]
    temp[, 2][is.na(temp[, 2])] = score_info$fill.na[i]
    colnames(temp) = c('gene', 'score')
    temp = temp %>% mutate(gene = paste(gene, '--', trait))
    temp_collect = rbind(temp_collect, temp)
  }
  result[[i]] = temp_collect
}
score_info$cutoff[2] = 0.05 / nrow(result[[2]])

map = data.frame(trait = c('LDL', 'HDL', 'TG', 'TC'), trait_tag = c('GLGC_Mc_LDL', 'GLGC_Mc_HDL', 'GLGC_Mc_TG', 'UKB_20002_1473_self_reported_high_cholesterol'))
ewas_tmp = read.csv('~/Desktop/rare_variant/lipid_ng2017_s12.csv') %>% filter(Trait %in% map$trait, p.value < 1e-5, ALT.FREQ < 0.05) %>% left_join(map %>% rename(Trait = trait), by = 'Trait')
temp = ewas_tmp %>% do(parse_annotation_ng2017(.$ANNOTATION))
temp$trait = ewas_tmp$trait_tag
ewas_tmp = temp %>% filter(!annotation %in% c('Intergenic', 'Intron'))
gene_map = read.table('../../../data/annotations_gencode_v26.tsv', header = T) %>% filter(gene_type == 'protein_coding')
ewas_genes = left_join(ewas_tmp, gene_map %>% select(gene_name, gene_id, gene_type), by = 'gene_name') %>% rename(gene = gene_id)
ewas_genes_mapped = ewas_genes %>% filter(!is.na(gene)) %>% mutate(gene = paste(gene, '--', trait))
causal_genes = unique(ewas_genes_mapped$gene)
causal_gene_cleaned = causal_genes[causal_genes %in% gwas_gene_trait_pairs]



# Precision - Recall curve
margin_curve = data.frame()
point_curve = data.frame()
for(i in 1 : length(score_info$tag)) {
  # if(i == 1) {
  #   next
  # }
  tag = score_info$tag[i]
  margin_curve_temp = gen_fdr_power_curve(causal_gene_cleaned, result[[i]]$gene, result[[i]]$score, method = score_info$method[i])
  point_curve_temp = gen_fdr_power_curve(causal_gene_cleaned, result[[i]]$gene, result[[i]]$score, method = score_info$method[i], cutoff = score_info$cutoff[i])
  margin_curve = rbind(margin_curve, margin_curve_temp %>% mutate(method = tag))
  point_curve = rbind(point_curve, point_curve_temp %>% mutate(method = tag))
}

# add joint
joint_curve = gen_fdr_power_curve_joint(causal_gene_cleaned, result[[enloc_joint_idx]]$gene, result[[enloc_joint_idx]]$score, result[[2]]$score, method1 = score_info$method[enloc_joint_idx], method2 = score_info$method[2], by1 = 1)
joint_curve_g = gen_fdr_power_curve_joint(causal_gene_cleaned, result[[enloc_joint_idx]]$gene, result[[enloc_joint_idx]]$score, result[[2]]$score, method1 = score_info$method[enloc_joint_idx], method2 = score_info$method[2], cutoff1 = enloc_cutoffs)

# plot
p = ggplot() + geom_point(data = joint_curve, aes(recall, precision), color = 'gray')
p = p + geom_line(data = joint_curve_g %>% mutate(cutoff1 = paste(score_info$tag[enloc_joint_idx], cutoff1)), aes(recall, precision, color = cutoff1))
p = p + geom_line(data = margin_curve %>% filter(method != 'nearest'), aes(recall, precision, color = method))
p = p + geom_point(data = point_curve, aes(recall, precision, shape = method))
p = p + scale_color_manual(values = cbPalette) + coord_fixed()
ggsave(paste0(opt$outname, '-', outname, '_rare_PR.png'), p)

# TPR - FPR curve
margin_curve2 = data.frame()
point_curve2 = data.frame()
for(i in 1 : length(score_info$tag)) {
  # if(i == 1) {
  #   next
  # }
  tag = score_info$tag[i]
  margin_curve_temp = gen_roc_curve(causal_gene_cleaned, result[[i]]$gene, result[[i]]$score, method = score_info$method[i])
  point_curve_temp = gen_roc_curve(causal_gene_cleaned, result[[i]]$gene, result[[i]]$score, method = score_info$method[i], cutoff = score_info$cutoff[i])[1, ]
  margin_curve2 = rbind(margin_curve2, margin_curve_temp %>% mutate(method = tag))
  point_curve2 = rbind(point_curve2, point_curve_temp %>% mutate(method = tag))
}

# add joint
joint_curve2 = gen_roc_curve_joint(causal_gene_cleaned, result[[enloc_joint_idx]]$gene, result[[enloc_joint_idx]]$score, result[[2]]$score, method1 = score_info$method[enloc_joint_idx], method2 = score_info$method[2], by1 = 1)
joint_curve_g2 = gen_roc_curve_joint(causal_gene_cleaned, result[[enloc_joint_idx]]$gene, result[[enloc_joint_idx]]$score, result[[2]]$score, method1 = score_info$method[enloc_joint_idx], method2 = score_info$method[2], cutoff1 = enloc_cutoffs)

# plot
p = ggplot() + geom_point(data = joint_curve2, aes(fpr, tpr), color = 'gray')
p = p + geom_line(data = joint_curve_g2 %>% mutate(cutoff1 = paste(score_info$tag[enloc_joint_idx], cutoff1)), aes(fpr, tpr, color = cutoff1))
p = p + geom_line(data = margin_curve2 %>% filter(method != 'nearest'), aes(fpr, tpr, color = method))
p = p + geom_point(data = point_curve2, aes(fpr, tpr, shape = method))
p = p + scale_color_manual(values = cbPalette) + coord_fixed()
ggsave(paste0(opt$outname, '-', outname, '_rare_ROC.png'), p)
