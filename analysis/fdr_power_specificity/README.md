# Idea

The task in this module is to calculate FDR, power, and specificity, etc for various prioritization method: MultiXcan, ENLOC, COLOC, etc, along with nearest gene strategy. To do so, we use EWAS/rare variant association as ground truth.

# Workflow

The **input** data are

* GWAS loci (tell where to look at)
* Gene annotation (tell which genes to look at)
* Prioritization score (plus a generic cutoff people may use and a sequence of generic cutoff)
* ground truth gene list

The **workflow** is

1. Given the GWAS loci, extract all genes nearby. Here we use distance to gene body
2. Annotate the extracted genes with:

    * Prioritization score (add an extra filter to make sure that the signal obtained is driven by the GWAS loci so that we require that the gene's TSS should be close to GWAS leading loci)
    * Whether it is the nearest gene (relative to GWAS leading loci)

3. Annotate the extracted genes with whether it is EWAS/rare variant gene.

The **output** are

1. GWAS leading variant
2. A `data.frame` of extracted genes along with EWAS/rare variant annotation
3. For each score, a `data.frame` of annotation
4. A `data.frame` for nearest gene annotation
