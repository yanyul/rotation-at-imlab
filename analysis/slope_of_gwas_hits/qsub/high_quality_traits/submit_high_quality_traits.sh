e=$( cat /home/t.cri.yliang/scratch/repo/rotation-at-imlab/data/trait_lists/high_quality_gwas_from_alvaro.txt | sed 's/imputed_//' | sed 's/.txt.gz//' )
for i in $e
  do
    qsub -v TRAIT=$i,NAME="high_quality_traits" -o $i.out -N high_quality_traits_$i -e $i.err ../run.qsub
  done
