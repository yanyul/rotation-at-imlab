library(optparse)

option_list = list(
  make_option(c("-e", "--eqtl"), type="character", default=NULL,
              help="name of the input eQTL file", metavar="character"),
  make_option(c("-p", "--ld_prefix"), type="character", default=NA,
              help="prefix of input ld file", metavar="character"),
  make_option(c("-s", "--ld_suffix"), type="character", default=NA,
              help="suffix of input ld file", metavar="character"),
  make_option(c("-o", "--out"), type="character", default=NA,
              help="output histogram before/after pruning", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

source('../../scripts/mylib.R')
source('../../scripts/mylib_playground.R')
library(ggplot2)
library(data.table)
options(datatable.fread.datatable = FALSE)

get_beta_tilde <- function(df) {
  # obtain MAF
  # message('MAF')
  maf.pmin <- do.call(pmin, data.frame(df[, 'V11'], 1 - df[, 'V11']))
  df <- df[!is.na(maf.pmin), ]
  df$maf <- maf.pmin[!is.na(maf.pmin)]
  df <- df[df$maf != 0 & df$maf * df[, 'V12'] >= 50, ]  # remove rare variants (low power variants)

  # Remove NA zscore
  df <- df[!is.na(df[, 'V13']), ]

  # impute beta_tilde
  # message('Beta')
  df$beta_tilde <- df[, 'V13'] / sqrt(2 * df[, 'V12'] * df$maf * (1 - df$maf))
  return(df)
}

df <- fread(paste0('zcat < ', opt$eqtl), header = F)
df <- df[df$V1 != 'chr6', ]
df <- get_beta_tilde(df)
ind.snp <- ld_pruning(df$V1, df$V2, df$V14)
df.cleaned <- df[ind.snp, ]

df.ld <- data.frame()
for(i in 1 : 22) {
  temp <- fread(paste0('zcat ', opt$ld_prefix, i, opt$ld_suffix))
  df.ld <- rbind(df.ld, temp)
}

df.ld <- df.ld[df.ld$SNP_A != df.ld$SNP_B, ]
df.ld.before <- df.ld[df.ld$SNP_A %in% df$V4 & df.ld$SNP_B %in% df$V4, ]
df.ld.before$label <- 'before'
df.ld.after <- df.ld[df.ld$SNP_A %in% df.cleaned$V4 & df.ld$SNP_B %in% df.cleaned$V4, ]
df.ld.after$label <- 'after'

df.plot <- data.frame(R2 = c(df.ld.before$R2, df.ld.after$R2), label = c(df.ld.before$label, df.ld.after$label))

p <- ggplot(df.plot) + geom_histogram(aes(x = R2)) + facet_wrap(~label, scales = 'free_y', ncol = 1) + ggtitle('R2 before/after ld_pruning')
ggsave(opt$out, p)
