e=$( ls /group/gtex-group/v8/59349/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/eqtl/GTEx_Analysis_v8_eQTL/ | grep .signif_variant_gene_pairs.txt.gz | sed 's#.v8.signif_variant_gene_pairs.txt.gz##g' )
declare -a eqtl=('DAPG_pip_gt_0.01')

for t in "${eqtl[@]}"
  do
  for TISSUE in $e
    do
      qsub -v TISSUE=$TISSUE,EQTL=$t -o logs/$TISSUE\_$t.out -N ls2_ldsc_$TISSUE\_$t -e logs/$TISSUE\_$t.err ls2_ldsc_high_quality_traits.qsub 
    done
  done
