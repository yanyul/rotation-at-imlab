# this was copied from gtex-gwas-yanyu
# not runnable
# just to keep a record on how it was generated

awk -v OFS="\t" 'FNR==NR{a[$1]=1;next}{if($3 in a) print $3,$2}' ../../data/DAPGvariant-active_in_GTEx_v8_PCA_EUR.prune_varlist.txt <(cat ../../light_data/DAPG_pip_gt_0.01-AllTissues.txt | tail -n +2) | sort |uniq > DAPG_eQTL_after_pruning.txt 
