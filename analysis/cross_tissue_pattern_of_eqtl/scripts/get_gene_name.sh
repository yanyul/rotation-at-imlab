header=`cat ../../shared_data/cross_tissue_pattern_header.txt | head -n 1| tr '\t' ','`
header="geneID,variantID,$header"
echo $header | tr ',' '\t' > $2.temp

zcat $1 | tail -n +2 | awk -F"\t" -v OFS="\t" '{split($1,a,"_"); gsub(a[1]"_", "", $1); print a[1],$0}' | sort -k 1,1 -k 2,2 >> $2.temp

cat $2.temp | gzip > $2
rm $2.temp
