library(optparse)

option_list = list(
  make_option(c("-p", "--posterior"), type="character", default=NULL,
              help="if use posterior, set to True, else, set to False", metavar="character"),
  make_option(c("-f", "--flash"), type="character", default=NULL,
              help="flashr model in RDS format", metavar="character"),
  make_option(c("-e", "--eqtl"), type="character", default=NULL,
              help="eQTL matrix in RDS format", metavar="character"),
  make_option(c("-l", "--eqtl_list"), type="character", default=NULL,
              help="list of eQTL ID to be included", metavar="character"),
  make_option(c("-o", "--output"), type="character", default="test",
              help="name of output RDS file", metavar="character")
);

args_parser = OptionParser(option_list=option_list);
opt = parse_args(args_parser);

library(stringr)
source('../../scripts/mylib.R')

flash <- readRDS(opt$flash)
if(opt$posterior == 'False') {
  eqtl <- readRDS(opt$eqtl)$Bhat
} else if(opt$posterior == 'True') {
  eqtl <- readRDS(opt$eqtl)$PosteriorMean
}
eqtl_list <- read.table(opt$eqtl_list, header = F, sep = '\t')

row_name <- row.names(eqtl)
chr <- paste0('chr', str_match(row_name, '[A-Z0-9]+_([A-Za-z0-9]+)_[A-Z0-9_]+')[, 2])
pos_alleles <- str_match(row_name, '[A-Z0-9]+_[A-Za-z0-9_]+_([0-9]+_[A-Z]+_[A-Z]+$)')[,2]
var_name <- paste0(chr, '_', pos_alleles, '_b38')
# var_name <- var_name[!is.na(chr) & !is.na(pos_alleles)]

eqtl <- eqtl[var_name %in% eqtl_list$V2, ]
factor_mat <- flash$model$ldf$f

df <- project_eqtl_to_flash_factor(eqtl, factor_mat)
df <- df$best_membership
saveRDS(df, opt$output)
