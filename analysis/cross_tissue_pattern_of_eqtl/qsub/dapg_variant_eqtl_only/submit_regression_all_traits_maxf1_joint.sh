name=( $( cat ../../../../shared_data/GTEx_V8_GWAS_Share-Imputation.tsv | tail -n +2 | cut -f2 | sed 's/imputed_//' | sed 's/.txt.gz//' ) )

for (( j=0; j<${#name[@]}; j++))
  do
    qsub -v GWAS=${name[$j]} -o logs/${name[$j]}.out -N ${name[$j]} -e logs/${name[$j]}.err regression_maxf1_joint_with_coreg.qsub
  done
