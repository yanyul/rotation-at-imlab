echo 'ANNOT' > $4.tmp
zcat $2 | grep $3 > $4.sub
awk 'NR==FNR{a[$4]=$5;next}; {if($2 in a){print a[$2]}else{print "0"}}' $4.sub $1 >> $4.tmp
cat $4.tmp | gzip > $4
rm $4.tmp
rm $4.sub
