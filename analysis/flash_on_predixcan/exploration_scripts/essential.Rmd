---
title: "Essential genes vs. regulable genes"
output:
  html_document:
    toc: true
    toc_float: true
    theme: cosmo
    highlight: textmate
    number_sections: true
---


```{r setup}
source('../../scripts/mylib.R')
library(dplyr)
library(reshape2)
library(ggplot2)
library(data.table)
library(stringr)
library(pander)
options(datatable.fread.datatable = F)
myquery = function(dataset, table) {
  # extract variant by PIP cutoff
  tbl <- tableInfo(dataset, table)
  full_name = paste0(unlist(tbl), collapse = '.')
  sql = paste0('SELECT * FROM `', full_name, '` ')
  df <- getOutputBySQL(tbl, sql)
  df
}
```

# Data

## Get essential measure

```{r}
if(!file.exists('essential.rds')) {
  df_ess <- myquery(dataset = 'annotations', table = 'human_gene_essentiality_scores')
  saveRDS(df_ess, 'essential.rds')
} else {
  df_ess = readRDS('essential.rds')
}

```

## Gene expression heritability

```{r}
if(!file.exists('gene_h2.rds')) {
  df_h2 <- myquery(dataset = 'genarch', table = 'gcta_h2_gtex_tissue_specific')
  saveRDS(df_h2, 'gene_h2.rds')
} else {
  df_h2 = readRDS('gene_h2.rds')
}
``` 

## Hilary's gene prioritization from GTEx v6p

```{r}
if(!file.exists('seg.rds')) {
  df_seg = read.table('~/Downloads/GTEx.tstat.tsv', header = T)
  saveRDS(df_seg, 'seg.rds')
} else {
  df_seg = readRDS('seg.rds')
}
if(!file.exists('seg_franke.rds')) {
  df_segf = fread('zcat < ~/Downloads/GPL570EnsemblGeneExpressionPerTissue_DEPICT20130820_z.txt.gz', header = T)
  map = read.csv('~/Downloads/seg_franke_map.csv', skip = 1)
  saveRDS(list(df = df_segf, map = map), 'seg_franke.rds')
} else {
  df_segf = readRDS('seg_franke.rds')$df
  map = readRDS('seg_franke.rds')$map
}
``` 

# Analysis

```{r}
df = inner_join(df_ess %>% select(string_field_0, pLi, RVIS) %>% rename(gene = string_field_0), df_h2 %>% select(h2_CrossTissue, se_CrossTissue, EnsemblGeneID, h2_MuscleSkeletal, se_MuscleSkeletal) %>% rename(gene = EnsemblGeneID) %>% mutate(gene = trim_dot(gene)), by = 'gene')
df = inner_join(df, df_seg %>% select(ENSGID, Muscle_Skeletal) %>% rename(gene = ENSGID, seg_Muscle_Skeletal = Muscle_Skeletal), by = 'gene')
df_segf_sub = df_segf[, colnames(df_segf) %in% map$Tissue[map$Tissue.category.for.display == 'Musculoskeletal/Connective']]
df_segf_sub$gene = df_segf$`-`
df = inner_join(df, df_segf_sub, by = 'gene')
```

## Specific express gene (SEG in Hiliary et al)

```{r, results='asis'}
df_odds = data.frame()
for(i in c('seg_Muscle_Skeletal', as.character(unique(map$Tissue[map$Tissue.category.for.display == 'Musculoskeletal/Connective'])))) {
  df$now = df[[i]]
  p = df %>% mutate(h2_zval = h2_MuscleSkeletal / se_MuscleSkeletal) %>% mutate(is_heritable = h2_zval > qnorm(0.95)) %>% ggplot() + geom_density(aes(x = now)) + facet_wrap(~is_heritable, ncol = 1)
  p = p + ggtitle(i)
  print(p)
}

```

## pLI

```{r}

df %>% mutate(h2_zval = h2_CrossTissue / se_CrossTissue) %>% mutate(is_heritable = h2_zval > qnorm(0.95)) %>% ggplot() + geom_density(aes(x = pLi)) + facet_wrap(~is_heritable, ncol = 1)
```

## RVIS

```{r}

df %>% mutate(h2_zval = h2_CrossTissue / se_CrossTissue) %>% mutate(is_heritable = h2_zval > qnorm(0.95)) %>% ggplot() + geom_density(aes(x = RVIS)) + facet_wrap(~is_heritable, ncol = 1)
```


