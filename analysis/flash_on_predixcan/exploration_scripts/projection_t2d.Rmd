---
title: "Projecting T2D"
output:
  html_document:
    toc: true
    toc_float: true
    theme: cosmo
    highlight: textmate
    number_sections: true
---


```{r setup}
library(flashr)
library(mashr)
source('../../scripts/mylib.R')
library(dplyr)
library(reshape2)
library(ggplot2)
library(data.table)
library(stringr)
options(datatable.fread.datatable = F)
```

Run on UKB_20002_1223_self_reported_type_2_diabetes as example.

# Data

## Load data from BigQuery

```{r}
if(!file.exists('ukb_t2d.rds')) {
  myquery = function(trait, dataset = 'GTEx_V8_ElasticNet_EUR_2018_07_05', table = 'predixcan_results') {
    # extract variant by PIP cutoff
    tbl <- tableInfo(dataset, table)
    full_name = paste0(unlist(tbl), collapse = '.')
    sql = paste0('SELECT * FROM `', full_name, '` WHERE ', 'phenotype = "', trait, '"')
    df <- getOutputBySQL(tbl, sql)
    df
  }
  df <- myquery('UKB_20002_1223_self_reported_type_2_diabetes')
  saveRDS(df, 'ukb_t2d.rds')
} else {
  df = readRDS('ukb_t2d.rds')
}

```

## Load sample size

```{r}
sample_size = read.csv('../../data/gtex_sample_counts_by_tissue.csv') %>% filter(v8_eur > 0)
sample_size$tissue[sample_size$tissue == 'Cells_Transformed_fibroblasts'] = 'Cells_Cultured_fibroblasts'
```

## Data.frame to matrix

```{r}
mat = dcast(data = df %>% select(tissue, zscore, gene), gene ~ tissue, value.var = 'zscore')
mat[is.na(mat)] = NaN
```

## Read in enloc

```{r}
if(!file.exists('ukb_t2d_enloc.rds')) {
  tissues = unique(df$tissue)
  df_enloc = data.frame()
  for(i in tissues) {
    filename = paste0('~/Desktop/flash/all_indiv/', i, '_w_Type_2_Diabetes_UKBS_enloc_output.txt.gz')
    if(!file.exists(filename)) {
      next
    }
    temp = fread(paste0('zcat < ',filename), header = T)
    temp$tissue = i
    df_enloc = rbind(df_enloc, temp)
  }
  df_enloc$molecular_qtl_trait = df_enloc$gene_id
  df_enloc$locus_rcp = df_enloc$rcp
  saveRDS(df_enloc, 'ukb_t2d_enloc.rds')
} else {
  df_enloc = readRDS('ukb_t2d_enloc.rds')
}
```


## Load FLASH run on eQTL matrix

```{r}
mod = readRDS('../../data/FastQTLSumStats.mash.flash.model.rds')
factors = mod$model$ldf$f[, mod$model$pve > 0]
r = data.frame(factors); maxr = apply(r, 2, max); r = sweep(r, 2, maxr, FUN = '/'); r = r %>% mutate(tissue = rownames(r))
r$tissue = factor(r$tissue, levels = sample_size$tissue[order(sample_size$v8_eur)])
r %>% melt(id.vars = 'tissue') %>% ggplot() + geom_raster(aes(x = tissue, y = variable, fill = value)) + theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = .5))
```


# Analysis

For each gene, project each PrediXcan z-score vector onto FLASH factors learned from eQTL effect size matrix.

```{r}
source('../../scripts/mylib.R')
negative_genes = df_enloc %>% group_by(molecular_qtl_trait) %>% summarize(rcp = max(locus_rcp)) %>% filter(rcp == 0)
mat_rcp_gt_0.5 = mat[mat$gene %in% df_enloc$molecular_qtl_trait[df_enloc$locus_rcp > 0.1], ]; mat_rcp_gt_0.5[is.na(mat_rcp_gt_0.5)] = 0
mat_rcp_lt_0.01 = mat[mat$gene %in% negative_genes$molecular_qtl_trait, ]; mat_rcp_lt_0.01[is.na(mat_rcp_lt_0.01)] = 0

proj_rcp_gt_0.5 = project_eqtl_to_flash_factor(as.matrix(mat_rcp_gt_0.5[, -1]), factors)
proj_rcp_gt_0.01 = project_eqtl_to_flash_factor(as.matrix(mat_rcp_lt_0.01[, -1]), factors)

proj_mem1 = proj_rcp_gt_0.5$best_membership %>% filter(membership != 1, fraction_of_explain > 0.2, membership != 7) %>% group_by(membership) %>% summarize(count = n()) %>% mutate(frac = count / sum(count), type = 'pos')
proj_mem2 = proj_rcp_gt_0.01$best_membership %>% filter(membership != 1, fraction_of_explain > 0.2, membership != 7) %>% group_by(membership) %>% summarize(count = n()) %>% mutate(frac = count / sum(count),type = 'neg')
proj_mem = rbind(proj_mem1, proj_mem2)
proj_mem %>% ggplot() + geom_bar(aes(x = factor(membership), y = frac, fill = type), stat = 'identity', position = 'dodge') + geom_text(aes(x = factor(membership), y = frac, label = count, group = type), position = position_dodge(1))
proj_mem_sum = proj_mem %>% group_by(type) %>% summarise(sum(count))
proj_mem %>% group_by(membership) %>% do(perform_fisher_test(.$count[.$type == 'pos'], .$count[.$type == 'neg'], proj_mem_sum$`sum(count)`[proj_mem_sum$type == 'pos'], proj_mem_sum$`sum(count)`[proj_mem_sum$type == 'neg'])) %>% ggplot() + geom_point(aes(x = factor(membership), y = odds_ratio, size = -log10(pval), color = pval < 0.05))
```

