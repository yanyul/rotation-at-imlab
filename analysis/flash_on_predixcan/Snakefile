# run by trait
rule all:
    input:
        'output/{name}--{trait}__enrichment.enloc.rds'.format(trait = config['trait'], name = config['name']),
        'output/{name}--{trait}__enrichment.predixcan.rds'.format(trait = config['trait'], name = config['name']),
        'output/{name}--{trait}__enrichment.combined.rds'.format(trait = config['trait'], name = config['name']),
        'output/{name}--{trait}__enrichment.factor1.rds'.format(trait = config['trait'], name = config['name'])

rule extract_enloc:
    input:
        tissue_list = config['tissue']
    params:
        enloc_prefix = config['enloc']['prefix'],
        enloc_suffix = config['enloc']['suffix']
    output:
        'output/{name}--{trait}__data.enloc.rds'
    shell:
        'Rscript scripts/extract_enloc.R --tissue {input.tissue_list} --enloc_prefix {params.enloc_prefix} --enloc_suffix {params.enloc_suffix} --output {output[0]}'

rule extract_predixcan:
    input:
        tissue_list = config['tissue']
    params:
        predixcan_prefix = config['predixcan']['prefix'],
        predixcan_suffix = config['predixcan']['suffix']
    output:
        'output/{name}--{trait}__data.predixcan.rds'
    shell:
        'Rscript scripts/extract_predixcan.R --tissue {input.tissue_list} --predixcan_prefix {params.predixcan_prefix} --predixcan_suffix {params.predixcan_suffix} --output {output[0]}'

rule run:
    input:
        tissue_list = config['tissue'],
        flash = config['flash'],
        enloc = 'output/{name}--{trait}__data.enloc.rds',
        predixcan = 'output/{name}--{trait}__data.predixcan.rds'
    output:
        'output/{name}--{trait}__enrichment.enloc.rds',
        'output/{name}--{trait}__enrichment.predixcan.rds'
    shell:
        'Rscript scripts/run_enrich.R --tissue {input.tissue_list} --flash {input.flash} --enloc {input.enloc} --predixcan {input.predixcan} --output_prefix output/{wildcards.name}--{wildcards.trait}'

rule run_combined:
    input:
        tissue_list = config['tissue'],
        flash = config['flash'],
        enloc = 'output/{name}--{trait}__data.enloc.rds',
        predixcan = 'output/{name}--{trait}__data.predixcan.rds'
    output:
        'output/{name}--{trait}__enrichment.combined.rds'
    shell:
        'Rscript scripts/run_enrich_combined.R --tissue {input.tissue_list} --flash {input.flash} --enloc {input.enloc} --predixcan {input.predixcan} --output_prefix output/{wildcards.name}--{wildcards.trait}'

rule run_factor1:
    input:
        tissue_list = config['tissue'],
        flash = config['flash'],
        enloc = 'output/{name}--{trait}__data.enloc.rds',
        predixcan = 'output/{name}--{trait}__data.predixcan.rds'
    output:
        'output/{name}--{trait}__enrichment.factor1.rds'
    shell:
        'Rscript scripts/run_enrich_factor1.R --tissue {input.tissue_list} --flash {input.flash} --enloc {input.enloc} --predixcan {input.predixcan} --output_prefix output/{wildcards.name}--{wildcards.trait}'
