This module perform matrix factorization on gene-by-tissue matrix for each trait.

The pipeline extract enloc and PrediXcan runs for each trait-tissue pair and perform projection analysis (onto FLASH factor learnt from eQTL).
