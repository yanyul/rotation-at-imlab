For submission

Specify the configfile and `run.qsub` will loop over all traits in the trait list.

For instance

* Using PrediXcan-dapgw 

```
$ qsub -v TRAIT=not_deflated_trait_list.txt,CONFIG=eqtl-dapgw -N eqtl-dapgw-ct run.qsub
```
