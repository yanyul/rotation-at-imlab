# This module preprocesses mashr output into format that can be used for pleiotropy_effect_size/ module

rule all:
    input:
        'output/{task_name}__{tissue}.gz'.format(task_name = config['task_name'], tissue = config['tissue'])

rule all_varlist:
    input:
        'output/{task_name}.variant_list.txt'.format(task_name = config['task_name'])

rule rds2txt:
    input:
        lambda wildcards: config['path'].format(batch_id = wildcards.batch_id)
    output:
        'output/{task_name}__{batch_id}__{tissue}.txt'
    shell:
        'Rscript scripts/rds2txt_bytissue.R --input {input[0]} --tissue {wildcards.tissue} --output {output[0]}'

rule aggregate_batches:
    input:
        [ 'output/{{task_name}}__{batch_id}__{{tissue}}.txt'.format(batch_id = i) for i in range(1, config['nbatch'] + 1) ]
    output:
        'output/{task_name}__{tissue}.gz'
    shell:
        'bash scripts/aggregate_files.sh {output[0]} {input}'

rule get_variant_list:
    input:
        lambda wildcards: config['path'].format(batch_id = wildcards.batch_id)
    output:
        'output/{task_name}__{batch_id}.variant_list-batch.txt'
    shell:
        'Rscript scripts/get_variant_list.R --input {input[0]} --output {output[0]}'

rule aggregate_varlists:
    input:
        [ 'output/{{task_name}}__{batch_id}.variant_list-batch.txt'.format(batch_id = i) for i in range(1, config['nbatch'] + 1) ]
    output:
        'output/{task_name}.variant_list.txt'
    shell:
        'bash scripts/aggregate_variants.sh {output[0]} {input}'
