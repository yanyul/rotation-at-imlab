input=$1
output=$2
chr_num=$3
start=$4
start=$((start-1))
end=$5

zcat $input | head -n 1 > $output.temp
# echo "chr$chr_num,$start,$end" | tr ',' '\t'
bedtools intersect -a <(echo "chr$chr_num,$start,$end" | tr ',' '\t') -b <(zcat $input | tail -n +2) -wb | cut -f 4- >> $output.temp
cat $output.temp | gzip > $output
rm $output.temp
