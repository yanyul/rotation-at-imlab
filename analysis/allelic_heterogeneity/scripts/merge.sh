# bash merge.sh {input.list} {params.prefix} {params.output} {params.suffix}

mylist=$1
myprefix=$2
mysuffix=$4
opip=$3.$mysuffix
ocs=$3.cs.$mysuffix

# blockone=`cat $mylist | tail -n +2 | cut -f 1 | head -n 1`
# zcat $myprefix$blockone.$mysuffix | head -n 1 > $opip.temp
# zcat $myprefix$blockone.cs.$mysuffix | head -n 1 > $ocs.temp

echo 'cs,cs_log10bf,cs_avg_r2,cs_min_r2,chr,pos_min,pos_max,type' | tr ',' '\t' > $ocs.temp
echo 'variable,variable_prob,cs,variant,type' | tr ',' '\t' > $opip.temp

for i in `cat $mylist | tail -n +2 | cut -f 1`
do
  pipfile=$myprefix$i.$mysuffix
  csfile=$myprefix$i.cs.$mysuffix
  zcat $pipfile | tail -n +2 >> $opip.temp
  zcat $csfile | tail -n +2 >> $ocs.temp
done

cat $opip.temp | gzip > $opip
cat $ocs.temp | gzip > $ocs

rm $opip.temp
rm $ocs.temp
