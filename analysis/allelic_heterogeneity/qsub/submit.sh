trait=$1
list=$2
n=$3
outdir=$4

if [ ! -d "configs" ]; then
  mkdir configs/
fi
if [ ! -d "logs" ]; then
  mkdir logs/
fi

# FIX BLOCK SPLIT!!!
# IN CASE YOU WANT TO CHANGE
# UNCOMMENT AND EDIT ACCORDINGLY
# if [ ! -d "block_list" ]; then
#   mkdir block_list/
# fi

# rm block_list/* 
# cat $list | tail -n +2 > block_list/all.txt
# cd block_list/
# split -l $n -a 3 -d all.txt
# cd ../

echo "genotype:
  path: '/home/t.cri.yliang/labshare/mv_from_scratch/repo_new/rotation-at-imlab/analysis/compute_ldsc/vcf2plink/GTEx_v8-fixed__PCA_EUR'
  name: 'GTEx_v8-fixed__PCA_EUR'
gwas:
  path: '/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_$trait.txt.gz'
  name: '$trait'
ld_block: 'qsub/block_list/x000'
outdir: '$outdir'
" > configs/config.$trait\_\_1.yaml
cd ../
snakemake --configfile qsub/configs/config.$trait\_\_1.yaml $outdir/$trait.bed-temp.gz -p > qsub/logs/$trait.prepare.log 2>&1
cd qsub/
COUNTER=1
for i in `ls block_list/x*`;
do
  qsub -v BLOCKS=qsub/$i,TRAIT=$trait,JOBID=$COUNTER,OUTDIR=$outdir -e logs/$trait\_\_$COUNTER.err -o logs/$trait\_\_$COUNTER.out -N $trait\_\_$COUNTER qsub_generic.qsub
  COUNTER=$((COUNTER + 1))
done

echo After qsub jobs finish, run:
echo cd ../
echo snakemake --configfile qsub/configs/config.$trait\_\_1.yaml all_merge --config ld_block=qsub/block_list/all.txt 

