inputprefix=$1
out=$2
echo 'CHR,F,SNP,BP,P,TOTAL,NSIG,S05,S01,S001,S0001,SP2' | tr ',' '\t' > $out 
# cat $inputprefix"1".clumped > $out
for i in `seq 1 22`
do
  cat $inputprefix$i.clumped |awk -F" " -v OFS="\t" '{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12}' | tail -n +2 >> $out
  # cat $inputprefix$i.clumped | tail -n +2 >> $out
done
