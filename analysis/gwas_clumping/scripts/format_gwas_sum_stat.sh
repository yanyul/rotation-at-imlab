gwas=$1
out=$2

echo 'SNP,P' | tr ',' '\t' > $out
zcat $gwas | cut -f 2,11 | tail -n +2 >> $out
