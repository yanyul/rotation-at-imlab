This module contains some quick scripts to extract GWAS loci from summary statistic using `plink --clump`

Steps:

1. Format summary statistics to clump input
2. Perform clumping
