config=$1

traits=($(ls /gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_* | sed 's#/gpfs/data/im-lab/nas40t2/Data/SummaryResults/imputed_gwas_hg38_1.1/imputed_##g'|sed 's#.txt.gz##g'))

for trait in "${traits[@]}"
do
  qsub -v TRAIT=$trait,CONFIG=$config -N clump-$trait qsub_generic.qsub
done
