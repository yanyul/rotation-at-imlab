for i in {1..22} 
  do
    zcat ../1000G_EUR_Phase3_hg38/baselineLD_v1.1/baselineLD.$i\.l2.ldscore.gz | awk -F"\t" -v OFS="\t" '{print $1,$2,$3,$4}' | gzip > baseline_only.$i\.l2.ldscore.gz
    cat ../1000G_EUR_Phase3_hg38/baselineLD_v1.1/baselineLD.$i\.l2.M | cut -f 1 > baseline_only.$i\.l2.M
    cat ../1000G_EUR_Phase3_hg38/baselineLD_v1.1/baselineLD.$i\.l2.M_5_50 | cut -f 1 > baseline_only.$i\.l2.M_5_50
  done
