my_liftover () {
  # FILENAME=Allscores
  e2=`head -n 1 ../raw_data/$1.txt | tr '\t' ','`
  echo chr_hg38,start0_hg38,start1_hg38,$e2 | tr ',' '\t' > $1\_hg38.bed
  # head -n 1 ../raw_data/$1.txt > $1\_hg38.bed
  liftOver -bedPlus=3 <(cat ../raw_data/$1.txt |tail -n +2 |awk -F"\t" -v OFS="\t" '{print "chr"$2,$3-1,$3,$0}') ../../hg19ToHg38.over.chain $1\_hg38.bed.tmp $1\_unmap.bed
  cat $1\_hg38.bed.tmp >> $1\_hg38.bed
  rm $1\_hg38.bed.tmp
}

my_liftover AllScores 
my_liftover AllScoresLDCorrected 
