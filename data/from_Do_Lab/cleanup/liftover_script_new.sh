my_liftover () {
  # FILENAME=Allscores
  e2=`zcat $1.txt.gz | head -n 1 | tr '\t' ','`
  echo chr_hg38,start0_hg38,start1_hg38,$e2 | tr ',' '\t' > $1\_hg38.bed
  # head -n 1 ../raw_data/$1.txt > $1\_hg38.bed
  liftOver -bedPlus=3 <(zcat $1.txt.gz |tail -n +2 |awk -F"\t" -v OFS="\t" '{print "chr"$3,$4-1,$4,$0}') ../../hg19ToHg38.over.chain $1\_hg38.bed.tmp $1\_unmap.bed
  cat $1\_hg38.bed.tmp >> $1\_hg38.bed
  rm $1\_hg38.bed.tmp
}

# clean up variants with one RSID corresponding to more than one variants (different ALT allele)
## extract the list of RSID with multiple variants
zcat ../raw_data/ScoresUKbbSAIGE_AllSNPs_LDTheo.txt.tar.gz |tail -n +2 | cut -f2 | sort | uniq -c |awk '{if($1!=1) print $2}' > ScoresUKbbSAIGE_AllSNPs_LDTheo.dupRSID.txt
zcat ../raw_data/ScoresUKbbSAIGE_AllSNPs_LDTheo.txt.tar.gz | head -n 1 > ScoresUKbbSAIGE_AllSNPs_LDTheo_nodupRSID.txt
awk 'FNR==NR{a[$1]=$1 ;next}{ if(!($2 in a)) print $0}' ScoresUKbbSAIGE_AllSNPs_LDTheo.dupRSID.txt <(zcat ../raw_data/ScoresUKbbSAIGE_AllSNPs_LDTheo.txt.tar.gz | tail -n +2) >> ScoresUKbbSAIGE_AllSNPs_LDTheo_nodupRSID.txt

gzip ScoresUKbbSAIGE_AllSNPs_LDTheo_nodupRSID.txt

my_liftover ScoresUKbbSAIGE_AllSNPs_LDTheo_nodupRSID

