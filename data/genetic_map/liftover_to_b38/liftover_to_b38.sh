for i in $(seq 1 22)
  do
    bash ../../../analysis/compute_ldsc/lift_over_genetic_map.sh ../from_1000GP_Phase3/genetic_map_chr$i\_combined_b37.txt chr$i ../../hg19ToHg38.over.chain genetic_map_chr$i\_b38.txt
  done
# i=X
# echo bash ../../analysis/compute_ldsc/lift_over_genetic_map.sh ../from_1000GP_Phase3/genetic_map_chr$i\_combined_b37.txt chr$i ../../hg19ToHg38.over.chain genetic_map_$i\_b38.txt
