python ../../analysis/compute_ldsc/add_gtex_variant_id_to_hapmap_snplist.py --snplist w_hm3.snplist --lookup_table /gpfs/data/gtex-group/v8/61897/gtex/exchange/GTEx_phs000424/exchange/analysis_releases/GTEx_Analysis_2017-06-05_v8/genotypes/WGS/variant_calls/GTEx_Analysis_2017-06-05_v8_WholeGenomeSeq_838Indiv_Analysis_Freeze.lookup_table.txt.gz --gtex_col 3 --snp_col 7 --snp_col_snplist 1 --output tmp
echo SNP,A1,A2 | tr ',' '\t' > w_hm3_kept_rsID.snplist
cat tmp | awk -F"\t" -v OFS="\t" '{print $1,$2,$3}' >> w_hm3_kept_rsID.snplist
echo SNP,A1,A2 | tr ',' '\t' > w_hm3_kept_gtexID.snplist
cat tmp | awk -F"\t" -v OFS="\t" '{print $4,$2,$3}' >> w_hm3_kept_gtexID.snplist
rm tmp
