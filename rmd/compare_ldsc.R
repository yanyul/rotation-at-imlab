library(data.table)
library(dplyr)
library(ggplot2)
options(datatable.fread.datatable = FALSE)

ldsc.1kg.prefix <- '../data/1kg_ldsc/1000G_EUR_Phase3_hg38/baselineLD_v1.1/baselineLD.'
ldsc.gtex.prefix <- '../analysis/compute_ldsc/plink2ldsc/GTEx_v8/chr'
suffix <- '.l2.ldscore.gz'

chroms <- 21 : 22
df <- data.frame()
for(c in chroms) {
  ldsc.1kg.df <- fread(paste0('zcat', ' < ', ldsc.1kg.prefix, c, suffix))
  ldsc.1kg.df <- ldsc.1kg.df[, 1 : 4]
  ldsc.gtex.df <- fread(paste0('zcat', ' < ', ldsc.gtex.prefix, c, suffix))
  df.1kg.o <- ldsc.1kg.df[order(ldsc.1kg.df$BP, decreasing = F), ]
  df.gtex.o <- ldsc.gtex.df[order(ldsc.gtex.df$BP, decreasing = F), ]
  l2.gtex <- df.gtex.o[df.gtex.o$BP %in% df.1kg.o$BP, ]
  l2.1kg <- df.1kg.o[df.1kg.o$BP %in% df.gtex.o$BP, ]
  df <- rbind(df, data.frame(chr = c, pos = l2.gtex$BP, ldsc_gtex = l2.gtex$L2, ldsc_1kg = l2.1kg$baseL2))
}
p <- ggplot(df) + geom_point(aes(x = ldsc_1kg, y = ldsc_gtex)) + facet_wrap(~chr) + geom_abline(slope = 1, intercept = 0, linetype = 2, color = 'red')
