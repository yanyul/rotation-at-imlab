---
title: "Calculating horizontal pleiotropic score for a candidate region in whole blood"
# author: Yanyu Liang
date: "`r format(Sys.time(), '%d %B, %Y')`"
bibliography: myref.bib
---

# Set up

```{r setup}
source('../scripts/mylib.R')
library(dplyr)
library(reshape2)
library(ggplot2)
library(stringr)
library(htmltools)
library(plotly)
library(gridExtra)
set.seed(2018)
```

# Preparing BigQuery tables

```{r}
tbl.all <- tableInfo('GTEx_v8_eQTL', 'Whole_Blood_allpairs')
tbl.pos <- tableInfo('annotations', 'gencode_v26')
```

# Load candidate gene clusters

The definition of cluster can be found at [here](distr_gene_whole_blood.html).

```{r}
nclst <- 5
cluster.df <- read.table('../output/distr_gene_whole_blood__candidate_regions.txt', header = T)
cluster.df <- cluster.df[cluster.df$clst.idx %in% sample(unique(cluster.df$clst.idx), nclst, replace = F), ]
zscore.df <- getZscoreByGene(tbl.all, cluster.df$gene)
clst.idxs <- unique(cluster.df$clst.idx)
```

# Definition of $P_n$ and $P_m$

As suggested by [@jordan2018landscape] with a little change (normalize z-score after correcting for correlation).

\begin{align*}
  Z^{raw} &\in \mathbb{R}^{p \times t} \\
  \Sigma &= \text{Cor}(Z^{raw}) \in \mathbb{R}^{t \times t} \\
  A &= \text{cholesky}(\Sigma) \quad\text{i.e. $\Sigma = A^T A$} \\
  Z^{un-normalized} &= A^{-T} Z^{raw} \\
  Z &= \text{normalize}(Z^{un-normalized}) \\
  P_n &:= \#\{|Z_{ij}| > 2\} \quad\text{for each variant $j$ counting over all traits} \\
  P_m &:= \sum_i Z_{ij}^2 \quad\text{for each variant $j$ summing over all traits} \\
\end{align*}
, where $t$ is the number of traits and $p$ is the number of variants.

Under $H_0$, no horizontal pleiotropy,

\begin{align*}
  P_n &\sim Binomial(t, \Pr(|z| > 2)) \\
  P_m &\sim \chi^2_t
\end{align*}

# Compute horizontal pleiotropy for each cluster

```{r}
gene.cls <- list()
for(j in 1 : 5) {
  i <- clst.idxs[j]
  clst.i <- cluster.df[cluster.df$clst.idx == i, ]
  genes <- cluster.df$gene[cluster.df$clst.idx == i]
  genes.cleaned <- str_match(genes, '([A-Za-z0-9]+).[0-9]+')[, 2]
  gene.info <- getPosByGeneID(tbl.pos, genes.cleaned)
  gene.info$rank <- rank(gene.info$start)
  gene.cls[[j]] <- gene.info
}
```

```{r, results='asis', fig.height=10}
output <- list()
for(j in 1 : 5) {
  i <- clst.idxs[j]
  clst.i <- cluster.df[cluster.df$clst.idx == i, ]
  cat('##', paste0(clst.i$clst.chr[1], ':', clst.i$clst.start[1], '-', clst.i$clst.end[1]))
  cat('\n')
  cat('\n')
  plots <- list()
  genes <- cluster.df$gene[cluster.df$clst.idx == i]
  zscore.df.sub <- zscore.df[zscore.df$gene_id %in% genes, ]
  shared.snp <- zscore.df.sub %>% group_by(variant_id) %>%
    summarise(ngene = length(gene_id)) %>% 
    filter(ngene == length(genes)) %>%
    select(variant_id)
  zscore.df.shared <- zscore.df.sub[zscore.df.sub$variant_id %in% shared.snp$variant_id, ]
  zscore.df.shared$zscore <- zscore.df.shared$slope / zscore.df.shared$slope_se
  zscore_mat <- dcast(zscore.df.shared, variant_id ~ gene_id, value.var = 'zscore')
  zscore_mat.cleaned <- zscore_mat[, -1]
  hp <- horizontalPleiotropy(zscore_mat.cleaned)
  pw <-plotWhitening(zscore_mat.cleaned)
  df.pw <- melt(pw)
  df.plot <- data.frame(Pn.pval = hp$Pn.pval, Pm.pval = hp$Pm.pval, Pn = hp$Pn, Pm = hp$Pm)
  plots[[length(plots) + 1]] <- ggplot(df.plot) + geom_jitter(aes(x = Pn.pval, y = Pm.pval), alpha = .3)
  plots[[length(plots) + 1]] <- ggplot(df.plot) + geom_jitter(aes(x = Pn, y = Pm), alpha = .3)
  pos <- as.numeric(str_match(zscore_mat[, 1], 'chr[0-9]+_([0-9]+)_[A-Za-z_0-9]+')[, 2])
  df.plot$pos <- pos
  p <- ggplot() + geom_point(data = df.plot, aes(x = pos, y = -log10(Pm.pval), color = Pn)) + scale_color_gradient2(low = "black", midpoint = 2, mid = "orange", high = "red")
  plots[[length(plots) + 1]] <- p
  plots[[length(plots) + 1]] <- ggplot(df.plot) + geom_point(aes(x = pos, y = -log10(Pn.pval), color = Pm)) + scale_color_gradient2(low = "black", midpoint = dim(zscore_mat.cleaned)[2], mid = "orange", high = "red")
  plots[[length(plots) + 1]] <- QQplot(df.plot$Pm.pval, title = 'QQplot of Pm')
  plots[[length(plots) + 1]] <- QQplot(df.plot$Pn.pval, title = 'QQplot of Pn')
  
  # genes.cleaned <- str_match(genes, '([A-Za-z0-9]+).[0-9]+')[, 2]
  # gene.info <- getPosByGeneID(tbl.pos, genes.cleaned)
  # gene.info$rank <- rank(gene.info$start)
  gene.info <- gene.cls[[j]]
  df.pw$Var1 <- factor(df.pw$Var1, levels = genes[order(gene.info$start, decreasing = F)])
  df.pw$Var2 <- factor(df.pw$Var2, levels = genes[order(gene.info$start, decreasing = F)])
  plots[[length(plots) + 1]] <- ggplot(df.pw[df.pw$L1 == 'before_whitening', ]) + geom_raster(aes(x = Var1, y = Var2, fill = value)) + scale_fill_gradient2(low = "blue", midpoint = 0, mid = "white", high = "red") + ggtitle('Before whitening') + theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(), axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
  plots[[length(plots) + 1]] <- ggplot(df.pw[df.pw$L1 == 'after_whitening', ]) + geom_raster(aes(x = Var1, y = Var2, fill = value)) + scale_fill_gradient2(low = "blue", midpoint = 0, mid = "white", high = "red") + ggtitle('After whitening') + theme(axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(), axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
  # # grid.arrange(grobs = plots, ncol = 2)
  do.call('grid.arrange', c(plots, ncol = 2))
  # cat('\n')
  # cat('\n')
  # 
  p2 <- p + geom_rect(data = gene.info, aes(xmin = start, xmax = end, ymin = -(rank / 2 - 0.25 + 1), ymax = -(rank / 2 + 0.25 + 1), fill = strand)) + geom_text(data = gene.info, aes(x = start, y = -(rank / 2 + 1), label = gene_name), size = 2, hjust = 0)
  p2 <- ggplotly(p2)
  x <- as.widget(ggplotly(p2))
  output[[length(output) + 1]] <- x
  cat(renderTags(x)$html)
  cat('\n')
  cat('\n')
}
```

```{r echo=FALSE, messages=FALSE, warning=FALSE}
# attach the Dependencies
# since the do not get included with renderTags(...)$html
deps <- lapply(
  Filter(function(x){{inherits(x,"htmlwidget")}},output),
  function(hw){{
    renderTags(hw)$dependencies
  }}
)
attachDependencies(
  tagList(),
  unlist(deps,recursive=FALSE)
)
```

# Comments

Note that LD is not corrected which may lead to inflated p-values for some regions. Also, the number of traits is small, so that $P_n$ is discretized.

# References