---
title: "LD (R2) between eQTLs defined by DAPG (aggregated across all tissues)" 
# author: Yanyu Liang
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    code_folding: hide
---

# Load traits

```{r}
traits <- c() # read.table('../analysis/slope_of_gwas_hits/qsub/selected_traits/selected_traits.txt')
temp <- read.table('../data/trait_lists/high_quality_gwas_from_alvaro.txt')
temp <- str_remove(temp$V1, 'imputed_')
temp <- str_remove(temp, '.txt.gz')
traits <- unique(c(as.character(traits$V1), temp))
```

# Histogram of LD (R2) between DAPG eQTLs

```{r, results='asis'}
for(t in traits) {
  cat('##', t, '\n\n')
  cat(paste0('![](../analysis/slope_of_gwas_hits/output_ld/hist.', t, '.png)\n\n'))
}
```