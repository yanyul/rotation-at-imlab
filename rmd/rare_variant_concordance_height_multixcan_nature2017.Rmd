---
title: "Rare variant association vs. MultiXcan result: exploratory analysis of GIANT HEIGHT and UKB Standing height"
# author: Yanyu Liang
date: "`r format(Sys.time(), '%d %B, %Y')`"
---

```{r setup}
library(ggplot2)
library(dplyr)
library(pander)
library(stringr)
library(data.table)
options(datatable.fread.datatable = F)
panderOptions('table.split.table', Inf)
source('../scripts/mylib.R')
source('../scripts/mylib_doc.R')
```


# Load MultiXcan and enloc data

```{r}
get_best = function(x, y) {
  o = x
  o[abs(y) > abs(x)] = y[abs(y) > abs(x)]
  o
}
predixcan = read.table('~/Desktop/rare_variant/predixcan_enloc/smultixcan_imputed_gwas_gtexelv8_GIANT_HEIGHT_ccn30.txt', header = T) %>% mutate(gene = trim_dot(gene)) %>% mutate(z_sig = get_best(z_min, z_max))
predixcan2 = read.table('~/Desktop/rare_variant/predixcan_enloc/smultixcan_imputed_gwas_gtexelv8_UKB_50_Standing_height_ccn30.txt', header = T) %>% mutate(gene = trim_dot(gene)) %>% mutate(z_sig = get_best(z_min, z_max))
enloc = readRDS('~/Desktop/rare_variant/predixcan_enloc/eQTL--GIANT_HEIGHT__data.enloc.rds') %>% mutate(gene = trim_dot(molecular_qtl_trait)) 
enloc2 = readRDS('~/Desktop/rare_variant/predixcan_enloc/eQTL--UKB_50_Standing_height__data.enloc.rds') %>% mutate(gene = trim_dot(molecular_qtl_trait)) 
```

# Load rare variant association (Nature paper)

Data from [this paper](https://www.nature.com/articles/nature21039). Only reported significant genes (`p < 1e-4`)

```{r}
rare_var_tmp = read.csv('~/Desktop/rare_variant/height_rare_variant_nature_2017_vartest.csv')
rare_var_tmp2 = read.csv('~/Desktop/rare_variant/height_rare_variant_nature_2017.csv')
```

# Map gene id 

Focus on protein coding genes

```{r}
gene_map = read.table('../data/annotations_gencode_v26.tsv', header = T)
rare_var = left_join(rare_var_tmp %>% rename(gene_name = Gene), gene_map %>% select(gene_name, gene_id, gene_type), by = 'gene_name') %>% rename(gene = gene_id)
rare_var_mapped = rare_var %>% filter(!is.na(gene)) 
rare_var_coding = rare_var_mapped %>% filter(gene_type == 'protein_coding')
rare_var2 = left_join(rare_var_tmp2 %>% rename(gene_name = Gene), gene_map %>% select(gene_name, gene_id, gene_type), by = 'gene_name') %>% rename(gene = gene_id)
rare_var_mapped2 = rare_var2 %>% filter(!is.na(gene)) 
rare_var_coding2 = rare_var_mapped2 %>% filter(gene_type == 'protein_coding')
is_consistent = function(x) {
  sum(x > 0) == 0 | sum(x < 0) == 0
}
get_beta = function(x, p) {
  x[which.min(p)]
}
rare_var_coding = rare_var_coding %>% group_by(gene) %>% summarise(is_consistent = is_consistent(Beta), effect_size = get_beta(Beta, P.value), se = get_beta(SE, P.value), annotation = get_beta(VEP.annotation, P.value), gene_name = get_beta(gene_name, P.value), pval = get_beta(P.value, P.value), eaf = get_beta(EAF, P.value))
rare_var_coding = rare_var_coding %>% filter(is_consistent)
```

# GIANT HEIGHT

```{r}
predixcan = predixcan %>% mutate(is_nature_sig = gene %in% rare_var_coding$gene, is_confident_direction = z_min * z_max > 0, is_bonferroni_sig = pvalue < 0.05 / n())
predixcan %>% group_by(is_nature_sig) %>% mutate(p_expected = rank(pvalue) / n()) %>% ungroup() %>% ggplot() + geom_point(aes(x = -log10(p_expected), y = -log10(pvalue), color = is_nature_sig)) + geom_abline(slope = 1, intercept = 0) + ggtitle('MultiXcan QQ plot - GIANT HEIGHT')
enloc_sum = enloc %>% group_by(gene) %>% summarize(rcp = max(locus_rcp)) %>% ungroup() %>% mutate(is_nature_sig = gene %in% rare_var_coding$gene, is_colocalized = rcp > 0.1)
enloc_sum %>% group_by(is_nature_sig) %>% summarize(fraction_of_colocalized = mean(is_colocalized)) %>% ggplot() + geom_bar(aes(x = is_nature_sig, y = fraction_of_colocalized), stat = 'identity') + ggtitle('Encolocalized fraction - GIANT HEIGHT')
predixcan = predixcan %>% mutate(is_colocalized = gene %in% enloc_sum$gene[enloc_sum$rcp > 0.1])
predixcan %>% filter(is_nature_sig, is_confident_direction, is_bonferroni_sig, is_colocalized) %>% inner_join(rare_var_coding, by = 'gene') %>% ggplot() + geom_point(aes(x = effect_size / se, y = z_sig, color = annotation)) + ggtitle('Rare variant z-score vs MultiXcan z-score - GIANT HEIGHT')
```

# UKB Standing height

```{r}
predixcan2 = predixcan2 %>% mutate(is_nature_sig = gene %in% rare_var_coding$gene, is_confident_direction = z_min * z_max > 0, is_bonferroni_sig = pvalue < 0.05 / n())
predixcan2 %>% group_by(is_nature_sig) %>% mutate(p_expected = rank(pvalue) / n()) %>% ungroup() %>% ggplot() + geom_point(aes(x = -log10(p_expected), y = -log10(pvalue), color = is_nature_sig)) + geom_abline(slope = 1, intercept = 0) + ggtitle('MultiXcan QQ plot - UKB Standing height')
enloc_sum2 = enloc2 %>% group_by(gene) %>% summarize(rcp = max(locus_rcp)) %>% ungroup() %>% mutate(is_nature_sig = gene %in% rare_var_coding$gene, is_colocalized = rcp > 0.1)
enloc_sum2 %>% group_by(is_nature_sig) %>% summarize(fraction_of_colocalized = mean(is_colocalized)) %>% ggplot() + geom_bar(aes(x = is_nature_sig, y = fraction_of_colocalized), stat = 'identity') + ggtitle('Encolocalized fraction - UKB Standing height')
predixcan2 = predixcan2 %>% mutate(is_colocalized = gene %in% enloc_sum2$gene[enloc_sum2$rcp > 0.1])
predixcan2 %>% filter(is_nature_sig, is_confident_direction, is_bonferroni_sig, is_colocalized) %>% inner_join(rare_var_coding, by = 'gene') %>% ggplot() + geom_point(aes(x = effect_size / se, y = z_sig, color = annotation)) + ggtitle('Rare variant z-score vs MultiXcan z-score - UKB Standing height')
```