---
title: "Causal tissue exploratory enrichment analysis all traits (using PrediXcan-mashr)"
# author: Yanyu Liang
date: "`r format(Sys.time(), '%d %B, %Y')`"
---

```{r setup}
library(ggplot2)
library(dplyr)
library(pander)
panderOptions('table.split.table', Inf)
```

# About

**Procedure**:

1. Extract enloc and PrediXcan result
2. Define gene set by PrediXcan + enloc criteria:
        - **Positive**: min(p-value) < 0.05 / ntest and enloc > 0.01 in at least one tissue
        - **Negative**: RCP = 0 in all tissues
3. Project gene onto eQTL FLASH factor
    * Exclude factor 1 and 7 (tissue-shared and testis factors) and multiple tissue factor (likely to be leftover cross-tissue factor facvoring large sample size)
    * Exclude genes with max PVE <= 0.2
    * Merge FLASH factors into categories 
    ![](../analysis/flash_on_predixcan/exploration_scripts/merged_factor.png)
4. Enrichment: construct positive-negative by tissue-pattern-category table (2x2 table) and perform Fisher's exact test

# Trait list

```{r}
traits = read.delim2('../data/gwas_metadata.txt') %>% select(Tag, new_Phenotype, Category, color)                      
traits = traits[order(paste(traits$Category, traits$new_Phenotype)), ]
rownames(traits) = NULL
traits %>% pander
```

# Read in enrichment results

```{r}
df = data.frame()
for(i in 1 : nrow(traits)) {
  filename = paste0('../analysis/flash_on_predixcan/output/eQTL-mashr--', traits$Tag[i], '__enrichment.combined.rds')
  if(file.exists(filename)) {
    combined = readRDS(filename)$test
  } else {
    next
  }
  combined = combined %>% mutate(trait = traits$Tag[i], color = traits$color[i], name = traits$new_Phenotype[i])
  df = rbind(df, as.data.frame(combined))
}
```

# Plot

```{r}
df_clean = df %>% group_by(trait) %>% summarize(ntissue = sum(!is.na(odds_ratio))) # %>% filter(ntissue >= 8)
df_clean = df %>% filter(trait %in% df_clean$trait)
df_clean$name = factor(df_clean$name, levels = traits$new_Phenotype[order(paste(traits$Category, traits$new_Phenotype))])
color_guide = df_clean$color[order(df_clean$name)][!duplicated(df_clean$trait)]
names(color_guide) = df_clean$name[order(df_clean$name)][!duplicated(df_clean$trait)]
p = df_clean %>% mutate(signed_log10p = -log10(pval) * ((odds_ratio > 1) - 0.5) * 2, is_signif = ifelse(pval < 0.05, 'dot', 'no_dot')) %>% ggplot(aes(x = name, y = merged_membership)) + geom_raster(aes(fill = signed_log10p)) + geom_point(aes(size = is_signif)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5, color = as.character(color_guide[as.character(df_clean$name[order(df_clean$name)][!duplicated(df_clean$trait)])]))) + scale_fill_gradient2(low = 'blue', high = 'red', mid = 'white', midpoint = 0, na.value = 'white')+
     scale_size_manual(values=c(dot=0.00006, no_dot=NA), guide="none") + theme(legend.position="bottom")
ggsave('../output/causal_tissue_complete-mashr.png', p, width = 10, height = 6, unit = 'in')
p
```



# Plot with odds ratio

```{r}
df_clean = df %>% group_by(trait) %>% summarize(ntissue = sum(!is.na(odds_ratio))) # %>% filter(ntissue >= 8)
df_clean = df %>% filter(trait %in% df_clean$trait)
df_clean$name = factor(df_clean$name, levels = traits$new_Phenotype[order(paste(traits$Category, traits$new_Phenotype))])
color_guide = df_clean$color[order(df_clean$name)][!duplicated(df_clean$trait)]
names(color_guide) = df_clean$name[order(df_clean$name)][!duplicated(df_clean$trait)]
p = df_clean %>% mutate(signed_log10p = -log10(pval) * ((odds_ratio > 1) - 0.5) * 2, is_signif = ifelse(pval < 0.05, 'dot', 'no_dot')) %>% ggplot(aes(x = name, y = merged_membership)) + geom_raster(aes(fill = odds_ratio)) + geom_point(aes(size = is_signif)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5, color = as.character(color_guide[as.character(df_clean$name[order(df_clean$name)][!duplicated(df_clean$trait)])]))) + scale_fill_gradient2(low = 'blue', high = 'red', mid = 'white', midpoint = 0, na.value = 'white')+
     scale_size_manual(values=c(dot=0.00006, no_dot=NA), guide="none") + theme(legend.position="bottom")
ggsave('../output/causal_tissue_complete-odds-ratio-mashr.png', p, width = 10, height = 6, unit = 'in')
p
```
